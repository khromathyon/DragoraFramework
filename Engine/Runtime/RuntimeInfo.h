#pragma once

#include "Platform/Types.h"

#include "Foundation/String.h"
#include "Foundation/Name.h"
#include "Foundation/FilePath.h"

#include "Runtime/API.h"

namespace Dragora
{
    class DRAGORA_RUNTIME_API RuntimeInfo
	{
	public:
		static void Cleanup();

		static Dragora::String& GetApplicationName();
		static Dragora::String& GetDeveloperName();
		static Dragora::String& GetPublisherName();
        static bool HasPublisher();

		static void SetRuntimeInfo(const Dragora::String& appName,
								   const Dragora::String& devName,
                                   const Dragora::String& pubName,
                                   const bool gotPublisher = false);

		//TODO: Versioning Schema for Application: something like at: http://sourcey.com/comparing-version-strings-in-cpp/
	};
}
