#pragma once

#include "Platform/Types.h"

#include "Foundation/String.h"
#include "Foundation/FilePath.h"

#include "Runtime/API.h"

namespace Dragora
{
	class FileStream;

	/// Low-level file utilities
    class DRAGORA_RUNTIME_API FileLocations
	{
	public:
		/// @name Static Initialization
		//@{
		static void Shutdown();
		//@}

		/// @name Filesystem Information
		//@{
		static void SetBaseDirectory( const FilePath& path );
        static void SetPrefDirectory( const FilePath& path );

		static bool GetBaseDirectory( FilePath& path );
		static bool GetDataDirectory( FilePath& path );
		static bool GetUserDirectory( FilePath& path );
        static bool GetPrefDirectory( FilePath& path );
		//@}
	};
}
