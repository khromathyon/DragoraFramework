#pragma once

#include "Platform/System.h"

#if DRAGORA_SHARED
# ifdef DRAGORA_RUNTIME_EXPORTS
#  define DRAGORA_RUNTIME_API DRAGORA_API_EXPORT
# else
#  define DRAGORA_RUNTIME_API DRAGORA_API_IMPORT
# endif
#else
# define DRAGORA_RUNTIME_API
#endif
