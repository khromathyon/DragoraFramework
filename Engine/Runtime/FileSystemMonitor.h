#pragma once

#include "Runtime/API.h"
#include "Foundation/String.h"

namespace Dragora
{
    class DRAGORA_RUNTIME_API OnChangeCallback
    {
    public:
        virtual void OnChange() = 0;
        virtual ~OnChangeCallback() {};
    };

}