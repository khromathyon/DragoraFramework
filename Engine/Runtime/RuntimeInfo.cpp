#include "Runtime/RuntimeInfo.h"

#include "Platform/Process.h"

using namespace Dragora;

static Dragora::String g_ApplicationName;
static Dragora::String g_DeveloperName;
static Dragora::String g_PublisherName;
static bool g_GotPublisher;

void RuntimeInfo::Cleanup()
{
	g_ApplicationName.Clear();
	g_DeveloperName.Clear();
	g_PublisherName.Clear();
}

Dragora::String & Dragora::RuntimeInfo::GetApplicationName()
{
	DRAGORA_ASSERT(!g_ApplicationName.IsEmpty());
	return g_ApplicationName;
}

Dragora::String & RuntimeInfo::GetDeveloperName()
{
	DRAGORA_ASSERT(!g_DeveloperName.IsEmpty());
	return g_DeveloperName;
}

Dragora::String & RuntimeInfo::GetPublisherName()
{
	DRAGORA_ASSERT(!g_PublisherName.IsEmpty());
    return g_PublisherName;
}

bool RuntimeInfo::HasPublisher()
{
    return g_GotPublisher;
}

void RuntimeInfo::SetRuntimeInfo(const Dragora::String & appName, const Dragora::String & devName, const Dragora::String & pubName, const bool gotPublisher)
{
	g_ApplicationName = appName;
	g_DeveloperName = devName;
    if(gotPublisher)
        g_PublisherName = pubName;
}

