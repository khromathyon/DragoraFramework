#pragma once

#include "Neutrino/API.h"

#include "Platform/File.h"
#include "Platform/Utility.h"
#include "Platform/Exception.h"

#include "Foundation/Stream.h"

#include <algorithm>
#include <vector>

namespace Assets { class IFileInterface; }

using namespace Dragora;

namespace Serialization { namespace ChunkFile
{
    typedef uint64_t TypeIdentifier;
    typedef uint32_t SizeType;

    static const TypeIdentifier TypeIdentifier_Unknown = 0;

    class DRAGORA_NEUTRINO_API ChunkHeader
    {
    public:
        TypeIdentifier  _type;
        unsigned        _chunkVersion;
        char            _name[32];      // fixed size for serialisation convenience
        SizeType        _fileOffset;
        SizeType        _size;

        ChunkHeader()
        {
            _type = TypeIdentifier_Unknown;
            _chunkVersion = 0;
            std::fill(_name, &_name[DRAGORA_ARRAY_COUNT(_name)], 0);
            _fileOffset = _size = 0;
        }

        ChunkHeader(TypeIdentifier type, unsigned version, 
                    const char name[], SizeType size)
        {
            _type = type;
            _chunkVersion = version;
            CopyString(_name, name);
            _fileOffset = 0;        // (not yet decided)
            _size = size;
        }
    };

    static const unsigned MagicHeader = uint32_t('K') | (uint32_t('R') << 8) | (uint32_t('O') << 16) | (uint32_t('~') << 24);
    static const unsigned ChunkFileVersion = 0;

    class DRAGORA_NEUTRINO_API ChunkFileHeader
    {
    public:
        uint32_t      _magic;
        unsigned    _fileVersionNumber;
        char        _buildVersion[64];
        char        _buildDate[64];
        unsigned    _chunkCount;
    };

    ChunkFileHeader MakeChunkFileHeader(unsigned chunkCount, const char buildVersionString[], const char buildDateString[]);
    std::vector<ChunkHeader> LoadChunkTable(::Assets::IFileInterface& file);

    ChunkHeader FindChunk(
        const char filename[], std::vector<ChunkHeader>& hdrs,
        TypeIdentifier chunkType, unsigned expectedVersion); //utf8

    std::unique_ptr<uint8_t[]> RawChunkAsMemoryBlock(
        const char filename[], TypeIdentifier chunkType, unsigned expectedVersion); //utf8

    namespace Internal
    {
        template<typename Writer>
            class SimpleChunkFileWriterT
        {
        public:
            SimpleChunkFileWriterT(
                Writer&& writer, 
				unsigned chunkCount,
                const char buildVersionString[], const char buildDateString[]);
            ~SimpleChunkFileWriterT();

            void BeginChunk(
                Serialization::ChunkFile::TypeIdentifier type,
                unsigned version, const char name[]);
            void FinishCurrentChunk();

			size_t Write(const void *buffer, size_t size, size_t count) never_throws;
			size_t Seek(size_t offset, Dragora::SeekOrigin anchor = SeekOrigins::Begin);
			size_t TellP() const never_throws;
			void Flush() never_throws;

        protected:
			Writer& _writer;
            Serialization::ChunkFile::ChunkHeader _activeChunk;
            size_t _activeChunkStart;
            bool _hasActiveChunk;
            unsigned _chunkCount;
            unsigned _activeChunkIndex;
        };
    }

    using SimpleChunkFileWriter = Internal::SimpleChunkFileWriterT<std::unique_ptr<Stream>>;

}}

