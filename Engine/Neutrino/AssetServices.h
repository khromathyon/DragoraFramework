#pragma once

#include "Neutrino/API.h"
#include <memory>
#include <assert.h>

namespace Assets
{
    class AssetSetManager;
    class CompileAndAsyncManager;
    class InvalidAssetManager;

    class DRAGORA_NEUTRINO_API AssetServices 
    {
    public:
		static AssetServices& GetInstance() { return *s_instance; }
        
        static AssetSetManager& GetAssetSets() { return *GetInstance()._assetSets; }
        static CompileAndAsyncManager& GetAsyncMan() { return *GetInstance()._asyncMan; }
        static InvalidAssetManager* GetInvalidAssetMan() { return s_instance?s_instance->_invalidAssetMan.get():nullptr; }

        struct Flags 
        {
            enum Enum { RecordInvalidAssets = 1<<0 };
            typedef unsigned BitField;
        };

		static void Startup(Flags::BitField flags = 0);
		static void Shutdown();
        
        AssetServices(const AssetServices&) = delete;
        const AssetServices& operator=(const AssetServices&) = delete;
    protected:
		AssetServices(Flags::BitField flags = 0);
		~AssetServices();

        static AssetServices* s_instance;
        
        std::unique_ptr<AssetSetManager> _assetSets;
        std::unique_ptr<CompileAndAsyncManager> _asyncMan;
        std::unique_ptr<InvalidAssetManager> _invalidAssetMan;
    };

}


