#pragma once

#include "Neutrino/API.h"
#include "Neutrino/InvalidAssetManager.h"

#include "Foundation/String.h"

#include <memory>
#include <vector>

namespace Assets
{
    class PendingCompileMarker;

    /// NOTE: DEFAULT THEM ALL TO CJSON!
    /// <summary>Container file with with one child that is initialized via InputStreamFormatter</summary>
    ///
    /// Represents a file that contains a single serialized item. That item must be a type that
    /// can be deserialized with InputStreamFormatter.
    ///
    /// <example>
    ///     Consider a configuration object like:
    ///     <code>\code
    ///         class Config
    ///         {
    ///         public:
    ///             Config( InputStreamFormatter<utf8>& formatter,
    ///                     const ::Assets::DirectorySearchRules&);
    ///             ~Config();
    ///         };
    ///     \endcode</code>
    ///
    ///     This might contain some configuration options, maybe some simple members or maybe
    ///     even some complex members.
    ///
    ///     Sometimes we might want to store a configuration settings like this in it's own
    ///     individual file. Other times, we might want to store it within a larger file, just
    ///     as part of heirarchy of serialized objects.
    ///
    ///     Because the object is deserialized directly from the formatter, we have the flexibility
    ///     to do that.
    ///
    ///     When we want that object to exist on it's own, in an individual file, we can use
    ///     ConfigFileContainer<Config>. With a ConfigFileContainer, it can be considered a
    ///     fully functional asset, with a dependency validation, relative path rules and
    ///     reporting correctly to the InvalidAssetManager.
    /// </example>
    template<typename Formatter /*= InputStreamFormatter<char>*/>
        class ConfigFileContainer
    {
    public:
		Formatter GetRootFormatter() const;
		Formatter GetFormatter(Dragora::StringSection<typename Formatter::value_type>) const;

		static std::unique_ptr<ConfigFileContainer> CreateNew(Dragora::StringSection<ResChar> initialiser);

        ConfigFileContainer(Dragora::StringSection<ResChar> initializer);
        ~ConfigFileContainer();

        const std::shared_ptr<DependencyValidation>& GetDependencyValidation() const   { return _validationCallback; }
    protected:
        std::shared_ptr<DependencyValidation> _validationCallback;
		std::unique_ptr<uint8_t[]> _fileData;
		size_t _fileSize;
    };

    template<typename CharType>
        class TextChunk
    {
    public:
        Dragora::StringSection<CharType> _type, _name, _content;

        TextChunk(Dragora::StringSection<CharType> type, Dragora::StringSection<CharType> name, Dragora::StringSection<CharType> content)
            : _type(type), _name(name), _content(content) {}
    };

    template<typename CharType>
        std::vector<TextChunk<CharType>> ReadCompoundTextDocument(Dragora::StringSection<CharType> doc);
}

