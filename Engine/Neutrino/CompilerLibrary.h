#pragma once

#include "Neutrino/API.h"
#include "Neutrino/AssetsCore.h"

#include "Foundation/String.h"

#include <vector>
#include <memory>

namespace Assets
{
	class NascentChunk;
	using NascentChunkArray = std::shared_ptr<std::vector<NascentChunk>>;

	class DRAGORA_NEUTRINO_API ICompilerDesc
	{
	public:
		virtual const char*			Description() const = 0;

		class FileKind
		{
		public:
			const ::Assets::ResChar*	_extension;
			const char*					_name;
		};
		virtual unsigned			FileKindCount() const = 0;
		virtual FileKind			GetFileKind(unsigned index) const = 0;

		virtual ~ICompilerDesc();
	};

	class DRAGORA_NEUTRINO_API ICompileOperation
	{
	public:
		class DRAGORA_NEUTRINO_API TargetDesc
		{
		public:
			uint64_t		_type;
			const char*		_name;
		};
		virtual unsigned			TargetCount() const = 0;
		virtual TargetDesc			GetTarget(unsigned idx) const = 0;
		virtual NascentChunkArray	SerializeTarget(unsigned idx) = 0;

		virtual ~ICompileOperation();
	};

	typedef std::shared_ptr<ICompilerDesc> GetCompilerDescFn();
	typedef std::shared_ptr<ICompileOperation> CreateCompileOperationFn(Dragora::StringSection<::Assets::ResChar> identifier);
}

