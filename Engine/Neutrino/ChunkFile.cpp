#include "ChunkFile.h"
#include "Assets.h"
#include "IFileSystem.h"
#include <tuple>

using namespace Dragora;

namespace Serialization { namespace ChunkFile
{
    using Assets::Exceptions::FormatError;

    ChunkFileHeader MakeChunkFileHeader(unsigned chunkCount, const char buildVersionString[], const char buildDateString[])
    {
        ChunkFileHeader header;
        MemoryZero(&header, sizeof(ChunkFileHeader));
        header._magic = MagicHeader;
        header._fileVersionNumber = ChunkFileVersion;
        CopyString(header._buildVersion, buildVersionString);
        CopyString(header._buildDate, buildDateString);
        header._chunkCount = chunkCount;
        return header;
    }

    std::vector<ChunkHeader> LoadChunkTable(::Assets::IFileInterface& file)
    {
        ChunkFileHeader fileHeader;
        if (file.Read(&fileHeader, sizeof(ChunkFileHeader), 1) != 1) {
            throw FormatError("Incomplete file header");
        }

        if (fileHeader._magic != MagicHeader) {
            throw FormatError("Unrecognised format");
        }

        if (fileHeader._fileVersionNumber != ChunkFileVersion) {
            throw FormatError("Bad chunk file format");
        }

        std::vector<ChunkHeader> result;
        result.resize(fileHeader._chunkCount);
        auto readCount = file.Read(&(*result.begin()), sizeof(ChunkHeader), fileHeader._chunkCount);
        if (readCount != fileHeader._chunkCount) {
            throw FormatError("Incomplete file header");
        }

        return result;
    }

    Serialization::ChunkFile::ChunkHeader FindChunk(
        const char filename[],
        std::vector<Serialization::ChunkFile::ChunkHeader>& hdrs,
        Serialization::ChunkFile::TypeIdentifier chunkType,
        unsigned expectedVersion)
    {
        Serialization::ChunkFile::ChunkHeader scaffoldChunk;
        for (auto i=hdrs.begin(); i!=hdrs.end(); ++i) {
            if (i->_type == chunkType) {
                scaffoldChunk = *i;
                break;
            }
        }

        if (!scaffoldChunk._fileOffset) {
            throw FormatError("Missing could not find chunk in chunk file: %s", filename);
        }

        if (scaffoldChunk._chunkVersion != expectedVersion) {
            throw FormatError("Incorrect chunk version: %s", filename);
        }

        return scaffoldChunk;
    }

    std::unique_ptr<uint8_t[]> RawChunkAsMemoryBlock(
        const char filename[],
        Serialization::ChunkFile::TypeIdentifier chunkType,
        unsigned expectedVersion)
    {
        auto file = Assets::MainFileSystem::OpenFileInterface(filename, "rb");
        auto chunks = Serialization::ChunkFile::LoadChunkTable(*file);

        auto scaffoldChunk = FindChunk(filename, chunks, chunkType, expectedVersion);
        auto rawMemoryBlock = std::make_unique<uint8_t[]>(scaffoldChunk._size);
        file->Seek(scaffoldChunk._fileOffset);
        file->Read(rawMemoryBlock.get(), 1, scaffoldChunk._size);
        
        return std::move(rawMemoryBlock);
    }


///////////////////////////////////////////////////////////////////////////////////////////////////
    namespace Internal
    {
        template<typename Writer>
            SimpleChunkFileWriterT<Writer>::SimpleChunkFileWriterT(
				Writer&& writer,
                unsigned chunkCount, const char buildVersionString[], const char buildDateString[])
        : _writer(std::move(writer))
        , _chunkCount(chunkCount)
        {
            _activeChunkStart = 0;
            _hasActiveChunk = false;
            _activeChunkIndex = 0;
            
            ChunkFileHeader fileHeader = MakeChunkFileHeader(
                chunkCount, buildVersionString, buildDateString);
			_writer->Write(&fileHeader, sizeof(fileHeader), 1);
            for (unsigned c=0; c<chunkCount; ++c) {
                ChunkHeader t;
				_writer->Write(&t, sizeof(ChunkHeader), 1);
            }
        }

        template<typename Writer>
            SimpleChunkFileWriterT<Writer>::~SimpleChunkFileWriterT()
        {
            if (_hasActiveChunk) {
                FinishCurrentChunk();
            }
            assert(_activeChunkIndex == _chunkCount);
        }

        template<typename Writer>
            void SimpleChunkFileWriterT<Writer>::BeginChunk(   
                Serialization::ChunkFile::TypeIdentifier type,
                unsigned version, const char name[])
        {
            if (_hasActiveChunk) {
                FinishCurrentChunk();
            }

            _activeChunk._type = type;
            _activeChunk._chunkVersion = version;
            Dragora::CopyString(_activeChunk._name, name);
            _activeChunkStart = _writer->Tell();
            _activeChunk._fileOffset = (ChunkFile::SizeType)_activeChunkStart;
            _activeChunk._size = 0; // unknown currently

            _hasActiveChunk = true;
        }

        template<typename Writer>
            void SimpleChunkFileWriterT<Writer>::FinishCurrentChunk()
        {
            using namespace Serialization::ChunkFile;
            auto oldLoc = _writer->Tell();
            auto chunkHeaderLoc = sizeof(ChunkFileHeader) + _activeChunkIndex * sizeof(ChunkHeader);
            //_writer->Seek(chunkHeaderLoc);
			Seek(chunkHeaderLoc);
            _activeChunk._size = (ChunkFile::SizeType)std::max(size_t(0), oldLoc - _activeChunkStart);
			_writer->Write(&_activeChunk, sizeof(ChunkHeader), 1);
            //_writer->Seek(oldLoc);
			Seek(oldLoc);
            ++_activeChunkIndex;
            _hasActiveChunk = false;
        }

		template<typename Writer>
			size_t SimpleChunkFileWriterT<Writer>::Write(const void *buffer, size_t size, size_t count) never_throws
		{
			return _writer->Write(buffer, size, count);
		}

		template<typename Writer>
			size_t SimpleChunkFileWriterT<Writer>::Seek(size_t offset, Dragora::SeekOrigin anchor) never_throws
		{
			return _writer->Seek(offset, anchor);
		}

		template<typename Writer>
			size_t SimpleChunkFileWriterT<Writer>::TellP() const never_throws
		{
			return _writer->Tell();
		}

		template<typename Writer>
			void SimpleChunkFileWriterT<Writer>::Flush() never_throws
		{
			return _writer->Flush();
		}
        
    }

    template SimpleChunkFileWriter;

}}

