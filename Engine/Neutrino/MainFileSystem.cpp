
#include "IFileSystem.h"
#include "MountingTree.h"

using namespace Dragora;

namespace Assets
{
	static std::shared_ptr<MountingTree> s_mainMountingTree;
	static std::shared_ptr<IFileSystem> s_defaultFileSystem;

	static IFileSystem::IOReason AsIOReason(IFileSystem::TranslateResult transResult)
	{
		switch (transResult) {
		case IFileSystem::TranslateResult::Mounting: return IFileSystem::IOReason::Mounting;
		case IFileSystem::TranslateResult::Invalid: return IFileSystem::IOReason::Invalid;
		default: return IFileSystem::IOReason::Invalid;
		}
	}

	static FileDesc::State AsFileState(IFileSystem::TranslateResult transResult)
	{
		switch (transResult) {
		case IFileSystem::TranslateResult::Mounting: return FileDesc::State::Mounting;
		case IFileSystem::TranslateResult::Invalid: return FileDesc::State::Invalid;
		default: return FileDesc::State::Invalid;
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////

	namespace Internal 
	{
		using LookupResult = MountingTree::EnumerableLookup::Result;

		template<typename FileType, typename CharType>
			static IFileSystem::IOReason TryOpen(FileType& result, StringSection<CharType> filename, const char openMode[], FileMode shareMode)
		{
			result = FileType();

			MountingTree::CandidateObject candidateObject;
			auto lookup = s_mainMountingTree->Lookup(filename);
			for (;;) {
				auto r = lookup.TryGetNext(candidateObject);
				if (r == LookupResult::Invalidated)
					THROW(std::logic_error("Mounting point lookup was invalidated when the mounting tree changed. Do not change the mount or unmount filesystems while other threads may be accessing the same mounting tree."));

				if (r == LookupResult::NoCandidates)
					break;

				DRAGORA_ASSERT(candidateObject._fileSystem);
				auto ioRes = candidateObject._fileSystem->TryOpen(result, candidateObject._marker, openMode, shareMode);
				if (ioRes != IFileSystem::IOReason::FileNotFound && ioRes != IFileSystem::IOReason::Invalid)
					return ioRes;
			}

			// attempt opening with the default file system...
			if (s_defaultFileSystem)
				return ::Assets::TryOpen(result, *s_defaultFileSystem, filename, openMode, shareMode);
			return IFileSystem::IOReason::FileNotFound;
		}

		template<typename FileType, typename CharType>
			static IFileSystem::IOReason TryOpen(FileType& result, StringSection<CharType> filename, uint64_t size, const char openMode[], FileMode shareMode)
		{
			result = FileType();

			MountingTree::CandidateObject candidateObject;
			auto lookup = s_mainMountingTree->Lookup(filename);
			for (;;) {
				auto r = lookup.TryGetNext(candidateObject);
				if (r == LookupResult::Invalidated)
					THROW(std::logic_error("Mounting point lookup was invalidated when the mounting tree changed. Do not change the mount or unmount filesystems while other threads may be accessing the same mounting tree."));

				if (r == LookupResult::NoCandidates)
					break;

				DRAGORA_ASSERT(candidateObject._fileSystem);
				auto ioRes = candidateObject._fileSystem->TryOpen(result, candidateObject._marker, size, openMode, shareMode);
				if (ioRes != IFileSystem::IOReason::FileNotFound && ioRes != IFileSystem::IOReason::Invalid)
					return ioRes;
			}

			// attempt opening with the default file system...
			if (s_defaultFileSystem) 
				return ::Assets::TryOpen(result, *s_defaultFileSystem, filename, size, openMode, shareMode);
			return IFileSystem::IOReason::FileNotFound;
		}

		template<typename CharType>
			IFileSystem::IOReason TryMonitor(StringSection<CharType> filename, const std::shared_ptr<IFileMonitor>& evnt)
		{
			MountingTree::CandidateObject candidateObject;
			auto lookup = s_mainMountingTree->Lookup(filename);
			for (;;) {
				auto r = lookup.TryGetNext(candidateObject);
				if (r == LookupResult::Invalidated)
					THROW(std::logic_error("Mounting point lookup was invalidated when the mounting tree changed. Do not change the mount or unmount filesystems while other threads may be accessing the same mounting tree."));

				if (r == LookupResult::NoCandidates)
					break;

				DRAGORA_ASSERT(candidateObject._fileSystem);
				auto ioRes = candidateObject._fileSystem->TryMonitor(candidateObject._marker, evnt);
				if (ioRes != IFileSystem::IOReason::FileNotFound && ioRes != IFileSystem::IOReason::Invalid)
					return ioRes;
			}

			if (s_defaultFileSystem) 
				return ::Assets::TryMonitor(*s_defaultFileSystem, filename, evnt);
			return IFileSystem::IOReason::FileNotFound;
		}

		template<typename CharType>
			FileDesc TryGetDesc(StringSection<CharType> filename)
		{
			MountingTree::CandidateObject candidateObject;
			auto lookup = s_mainMountingTree->Lookup(filename);
			for (;;) {
				auto r = lookup.TryGetNext(candidateObject);
				if (r == LookupResult::Invalidated)
					THROW(std::logic_error("Mounting point lookup was invalidated when the mounting tree changed. Do not change the mount or unmount filesystems while other threads may be accessing the same mounting tree."));

				if (r == LookupResult::NoCandidates) 
					break;

				DRAGORA_ASSERT(candidateObject._fileSystem);
				auto res = candidateObject._fileSystem->TryGetDesc(candidateObject._marker);
				if (res._state != FileDesc::State::DoesNotExist)
					return std::move(res);
			}

			if (s_defaultFileSystem)
				return ::Assets::TryGetDesc(*s_defaultFileSystem, filename);
			return FileDesc{ std::basic_string<char>(), FileDesc::State::DoesNotExist };
		}
	}

	//
	// note -- the UTF8 and UTF16 versions of these functions are identical... They could be implemented
	//			with a template. But the C++ method resolution works better when they are explicitly separated
	//			like this.
	//		eg, because 
	//			MainFileSystem::FileOpen(u("SomeFile.txt"),...);
	//		relies on automatic conversion for StringSection<utf8>, it works in this case, but not in the
	//		template case.
	//

	auto MainFileSystem::TryOpen(std::unique_ptr<IFileInterface>& result, StringSection<char> filename, const char openMode[], FileMode shareMode) -> IOReason
	{
		return Internal::TryOpen(result, filename, openMode, shareMode);
	}

	auto MainFileSystem::TryOpen(std::unique_ptr<Stream>& result, StringSection<char> filename, const char openMode[], FileMode shareMode) -> IOReason
	{
		return Internal::TryOpen(result, filename, openMode, shareMode);
	}

	auto MainFileSystem::TryOpen(std::unique_ptr<DynamicArray<uint8_t>>& result, StringSection<char> filename, uint64_t size, const char openMode[], FileMode shareMode) -> IOReason
	{
		return Internal::TryOpen(result, filename, size, openMode, shareMode);
	}

	IFileSystem::IOReason MainFileSystem::TryMonitor(StringSection<char> filename, const std::shared_ptr<IFileMonitor>& evnt)
	{
		return Internal::TryMonitor(filename, evnt);
	}

	FileDesc MainFileSystem::TryGetDesc(StringSection<char> filename)
	{
		return Internal::TryGetDesc(filename);
	}

	auto MainFileSystem::TryOpen(std::unique_ptr<IFileInterface>& result, StringSection<wchar_t> filename, const char openMode[], FileMode shareMode) -> IOReason
	{
		return Internal::TryOpen(result, filename, openMode, shareMode);
	}

	auto MainFileSystem::TryOpen(std::unique_ptr<Stream>&result, StringSection<wchar_t> filename, const char openMode[], FileMode shareMode) -> IOReason
	{
		return Internal::TryOpen(result, filename, openMode, shareMode);
	}

	auto MainFileSystem::TryOpen(std::unique_ptr<DynamicArray<uint8_t>>& result, StringSection<wchar_t> filename, uint64_t size, const char openMode[], FileMode shareMode) -> IOReason
	{
		return Internal::TryOpen(result, filename, size, openMode, shareMode);
	}

	IFileSystem::IOReason MainFileSystem::TryMonitor(StringSection<wchar_t> filename, const std::shared_ptr<IFileMonitor>& evnt)
	{
		return Internal::TryMonitor(filename, evnt);
	}

	FileDesc MainFileSystem::TryGetDesc(StringSection<wchar_t> filename)
	{
		return Internal::TryGetDesc(filename);
	}

	std::unique_ptr<Stream> MainFileSystem::OpenBasicFile(StringSection<char> filename, const char openMode[], FileMode shareMode)
	{
		std::unique_ptr<Stream> result;
		auto ioRes = TryOpen(result, filename, openMode, shareMode);
		//if (ioRes != IOReason::Success)
			//THROW(Utility::Exceptions::IOException(ioRes, "Failure while opening file (%s) in mode (%s)", std::string((const char*)filename.begin(), (const char*)filename.end()).c_str(), openMode));
		return std::move(result);
	}

	std::unique_ptr<DynamicArray<uint8_t>> MainFileSystem::OpenMemoryMappedFile(StringSection<char> filename, uint64_t size, const char openMode[], FileMode shareMode)
	{
		std::unique_ptr<DynamicArray<uint8_t>> result;
		auto ioRes = TryOpen(result, filename, size, openMode, shareMode);
		//if (ioRes != IOReason::Success)
			//THROW(Utility::Exceptions::IOException(ioRes, "Failure while opening file (%s) in mode (%s)", std::string((const char*)filename.begin(), (const char*)filename.end()).c_str(), openMode));
		return std::move(result);
	}

	std::unique_ptr<IFileInterface> MainFileSystem::OpenFileInterface(StringSection<char> filename, const char openMode[], FileMode shareMode)
	{
		std::unique_ptr<IFileInterface> result;
		auto ioRes = TryOpen(result, filename, openMode, shareMode);
		//if (ioRes != IOReason::Success)
			//THROW(Utility::Exceptions::IOException(ioRes, "Failure while opening file (%s) in mode (%s)", std::string((const char*)filename.begin(), (const char*)filename.end()).c_str(), openMode));
		return std::move(result);
	}

///////////////////////////////////////////////////////////////////////////////////////////////////

	const std::shared_ptr<MountingTree>& MainFileSystem::GetMountingTree() { return s_mainMountingTree; }
	void MainFileSystem::Init(std::shared_ptr<MountingTree>& mountingTree, std::shared_ptr<IFileSystem>& defaultFileSystem)
	{
		s_mainMountingTree = mountingTree;
		s_defaultFileSystem = defaultFileSystem;
	}

	template<typename CharType, typename FileObject> IFileSystem::IOReason TryOpen(FileObject& result, IFileSystem& fs, StringSection<CharType> fn, const char openMode[], FileMode shareMode)
	{
		result = FileObject();

		IFileSystem::Marker marker;
		auto transResult = fs.TryTranslate(marker, fn);
		if (transResult == IFileSystem::TranslateResult::Success)
			return fs.TryOpen(result, marker, openMode);

		return AsIOReason(transResult);
	}

	template<typename CharType, typename FileObject> IFileSystem::IOReason TryOpen(FileObject& result, IFileSystem& fs, StringSection<CharType> fn, uint64_t size, const char openMode[], FileMode shareMode)
	{
		result = FileObject();

		IFileSystem::Marker marker;
		auto transResult = fs.TryTranslate(marker, fn);
		if (transResult == IFileSystem::TranslateResult::Success)
			return fs.TryOpen(result, marker, size, openMode);

		return AsIOReason(transResult);
	}

	template<typename CharType> IFileSystem::IOReason TryMonitor(IFileSystem& fs, StringSection<CharType> fn, const std::shared_ptr<IFileMonitor>& evnt)
	{
		IFileSystem::Marker marker;
		auto transResult = fs.TryTranslate(marker, fn);
		if (transResult == IFileSystem::TranslateResult::Success)
			return fs.TryMonitor(marker, evnt);
		return AsIOReason(transResult);
	}

	template <typename CharType> FileDesc TryGetDesc(IFileSystem& fs, StringSection<CharType> fn)
	{
		IFileSystem::Marker marker;
		auto transResult = fs.TryTranslate(marker, fn);
		if (transResult == IFileSystem::TranslateResult::Success)
			return fs.TryGetDesc(marker);
		return FileDesc{std::basic_string<char>(), AsFileState(transResult)};
	}

	template IFileSystem::IOReason TryOpen<char, std::unique_ptr<IFileInterface>>(std::unique_ptr<IFileInterface>& result, IFileSystem& fs, StringSection<char> fn, const char openMode[], FileMode shareMode);
	template IFileSystem::IOReason TryOpen<char, std::unique_ptr<Stream>>(std::unique_ptr<Stream>& result, IFileSystem& fs, StringSection<char> fn, const char openMode[], FileMode shareMode);
	template IFileSystem::IOReason TryOpen<char, std::unique_ptr<DynamicArray<uint8_t>>>(std::unique_ptr<DynamicArray<uint8_t>>& result, IFileSystem& fs, StringSection<char> fn, uint64_t size, const char openMode[], FileMode shareMode);
	template IFileSystem::IOReason TryMonitor<char>(IFileSystem& fs, StringSection<char> fn, const std::shared_ptr<IFileMonitor>& evnt);
	template FileDesc TryGetDesc<char>(IFileSystem& fs, StringSection<char> fn);
	template IFileSystem::IOReason TryOpen<wchar_t, std::unique_ptr<IFileInterface>>(std::unique_ptr<IFileInterface>& result, IFileSystem& fs, StringSection<wchar_t> fn, const char openMode[], FileMode shareMode);
	template IFileSystem::IOReason TryOpen<wchar_t, std::unique_ptr<Stream>>(std::unique_ptr<Stream>& result, IFileSystem& fs, StringSection<wchar_t> fn, const char openMode[], FileMode shareMode);
	template IFileSystem::IOReason TryOpen<wchar_t, std::unique_ptr<DynamicArray<uint8_t>>>(std::unique_ptr<DynamicArray<uint8_t>>& result, IFileSystem& fs, StringSection<wchar_t> fn, uint64_t size, const char openMode[], FileMode shareMode);
	template IFileSystem::IOReason TryMonitor<wchar_t>(IFileSystem& fs, StringSection<wchar_t> fn, const std::shared_ptr<IFileMonitor>& evnt);
	template FileDesc TryGetDesc<wchar_t>(IFileSystem& fs, StringSection<wchar_t> fn);

	std::unique_ptr<uint8_t[]> TryLoadFileAsMemoryBlock(StringSection<char> sourceFileName, size_t* sizeResult)
	{
		std::unique_ptr<IFileInterface> file;
		if (MainFileSystem::TryOpen(file, sourceFileName, "rb", FileModes::Read) == IFileSystem::IOReason::Success) {
			file->Seek(0, SeekOrigins::End);
			size_t size = file->TellP();
			file->Seek(0);
			if (size) {
				auto result = std::make_unique<uint8_t[]>(size+1);
				file->Read(result.get(), 1, size);
				result[size] = '\0';
				if (sizeResult) {
					*sizeResult = size;
				}
				return result;
			}
		}

		// on missing file (or failed load), we return the equivalent of an empty file
		if (sizeResult) { *sizeResult = 0; }
		return nullptr;
	}

	IFileInterface::~IFileInterface() {}
	IFileSystem::~IFileSystem() {}
}
