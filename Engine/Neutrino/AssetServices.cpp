#include "AssetServices.h"
#include "AssetSetManager.h"
#include "CompileAndAsyncManager.h"
#include "InvalidAssetManager.h"

//NOTE: Remove this header if you intend this to port from STL to EASTL.
#include "Platform/Assert.h"
#include "Platform/Cpp14Compat.h"

using namespace Dragora;

static unsigned short g_Init = 0;

namespace Assets
{
	AssetServices* AssetServices::s_instance = NULL;

	void AssetServices::Startup(Flags::BitField flags)
	{
		if (++g_Init == 1)
		{
			DRAGORA_ASSERT(!s_instance);
			s_instance = new AssetServices(flags);
			DRAGORA_ASSERT(s_instance);
		}
	}

	void AssetServices::Shutdown()
	{
		if (--g_Init == 0)
		{
			DRAGORA_ASSERT(s_instance);
			delete s_instance;
			s_instance = NULL;
		}
	}
    AssetServices::AssetServices(Flags::BitField flags)
    {        
        _assetSets = std::make_unique<AssetSetManager>();
        _asyncMan = std::make_unique<CompileAndAsyncManager>();
        _invalidAssetMan = std::make_unique<InvalidAssetManager>(!!(flags & Flags::RecordInvalidAssets));        
    }

    AssetServices::~AssetServices() 
    {
        _invalidAssetMan.reset();
        _assetSets.reset();
        _asyncMan.reset();
         
        s_instance = NULL;
    }
}

