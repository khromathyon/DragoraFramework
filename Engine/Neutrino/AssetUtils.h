#pragma once

#include "Neutrino/API.h"
#include "Neutrino/AssetsCore.h"
#include "Neutrino/Assets.h"

namespace Assets
{
    class  DRAGORA_NEUTRINO_API DirectorySearchRules
    {
    public:
        void AddSearchDirectory(Dragora::StringSection<ResChar> dir);
        void AddSearchDirectoryFromFilename(Dragora::StringSection<ResChar> filename);

        void ResolveFile(
            ResChar destination[], unsigned destinationCount, 
            const ResChar baseName[]) const;
        void ResolveDirectory(
            ResChar destination[], unsigned destinationCount, 
            const ResChar baseName[]) const;
        bool HasDirectory(Dragora::StringSection<ResChar> dir);
		std::vector<std::basic_string<ResChar>> FindFiles(Dragora::StringSection<char> wildcardSearch) const;

        template<int Count>
            void ResolveFile(ResChar (&destination)[Count], const ResChar baseName[]) const
                { ResolveFile(destination, Count, baseName); }

        void Merge(const DirectorySearchRules& mergeFrom);

        DirectorySearchRules();
        DirectorySearchRules(const DirectorySearchRules&);
        DirectorySearchRules& operator=(const DirectorySearchRules&);
    protected:
		//TODO: Remove that shit.
        ResChar _buffer[512];
        std::vector<ResChar> _bufferOverflow;
        unsigned _startOffsets[8];
        unsigned _bufferUsed;
        unsigned _startPointCount;

		//Use this instead:
		std::vector<rstring> SearchBuffer;

    };

    DirectorySearchRules DefaultDirectorySearchRules(Dragora::StringSection<ResChar> baseFile);

        ////////////////////////////////////////////////////////////////////////////////////////////////

    class DependentFileState
    {
    public:
        std::basic_string<ResChar> _filename;
        uint64_t _timeMarker;

        enum class Status { Normal, Shadowed };
        Status _status;

        DependentFileState() : _timeMarker(0ull), _status(Status::Normal) {}
        DependentFileState(Dragora::StringSection<ResChar> filename, uint64_t timeMarker)
        : _filename(filename.AsString()), _timeMarker(timeMarker), _status(Status::Normal) {}
		DependentFileState(const std::basic_string<ResChar>& filename, uint64_t timeMarker)
		: _filename(filename), _timeMarker(timeMarker), _status(Status::Normal) {}
    };

        ////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>Records the status of asynchronous operation, very much like a std::promise<AssetState></summary>
    class PendingOperationMarker : public std::enable_shared_from_this<PendingOperationMarker>
    {
    public:
        AssetState		GetAssetState() const { return _state; }
        void			SetState(AssetState newState);
        AssetState		StallWhilePending() const;

            // "initializer" interface only provided in debug builds, and only intended for debugging
        const char*     Initializer() const;
        void            SetInitializer(const char initializer[]);

        PendingOperationMarker();
        PendingOperationMarker(AssetState state);
        ~PendingOperationMarker();
    protected:
        AssetState _state;
#ifdef DRAGORA_DEBUG
        /*DEBUG_ONLY(*/char _initializer[260];/*)*/
#endif
    };

		////////////////////////////////////////////////////////////////////////////////////////////////

    /// <summary>Container for a asset filename in string format<summary>
    /// Just a simple generalisation of a path and file name in char array form.
    /// Avoids scattering references to ResChar and MaxPath about
    /// the code (and provide some future-proof-ness).
    ///
    /// Note that in this form there is a strict limit on the max length of 
    /// and asset file name. This is in line with the MAX_PATH soft limit
    /// on some filesystems and standard library implementations... But most
    /// filesystems can actually support much longer path names (even if an
    /// individual directory name or filename is limited)
    class ResolvedAssetFile
    {
    public:
        ResChar _fn[MaxPath];

        const ResChar* get() const  { return _fn; }
        const bool IsGood() const   { return _fn[0] != '\0'; }

        ResolvedAssetFile() { _fn[0] = '\0'; }
    };

    /// @{
    /// Converts an input filename to a form that is best suited for the assets system.
    /// This includes converting absolute filenames into relative format (relative to the
    /// primary mount point).
    /// This is intended for GUI tools that allow the user to enter filenames of any form.
    void MakeAssetName(ResolvedAssetFile& dest, const Dragora::StringSection<ResChar> src);
    void MakeAssetNameChar(ResolvedAssetFile& dest, const Dragora::StringSection<char> src);
    /// @}

        ////////////////////////////////////////////////////////////////////////////////////////////////

    void Dependencies_Shutdown();
}

