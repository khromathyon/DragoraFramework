#include "Assets.h"
#include "AssetServices.h"
#include "CompileAndAsyncManager.h"
#include "IntermediateAssets.h"

#include "Foundation/IteratorUtils.h"

using namespace Dragora;

namespace Assets 
{
    namespace Internal
    {
        void LogHeader(unsigned count, const char typeName[])
        {
            //LogInfo << "------------------------------------------------------------------------------------------";
            //LogInfo << "    Asset set for type (" << typeName << ") with (" <<  count << ") items";
        }

        void LogAssetName(unsigned index, const char name[])
        {
            //LogInfo << "    [" << index << "] " << name;
        }

        void InsertAssetName(   std::vector<std::pair<uint64_t, std::string>>& assetNames, 
                                uint64_t hash, const std::string& name)
        {
            auto ni = LowerBound(assetNames, hash);
            if (ni != assetNames.cend() && ni->first == hash) {
                DRAGORA_ASSERT(0);  // hit a hash collision! In most cases, this will cause major problems!
                    // maybe this could happen if an asset is removed from the table, but it's name remains?
                ni->second = "<<collision>> " + ni->second + " <<with>> " + name;
            } else {
                assetNames.insert(ni, std::make_pair(hash, name));
            }
        }

        void InsertAssetNameNoCollision(   
            std::vector<std::pair<uint64_t, std::string>>& assetNames, 
            uint64_t hash, const std::string& name)
        {
            auto ni = LowerBound(assetNames, hash);
            if (ni == assetNames.cend() || ni->first != hash)
                assetNames.insert(ni, std::make_pair(hash, name));
        }

        std::basic_string<ResChar> AsString() { return std::basic_string<ResChar>(); }

        AssetSetManager& GetAssetSetManager()
        {
            return AssetServices::GetAssetSets();
        }

        std::shared_ptr<ICompileMarker> BeginCompileOperation(
            uint64_t typeCode, const Dragora::StringSection<ResChar> initializers[], 
            unsigned initializerCount)
        {
            auto& compilers = AssetServices::GetAsyncMan().GetIntermediateCompilers();
            auto& store = AssetServices::GetAsyncMan().GetIntermediateStore();
            return compilers.PrepareAsset(typeCode, initializers, initializerCount, store);
        }

    }
}
