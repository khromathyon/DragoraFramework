#pragma once

#include <string>
#include <memory>

#include "Neutrino/API.h"
#include "Platform/Exception.h"

namespace Dragora { template<typename CharType> class StringSection; }
namespace Assets
{
    typedef char ResChar;
	using rstring = std::basic_string<ResChar>;

    enum class AssetState { Pending, Ready, Invalid };

    class DependencyValidation;
    using DepValPtr = std::shared_ptr<DependencyValidation>;

    namespace Exceptions
    {
        class DRAGORA_NEUTRINO_API AssetException : public Dragora::Exception
        {
        public:
            const ResChar* Initializer() const { return _initializer; }
            virtual AssetState State() const = 0;

            AssetException(Dragora::StringSection<ResChar> initializer, const char what[]);
        private:
            ResChar _initializer[512];
        };

        /// <summaryAn asset can't be loaded</summary>
        /// This exception means a asset failed during loading, and can
        /// never be loaded. It might mean that the resource is corrupted on
        /// disk, or maybe using an unsupported file format (or bad version).
        /// The most common cause is due to a compile error in a shader. 
        /// If we attempt to use a shader with a compile error, it will throw
        /// a InvalidAsset exception.
        class DRAGORA_NEUTRINO_API InvalidAsset : public AssetException
        {
        public: 
            virtual bool CustomReport() const;
            virtual AssetState State() const;

            InvalidAsset(Dragora::StringSection<ResChar> initializer, const char what[]);
        };

        /// <summary>An asset is still being loaded</summary>
        /// This is common exception. It occurs if we attempt to use an asset that
        /// is still being prepared. Usually this means that the resource is being
        /// loaded from disk, or compiled in a background thread.
        /// For example, shader resources can take some time to compile. If we attempt
        /// to use the shader while it's still compiling, we'll get a PendingAsset
        /// exception.
        class DRAGORA_NEUTRINO_API PendingAsset : public AssetException
        {
        public: 
            virtual bool CustomReport() const;
            virtual AssetState State() const;

            PendingAsset(Dragora::StringSection<ResChar> initializer, const char what[]);
        };

        class DRAGORA_NEUTRINO_API FormatError : public Dragora::Exception
        {
        public:
            enum class Reason
            {
                Success,
                UnsupportedVersion,
                FormatNotUnderstood
            };

            Reason GetReason() const { return _reason; }

            FormatError(const char format[], ...) never_throws;
            FormatError(Reason reason, const char format[], ...) never_throws;

        private:
            Reason _reason;
        };
    }
}

