#include "DivergentAsset.h"
#include "CompileAndAsyncManager.h"
#include "IntermediateAssets.h"
#include "AssetServices.h"

using namespace Dragora;

namespace Assets
{
    void DivergentAssetBase::AssetIdentifier::OnChange()
    {
        // We need to mark the target file invalidated.
        // this is a little strange, because the target file
        // hasn't actually changed. 
        // 
        // But this is required because some dependent assets
        // don't have a dependency on the asset itself (just
        // on the underlying file). Invalidating the file ensures
        // that we invoke a update on all assets that require it.

        if (_targetFilename.empty()) return;

        auto fn = _targetFilename;
        auto paramStart = fn.find_last_of(':');
		auto end = fn.cend();
        if (paramStart != std::basic_string<ResChar>::npos)
			end = fn.cbegin() + paramStart;

        AssetServices::GetAsyncMan().GetIntermediateStore().ShadowFile(MakeStringSection(&(*fn.cbegin()), &(*end)));
    }


    ITransaction::ITransaction(StringSection<ResChar> name, uint64_t assetId, uint64_t typeCode, std::shared_ptr<UndoQueue> undoQueue)
	: _undoQueue(std::move(undoQueue))
    , _name(name.AsString()), _assetId(assetId), _typeCode(typeCode)
	{}

	ITransaction::~ITransaction()
	{}

	void UndoQueue::PushBack(std::shared_ptr<ITransaction> transaction) {}
    std::shared_ptr<ITransaction> UndoQueue::GetTop() { return nullptr; }
    unsigned UndoQueue::GetCount() { return 0; }
    ITransaction* UndoQueue::GetTransaction(unsigned) { return nullptr; }
	UndoQueue::UndoQueue() {}
	UndoQueue::~UndoQueue() {}

	DivergentAssetBase::DivergentAssetBase(std::weak_ptr<UndoQueue> undoQueue)
	: _undoQueue(std::move(undoQueue))
	{}

	DivergentAssetBase::~DivergentAssetBase() {}

}

