#pragma once

#include "Neutrino/API.h"
#include "Neutrino/AssetsCore.h"

#include <memory>
#include <functional>
#include <vector>
#include <assert.h>

namespace Assets
{
    class DependencyValidation; class DependentFileState; 
    namespace IntermediateAssets { class CompilerSet; class Store; }
    class ArchiveCache;

    class DRAGORA_NEUTRINO_API IPollingAsyncProcess
    {
    public:
        struct Result { enum Enum { KeepPolling, Finish }; };
        virtual Result::Enum Update() = 0;

        IPollingAsyncProcess();
        virtual ~IPollingAsyncProcess();
    };

    class DRAGORA_NEUTRINO_API IThreadPump
    {
    public:
        virtual void Update() = 0;
        virtual ~IThreadPump();
    };

    class DRAGORA_NEUTRINO_API CompileAndAsyncManager
    {
    public:
        void Update();

        void Add(const std::shared_ptr<IPollingAsyncProcess>& pollingProcess);
        void Add(std::unique_ptr<IThreadPump>&& threadPump);

        IntermediateAssets::Store&			GetIntermediateStore();
        IntermediateAssets::CompilerSet&	GetIntermediateCompilers();
		IntermediateAssets::Store&			GetShadowingStore();

        CompileAndAsyncManager();
        ~CompileAndAsyncManager();
    protected:
		class Pimpl;
		std::unique_ptr<Pimpl> _pimpl;
    };

}

