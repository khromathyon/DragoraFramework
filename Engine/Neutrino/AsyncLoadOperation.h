#pragma once

#include "Neutrino/AssetUtils.h"
#include "Platform/MemoryHeap.h"

#include <memory>

namespace Assets
{
    class AsyncLoadOperation
    {
    public:
        static void Enqueue(const std::shared_ptr<AsyncLoadOperation>& op, Dragora::StringSection<ResChar> filename);

        AsyncLoadOperation();
        virtual ~AsyncLoadOperation();

        AsyncLoadOperation(const AsyncLoadOperation&) = delete;
        AsyncLoadOperation& operator=(const AsyncLoadOperation&) = delete;

    protected:
        const uint8_t* GetBuffer() const;
        size_t GetBufferSize() const;

        mutable ResChar _filename[260];

        virtual void Complete(const void* buffer, size_t bufferSize) = 0;
		virtual void OnFailure() = 0;
    private:
        std::unique_ptr<uint8_t[], Dragora::PODAlignMemFree> _buffer;
        size_t _bufferLength;
        mutable bool _hasBeenQueued;
    };   
}

