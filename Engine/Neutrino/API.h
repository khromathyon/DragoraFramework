#pragma once

#include "Platform/System.h"


#if DRAGORA_SHARED
# ifdef DRAGORA_NEUTRINO_EXPORTS
#  define DRAGORA_NEUTRINO_API DRAGORA_API_EXPORT
# else
#  define DRAGORA_NEUTRINO_API DRAGORA_API_IMPORT
# endif
#else
# define DRAGORA_NEUTRINO_API
#endif
