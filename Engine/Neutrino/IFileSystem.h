#pragma once

#include "Neutrino/API.h"

#include "Platform/File.h"
#include "Foundation/Stream.h"
#include "Foundation/String.h"
#include "Platform/Exception.h"
#include "Runtime/FileSystemMonitor.h"

#include <string>
#include <memory>

namespace Dragora { class OnChangeCallback; }

namespace Assets
{
	class FileDesc;
	class MountingTree;
	class IFileInterface;
	using IFileMonitor = Dragora::OnChangeCallback;

	static const Dragora::FileMode FileShareMode_Default = Dragora::FileModes::Read;

	/// <summary>Interface for interacting with a file</summary>
	/// A file can be a physical file on disk, or any logical object that behaves like a file.
	/// IFileInterface objects are typically returned from IFileSystem implementations as a result
	/// of an "open" operation.
	///
	/// This provides typical file system behaviour, such as reading, writing, searching and
	/// getting description information.
	class DRAGORA_NEUTRINO_API IFileInterface
	{
	public:
		virtual size_t			Write(const void * source, size_t size, size_t count = 1) never_throws = 0;
		virtual size_t			Read(void * destination, size_t size, size_t count = 1) const never_throws = 0;
		virtual ptrdiff_t		Seek(ptrdiff_t seekOffset, Dragora::SeekOrigin fileMode = Dragora::SeekOrigins::Begin) never_throws = 0;
		virtual size_t			TellP() const never_throws = 0;

		virtual FileDesc		GetDesc() const never_throws = 0;

		virtual 			   ~IFileInterface();
	};

	/// <summary>Interface for a mountable virtual file system</summary>
	/// Provides a generic way to access different types of resources in a file-system like way.
	/// Typical implementions include things like archive files and "virtual" memory-based files.
	/// But the underlying OS filesystem is accessed via a IFileSystem, as well.
	///
	/// File systems can be mounted via a MountingTree. This works much like the *nix virtual
	/// file system (where new file systems can be mounted under any filespec prefix).
	/// 
	/// IFileSystem can be compared to the interfaces in the /fs/ tree of linux. Some of the 
	/// functions provide similar functionality. It's possible that we could build an adapter
	/// to allow filesystem implementations from linux to mounted as a IFileSystem.
	/// Howver, note that IFileSystem is intended mostly for input. So there are no functions for 
	/// things like creating or removing directories.
	class DRAGORA_NEUTRINO_API IFileSystem
	{
	public:
		using Marker = std::vector<uint8_t>;

		enum class TranslateResult { Success, Mounting, Invalid };
		virtual TranslateResult		TryTranslate(Marker& result, Dragora::StringSection<char> filename) = 0;
		virtual TranslateResult		TryTranslate(Marker& result, Dragora::StringSection<wchar_t> filename) = 0;

		enum class IOReason
		{
			Success,
			FileNotFound,
			AccessDenied, 
			WriteProtect,
			Mounting,
			Invalid,
			Complex
		};

		virtual IOReason	TryOpen(std::unique_ptr<IFileInterface>& result, const Marker& marker, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default) = 0;
		virtual IOReason	TryOpen(std::unique_ptr<Dragora::Stream>& result, const Marker& marker, const char openMode[], Dragora::FileMode shareMode = FileShareMode_Default) = 0;
		virtual IOReason	TryOpen(std::unique_ptr<Dragora::DynamicArray<uint8_t>>& result, const Marker& marker, uint64_t size, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default) = 0;

		virtual	IOReason	TryMonitor(const Marker& marker, const std::shared_ptr<IFileMonitor>& evnt) = 0;
		virtual	FileDesc	TryGetDesc(const Marker& marker) = 0;
		virtual				~IFileSystem();
	};

	/// <summary>Description of a file object within a filesystem</summary>
	/// Typically files have a few basic properties that can be queried.
	/// But note the "files" in this sense can mean more than just files on disk.
	/// So some properties will not apply to all files.
	/// Also note that some filesystems can map multiple names onto the same object.
	/// (for example, a filesystem that is not case sensitive will map all case variations
	/// onto the same file).
	/// In cases like this, the "_naturalName" below represents the form closest to how
	/// the object is stored internally.
	class DRAGORA_NEUTRINO_API FileDesc
	{
	public:
		/// <summary>State of a file</summary>
		/// There are a few basic states that apply to all files. Here, DoesNotExist and
		/// Normal are clear. Mounting means that the file is not currently accessible, but
		/// is expected to be available later. This can happen if the filesystem is still
		/// mounting in a background thread, or there is some background operation that 
		/// must be completed before the resource can be used.
		/// Invalid is used in rare cases where the resource does not currently exist, and
		/// can never exist. This is typically required by filesystems that processing source
		/// resources into output resources.
		///
		/// Consider a filesystem that performs texture compression. A filesystem request may
		/// load a source texture and attempt to compile it. In this case "Mounting" will be
		/// returned while the texture is being compiled in a background thread. When the 
		/// compression is completed, "Normal" will be returned. If an appropriate source
		/// file does not exist, then "DoesNotExist" will be returned. If a source file does
		/// exist, but that source file is corrupt or invalid, then "Invalid" will be returned.
		///
		/// However, note that this is not a complete list of states for all files. Even if
		/// a file returns state "Normal", open operations may still fail. They can fail for
		/// filesystem specific problems (such as a permissions error)
		enum class State { DoesNotExist, Normal, Mounting, Invalid };

		std::basic_string<char>	_naturalName;
		State					_state;
		uint64_t				_modificationTime;
		uint64_t				_size;
	};

	/// <summary>Provides access to the global mounting tree</summary>
	/// The global mounting tree is the default mounting tree used to resolve file requests made by
	/// code in this process. It can be thought of as similar to the file system namespace for the 
	/// current process in Linux.
	///
	/// File requests that can't be resolved by the mounting tree (eg, absolute paths and paths beginning
	/// with a drive name) are passed onto a default filesystem (which is typically just raw access to 
	/// the underlying OS filesystem).
	class DRAGORA_NEUTRINO_API MainFileSystem
	{
	public:
		using IOReason = IFileSystem::IOReason;

		static IOReason	TryOpen(std::unique_ptr<IFileInterface>& result, Dragora::StringSection<char> filename, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default);
		static IOReason	TryOpen(std::unique_ptr<Dragora::Stream>& result, Dragora::StringSection<char> filename, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default);
		static IOReason	TryOpen(std::unique_ptr<Dragora::DynamicArray<uint8_t>>& result, Dragora::StringSection<char> filename, uint64_t size, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default);
		static IOReason	TryMonitor(Dragora::StringSection<char> filename, const std::shared_ptr<IFileMonitor>& evnt);
		static FileDesc	TryGetDesc(Dragora::StringSection<char> filename);

		static std::unique_ptr<Dragora::Stream> OpenBasicFile(Dragora::StringSection<char> filename, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default);
		static std::unique_ptr<Dragora::DynamicArray<uint8_t>> OpenMemoryMappedFile(Dragora::StringSection<char> filename, uint64_t size, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default);
		static std::unique_ptr<IFileInterface> OpenFileInterface(Dragora::StringSection<char> filename, const char openMode[], Dragora::FileMode shareMode = FileShareMode_Default);

		static IOReason	TryOpen(std::unique_ptr<IFileInterface>& result, Dragora::StringSection<wchar_t> filename, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default);
		static IOReason	TryOpen(std::unique_ptr<Dragora::Stream>& result, Dragora::StringSection<wchar_t> filename, const char openMode[], Dragora::FileMode  shareMode=FileShareMode_Default);
		static IOReason	TryOpen(std::unique_ptr<Dragora::DynamicArray<uint8_t>>& result, Dragora::StringSection<wchar_t> filename, uint64_t size, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default);
		static IOReason	TryMonitor(Dragora::StringSection<wchar_t> filename, const std::shared_ptr<IFileMonitor>& evnt);
		static FileDesc	TryGetDesc(Dragora::StringSection<wchar_t> filename);

		static const std::shared_ptr<MountingTree>& GetMountingTree();
		static void Init(std::shared_ptr<MountingTree>& mountingTree, std::shared_ptr<IFileSystem>& defaultFileSystem);

		/*
		static IOReason	TryOpen(std::unique_ptr<IFileInterface>& result, Dragora::StringSection<char> filename, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default)
			{ return TryOpen(result, Dragora::MakeStringSection((const char*)filename.begin(), (const char*)filename.end()), openMode, shareMode); }
		static IOReason	TryOpen(std::unique_ptr<Dragora::Stream>& result, Dragora::StringSection<char> filename, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default)
			{ return TryOpen(result, Dragora::MakeStringSection((const char*)filename.begin(), (const char*)filename.end()), openMode, shareMode); }
		static IOReason	TryOpen(std::unique_ptr<Dragora::DynamicArray<uint8_t>>& result, Dragora::StringSection<char> filename, uint64_t size, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default)
			{ return TryOpen(result, Dragora::MakeStringSection((const char*)filename.begin(), (const char*)filename.end()), size, openMode, shareMode); }
		static IOReason	TryMonitor(Dragora::StringSection<char> filename, const std::shared_ptr<IFileMonitor>& evnt)
			{ return TryMonitor(Dragora::MakeStringSection((const char*)filename.begin(), (const char*)filename.end()), evnt); }
		static FileDesc	TryGetDesc(Dragora::StringSection<char> filename)
			{ return TryGetDesc(Dragora::MakeStringSection((const char*)filename.begin(), (const char*)filename.end())); }

		static std::unique_ptr<Dragora::Stream> OpenBasicFile(Dragora::StringSection<char> filename, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default)
			{ return OpenBasicFile(Dragora::MakeStringSection((const char*)filename.begin(), (const char*)filename.end()), openMode, shareMode); }
		static std::unique_ptr<Dragora::DynamicArray<uint8_t>> OpenMemoryMappedFile(Dragora::StringSection<char> filename, uint64_t size, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default)
			{ return OpenMemoryMappedFile(Dragora::MakeStringSection((const char*)filename.begin(), (const char*)filename.end()), size, openMode, shareMode); }
		static std::unique_ptr<IFileInterface> OpenFileInterface(Dragora::StringSection<char> filename, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default)
			{ return OpenFileInterface(Dragora::MakeStringSection((const char*)filename.begin(), (const char*)filename.end()), openMode, shareMode); }*/
	};

	template<typename CharType, typename FileObject> IFileSystem::IOReason TryOpen(FileObject& result, IFileSystem& fs, Dragora::StringSection<CharType> fn, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default);
	template<typename CharType, typename FileObject> IFileSystem::IOReason TryOpen(FileObject& result, IFileSystem& fs, Dragora::StringSection<CharType> fn, uint64_t size, const char openMode[], Dragora::FileMode shareMode=FileShareMode_Default);
	template<typename CharType> IFileSystem::IOReason TryMonitor(IFileSystem& fs, Dragora::StringSection<CharType> fn, const std::shared_ptr<IFileMonitor>& evnt);
	template<typename CharType> FileDesc TryGetDesc(IFileSystem& fs, Dragora::StringSection<CharType> fn);

	std::unique_ptr<uint8_t[]> TryLoadFileAsMemoryBlock(Dragora::StringSection<char> sourceFileName, size_t* sizeResult = nullptr);

}

