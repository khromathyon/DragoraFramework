#include "AssetTraits.h"
#include "Assets.h"
#include "ConfigFileContainer.h"
#include "ChunkFileContainer.h"

using namespace Dragora;

namespace Assets { namespace Internal
{
	//TODO: IMplement formatters and fix it.
	/*
	const ConfigFileContainer<>& GetConfigFileContainer(StringSection<ResChar> identifier)
	{
		return ::Assets::GetAsset<ConfigFileContainer<>>(identifier);
	}
	*/

	const ChunkFileContainer& GetChunkFileContainer(StringSection<ResChar> identifier)
	{
		return ::Assets::GetAsset<ChunkFileContainer>(identifier);
	}
}}

