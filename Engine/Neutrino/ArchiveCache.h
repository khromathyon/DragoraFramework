#pragma once

#include <memory>
#include <vector>
#include <functional>

#include "Neutrino/API.h"
#include "Platform/Types.h"
#include "Platform/Locks.h"

#define ARCHIVE_CACHE_ATTACHED_STRINGS

namespace Assets
{
    class ArchiveDirectoryBlock;

    class DRAGORA_NEUTRINO_API ArchiveCache
    {
    public:
        typedef std::shared_ptr<std::vector<uint8_t>> BlockAndSize;

        void            Commit(uint64_t id, BlockAndSize&& data, const std::string& attachedString, std::function<void()>&& onFlush);
        BlockAndSize    TryOpenFromCache(uint64_t id);
        bool            HasItem(uint64_t id) const;
        void            FlushToDisk();
        
        class BlockMetrics
        {
        public:
            uint64_t _id;
            unsigned _offset, _size;
            std::string _attachedString;
        };
        class Metrics
        {
        public:
            unsigned _allocatedFileSize;
            unsigned _usedSpace;
            std::vector<BlockMetrics> _blocks;
        };

        /// <summary>Return profiling related breakdown</summary>
        /// Designed to be used for profiling archive usage and stats.
        Metrics GetMetrics() const;

        ArchiveCache(const char archiveName[], const char buildVersionString[], const char buildDateString[]);
        ~ArchiveCache();

        ArchiveCache(const ArchiveCache&) = delete;
        ArchiveCache& operator=(const ArchiveCache&) = delete;
        ArchiveCache(ArchiveCache&&) = delete;
        ArchiveCache& operator=(ArchiveCache&&) = delete;

    protected:
        class PendingCommit
        {
        public:
            uint64_t         _id;
            BlockAndSize    _data;
            unsigned        _pendingCommitPtr;      // (only used during FlushToDisk)
            std::function<void()> _onFlush;

            #if defined(ARCHIVE_CACHE_ATTACHED_STRINGS)
                std::string     _attachedString;    // used for appending debugging/profiling information. user defined format
            #endif

            PendingCommit() {}
            PendingCommit(uint64_t id, BlockAndSize&& data, const std::string& attachedString, std::function<void()>&& onFlush);
            PendingCommit(PendingCommit&& moveFrom);
            PendingCommit& operator=(PendingCommit&& moveFrom);

        private:
            PendingCommit(const PendingCommit&);
            PendingCommit& operator=(const PendingCommit&);
        };

        mutable Dragora::Mutex _pendingBlocksLock; // TODO: Can be SpinLock?
        std::vector<PendingCommit> _pendingBlocks;
        std::basic_string<char> _mainFileName, _directoryFileName;

        const char*     _buildVersionString;
        const char*     _buildDateString;

        class ComparePendingCommit
        {
        public:
            bool operator()(const PendingCommit& lhs, uint64_t rhs) { return lhs._id < rhs; }
            bool operator()(uint64_t lhs, const PendingCommit& rhs) { return lhs < rhs._id; }
            bool operator()(const PendingCommit& lhs, const PendingCommit& rhs) { return lhs._id < rhs._id; }
        };

        mutable std::vector<ArchiveDirectoryBlock> _cachedBlockList;
        mutable bool _cachedBlockListValid;
        const std::vector<ArchiveDirectoryBlock>* GetBlockList() const;
    };


}
