#include "KamabokoScriptingEnvironment.h"

#include "Foundation/String.h"

static unsigned short g_Init = 0;
KamabokoScriptRuntime* Dragora::KamabokoScriptRuntime::sm_Instance = NULL;

using namespace Dragora;

void KamabokoScriptRuntime::Startup()
{

}

void KamabokoScriptRuntime::Shutdown()
{

}

KamabokoScriptRuntime& KamabokoScriptRuntime::GetInstance()
{
	DRAGORA_ASSERT(sm_Instance);
	return *sm_Instance;
}