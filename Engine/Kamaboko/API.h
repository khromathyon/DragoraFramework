#pragma once

#include "Platform/System.h"

#if DRAGORA_SHARED
# ifdef DRAGORA_KAMABOKO_EXPORTS
#  define DRAGORA_KAMABOKO_API DRAGORA_API_EXPORT
# else
#  define DRAGORA_KAMABOKO_API DRAGORA_API_IMPORT
# endif
#else
# define DRAGORA_KAMABOKO_API
#endif
