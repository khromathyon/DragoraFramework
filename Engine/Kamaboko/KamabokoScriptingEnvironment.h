#pragma once

#include "Kamaboko/API.h"


namespace Dragora
{
	class DRAGORA_KAMABOKO_API KamabokoScriptRuntime
	{
	public:
		/// Static Access
		static void Startup();
		static void Shutdown();
		static KamabokoScriptRuntime& GetInstance();

		///Available Scripting Languages
		typedef enum
		{
			KAMABOKO_ANGELSCRIPT,
			KAMABOKO_SKOOKUMSCRIPT
		} ScriptingLanguages;

		typedef enum
		{
			BINDINGS_INTEGRATED_COMMON, // Register Functions from Foundation, Platform
			BINDINGS_INTEGRATED_ENGINE, // Registers Functions from the Engine Core (Neutrino, Runtime and Ramune)
			BINDINGS_KAMABOKO_HELPERS, // Registers 
			BINDINGS_EXTERNALLY_OTHER
		};
		/// Bindings Registers.

	
	private:
		static KamabokoScriptRuntime* sm_Instance;
	};
}