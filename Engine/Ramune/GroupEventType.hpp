#pragma once

namespace Ramune
{
enum class GroupEventType
{
	OnEntityAdded,
	OnEntityRemoved,
	OnEntityAddedOrRemoved
};
}
