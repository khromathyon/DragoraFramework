// Copyright (c) 2016 Juan Delgado (JuDelCo)
// License: MIT License
// MIT License web page: https://opensource.org/licenses/MIT

#pragma once

#include "Ramune/API.h"
#include "Ramune/Entity.hpp"

namespace Ramune
{
    class Matcher;
    class TriggerOnEvent;
    typedef std::vector<Matcher> MatcherList;

    class DRAGORA_RAMUNE_API Matcher
    {
        public:
            Matcher() = default;
            static auto AllOf(const ComponentIdList indices) -> const Matcher;
            static auto AllOf(const MatcherList matchers) -> const Matcher;
            static auto AnyOf(const ComponentIdList indices) -> const Matcher;
            static auto AnyOf(const MatcherList matchers) -> const Matcher;
            static auto NoneOf(const ComponentIdList indices) -> const Matcher;
            static auto NoneOf(const MatcherList matchers) -> const Matcher;

            bool IsEmpty() const;
            bool Matches(const EntityPtr& entity);
            auto GetIndices() -> const ComponentIdList;
            auto GetAllOfIndices() const -> const ComponentIdList;
            auto GetAnyOfIndices() const -> const ComponentIdList;
            auto GetNoneOfIndices() const -> const ComponentIdList;

            auto GetHashCode() const -> unsigned int;
            bool CompareIndices(const Matcher& matcher) const;

            auto OnEntityAdded() -> const TriggerOnEvent;
            auto OnEntityRemoved() -> const TriggerOnEvent;
            auto OnEntityAddedOrRemoved() -> const TriggerOnEvent;

            bool operator ==(const Matcher right) const;

        protected:
            void CalculateHash();

            ComponentIdList mIndices;
            ComponentIdList mAllOfIndices;
            ComponentIdList mAnyOfIndices;
            ComponentIdList mNoneOfIndices;

        private:
            auto ApplyHash(unsigned int hash, const ComponentIdList indices, int i1, int i2) const -> unsigned int;
            auto MergeIndices() const -> ComponentIdList;
            static auto MergeIndices(MatcherList matchers) -> ComponentIdList;
            static auto DistinctIndices(ComponentIdList indices) -> ComponentIdList;

            unsigned int mCachedHash{0};
    };
}

namespace std
{
template <>
struct hash<Ramune::Matcher>
{
	std::size_t operator()(const Ramune::Matcher& matcher) const
	{
		return hash<unsigned int>()(matcher.GetHashCode());
	}
};
}

namespace
{
#define FUNC_1(MODIFIER, X) MODIFIER(X)
#define FUNC_2(MODIFIER, X, ...) MODIFIER(X), FUNC_1(MODIFIER, __VA_ARGS__)
#define FUNC_3(MODIFIER, X, ...) MODIFIER(X), FUNC_2(MODIFIER, __VA_ARGS__)
#define FUNC_4(MODIFIER, X, ...) MODIFIER(X), FUNC_3(MODIFIER, __VA_ARGS__)
#define FUNC_5(MODIFIER, X, ...) MODIFIER(X), FUNC_4(MODIFIER, __VA_ARGS__)
#define FUNC_6(MODIFIER, X, ...) MODIFIER(X), FUNC_5(MODIFIER, __VA_ARGS__)
#define GET_MACRO(_1, _2, _3, _4, _5, _6, NAME,...) NAME
#define FOR_EACH(MODIFIER,...) GET_MACRO(__VA_ARGS__, FUNC_6, FUNC_5, FUNC_4, FUNC_3, FUNC_2, FUNC_1)(MODIFIER, __VA_ARGS__)

#define COMPONENT_GET_TYPE_ID(COMPONENT_CLASS) Ramune::ComponentTypeId::Get<COMPONENT_CLASS>()
#define Matcher_AllOf(...) (Ramune::Matcher)Ramune::Matcher::AllOf(std::vector<Ramune::ComponentId>({ FOR_EACH(COMPONENT_GET_TYPE_ID, __VA_ARGS__) }))
#define Matcher_AnyOf(...) (Ramune::Matcher)Ramune::Matcher::AnyOf(std::vector<Ramune::ComponentId>({ FOR_EACH(COMPONENT_GET_TYPE_ID, __VA_ARGS__) }))
#define Matcher_NoneOf(...) (Ramune::Matcher)Ramune::Matcher::NoneOf(std::vector<Ramune::ComponentId>({ FOR_EACH(COMPONENT_GET_TYPE_ID, __VA_ARGS__) }))
}
