#pragma once

#include "Ramune/CommonComponents/TransformComponent.h"

using namespace Dragora;
using namespace Ramune;

TransformComponent::TransformComponent()
	: m_Pos(Math::Vector3::Zero)
	, m_Scale(Math::Vector3::Zero)
	, m_Rot(Math::Quaternion::Identity)
{
}

TransformComponent::TransformComponent(const Math::Vector3& pos,
	const Math::Vector3& scale,
	const Math::Quaternion& rot)
	: m_Pos(pos)
	, m_Scale(scale)
	, m_Rot(rot)
{

}

void TransformComponent::Set(const Math::Vector3& pos, const Math::Vector3& scale, const Math::Quaternion& rot)
{
	m_Pos = pos;
	m_Scale = scale;
	m_Rot = rot;
}
