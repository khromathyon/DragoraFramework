#pragma once

#include "Ramune/API.h"
#include "Ramune/IComponent.hpp"

#include "KMath/KMath.h"

namespace Dragora
{
	class DRAGORA_RAMUNE_API TransformComponent : public Ramune::IComponent
	{
	public:
		TransformComponent();
		TransformComponent(const Math::Vector3& pos, 
						   const Math::Vector3& scale,
						   const Math::Quaternion& rot);

		/// Setters and Getters
		void Set(const Math::Vector3& pos,
				 const Math::Vector3& scale,
				 const Math::Quaternion& rot);

		inline void setPosition(const Math::Vector3& pos) { m_Pos = pos; }
		inline void setScale(const Math::Vector3& scale)  { m_Scale = scale; }
		inline void setRotation(const Math::Quaternion& rot) { m_Rot = rot; }

		inline Math::Vector3& getPosition() { return m_Pos; }
		inline Math::Vector3& getScale() { return m_Scale; }
		inline Math::Quaternion& getRotation() { return m_Rot; }
		
	private:
		Math::Vector3 m_Pos;
		Math::Vector3 m_Scale;
		Math::Quaternion m_Rot;
	};
}