#pragma once

#include "Ramune/API.h"

namespace Ramune
{
    class DRAGORA_RAMUNE_API IComponent
    {
        friend class Entity;

        protected:
            IComponent() = default;
    };
}
