#pragma once

#include "Platform/System.h"


#if DRAGORA_SHARED
# ifdef DRAGORA_RAMUNE_EXPORTS
#  define DRAGORA_RAMUNE_API DRAGORA_API_EXPORT
# else
#  define DRAGORA_RAMUNE_API DRAGORA_API_IMPORT
# endif
#else
# define DRAGORA_RAMUNE_API
#endif
