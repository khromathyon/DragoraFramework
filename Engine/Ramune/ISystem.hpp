#pragma once

#include "Ramune/API.h"
#include "Ramune/Entity.hpp"
#include "Ramune/Matcher.hpp"
#include "Ramune/TriggerOnEvent.hpp"
#include <vector>

namespace Ramune
{
    class Pool;

    class DRAGORA_RAMUNE_API ISystem
    {
        protected:
            ISystem() = default;

        public:
            virtual ~ISystem() = default;
    };

    class DRAGORA_RAMUNE_API ISetPoolSystem
    {
        protected:
            ISetPoolSystem() = default;

        public:
            virtual ~ISetPoolSystem() = default;

            virtual void SetPool(Pool* pool) = 0;
    };

    class DRAGORA_RAMUNE_API IInitializeSystem
    {
        protected:
            IInitializeSystem() = default;

        public:
            virtual ~IInitializeSystem() = default;

            virtual void Initialize() = 0;
    };

    class DRAGORA_RAMUNE_API IExecuteSystem : public ISystem
    {
        protected:
            IExecuteSystem() = default;

        public:
            virtual ~IExecuteSystem() = default;

            virtual void Execute() = 0;
    };

    class DRAGORA_RAMUNE_API IFixedExecuteSystem : public ISystem
    {
        protected:
            IFixedExecuteSystem() = default;

        public:
            virtual ~IFixedExecuteSystem() = default;

            virtual void FixedExecute() = 0;
    };

    class DRAGORA_RAMUNE_API IReactiveExecuteSystem : public ISystem
    {
        protected:
            IReactiveExecuteSystem() = default;

        public:
            virtual ~IReactiveExecuteSystem() = default;

            virtual void Execute(std::vector<EntityPtr> entities) = 0;
    };

    class DRAGORA_RAMUNE_API IReactiveSystem : public IReactiveExecuteSystem
    {
        public:
            virtual ~IReactiveSystem() = default;

            TriggerOnEvent trigger;
    };

    class DRAGORA_RAMUNE_API IMultiReactiveSystem : public IReactiveExecuteSystem
    {
        public:
            virtual ~IMultiReactiveSystem() = default;

            std::vector<TriggerOnEvent> triggers;
    };

    class DRAGORA_RAMUNE_API IEnsureComponents
    {
        protected:
            IEnsureComponents() = default;

        public:
            Matcher ensureComponents;
    };

    class DRAGORA_RAMUNE_API IExcludeComponents
    {
        protected:
            IExcludeComponents() = default;

        public:
            Matcher excludeComponents;
    };

    class DRAGORA_RAMUNE_API IClearReactiveSystem
    {
        protected:
            IClearReactiveSystem() = default;
    };
}
