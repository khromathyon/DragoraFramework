CMAKE_MINIMUM_REQUIRED( VERSION 2.6 )
PROJECT( Binn C )

if(EXISTS ${CMAKE_SOURCE_DIR}/makefile)
  file(REMOVE ${CMAKE_SOURCE_DIR}/makefile)
endif()


IF("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU" OR
   "${CMAKE_C_COMPILER_ID}" STREQUAL "Clang")
  SET(GNU_COMPATIBLE_COMPILER 1)
ENDIF()

if(GNU_COMPATIBLE_COMPILER)
if(UNIX)
    add_definitions(-fPIC)
endif()
endif()


set(BINN_SRC src/binn.h src/binn.c)
add_library(Binn STATIC ${BINN_SRC})

install(FILES
  src/binn.h
  DESTINATION ${CMAKE_INSTALL_PREFIX}/include)

install(TARGETS Binn
	ARCHIVE DESTINATION ${CMAKE_INSTALL_PREFIX}/lib)
