project(ZStd)
cmake_minimum_required (VERSION 2.6)

include_directories(lib/)
include_directories(lib/common)
include_directories(lib/compress)
include_directories(lib/decompress)


macro( add_recursive dir retVal )
    file( GLOB_RECURSE ${retVal} ${dir}/*.h ${dir}/*.hpp ${dir}/*.cpp ${dir}/*.c ${dir}/*.inl)
endmacro()

IF("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU" OR
   "${CMAKE_C_COMPILER_ID}" STREQUAL "Clang")
  SET(GNU_COMPATIBLE_COMPILER 1)
ENDIF()

if(GNU_COMPATIBLE_COMPILER)
if(UNIX)
    add_definitions(-fPIC)
endif()
endif()

# Adding common,compress, decompress directories:
add_recursive(lib/common ZSTD_COMMON_SOURCES)
add_recursive(lib/compress ZSTD_COMPRESS_SOURCES)
add_recursive(lib/decompress ZSTD_DECOMPRESS_SOURCES)

set(ZSTD_LIBRARY_SOURCES ${ZSTD_COMMON_SOURCES} ${ZSTD_COMPRESS_SOURCES} ${ZSTD_DECOMPRESS_SOURCES} lib/zstd.h)

add_library(zstd STATIC ${ZSTD_LIBRARY_SOURCES})

file( GLOB ZSTD_COMMON_INCLUDES lib/common/*.h )
file( GLOB ZSTD_COMPRESS_INCLUDES lib/compress/*.h )
file( GLOB ZSTD_DECOMPRESS_INCLUDES lib/decompress/*.h)

install(FILES
${ZSTD_COMMON_INCLUDES}
DESTINATION ${CMAKE_INSTALL_PREFIX}/include/common
)

install(FILES
${ZSTD_COMPRESS_INCLUDES}
DESTINATION ${CMAKE_INSTALL_PREFIX}/include/compress
)

install(FILES
${ZSTD_DECOMPRESS_INCLUDES}
DESTINATION ${CMAKE_INSTALL_PREFIX}/include/decompress
)

set(LIB_DIR lib/)

install(FILES
${LIB_DIR}/zstd.h
DESTINATION ${CMAKE_INSTALL_PREFIX}/include
)

install(TARGETS zstd
ARCHIVE DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
)

