project(lz4 C)

cmake_minimum_required (VERSION 2.6)

INCLUDE (CheckTypeSize)
check_type_size("void *" SIZEOF_VOID_P)
IF( ${SIZEOF_VOID_P} STREQUAL  "8" )
    set (CMAKE_SYSTEM_PROCESSOR "64bit")
    MESSAGE( STATUS "64 bit architecture detected size of void * is " ${SIZEOF_VOID_P})
ENDIF()

IF("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU" OR
   "${CMAKE_C_COMPILER_ID}" STREQUAL "Clang")
  SET(GNU_COMPATIBLE_COMPILER 1)
ENDIF()

if(GNU_COMPATIBLE_COMPILER)
if(UNIX)
    add_definitions(-fPIC)
endif()
endif()

set(LZ4_DIR lib/)
set(LZ4_SRCS_LIB ${LZ4_DIR}lz4.c ${LZ4_DIR}lz4hc.c ${LZ4_DIR}lz4.h ${LZ4_DIR}lz4hc.h ${LZ4_DIR}lz4frame.c ${LZ4_DIR}lz4frame.h ${LZ4_DIR}xxhash.c)

add_library(LZ4 STATIC ${LZ4_SRCS_LIB})
install(FILES
${LZ4_DIR}/lz4.h
${LZ4_DIR}/lz4hc.h
${LZ4_DIR}/lz4frame.h
DESTINATION ${CMAKE_INSTALL_PREFIX}/include
)

install(TARGETS LZ4
ARCHIVE DESTINATION ${CMAKE_INSTALL_PREFIX}/lib
)
