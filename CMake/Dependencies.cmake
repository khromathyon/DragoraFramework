include(ExternalProject)

# Helpful variables.
if(MSVC)
  set(LIBPREFIX)
  set(STATICLIB_EXT .lib)
  set(DYNAMICLIB_EXT .dll)
else()
  set(LIBPREFIX lib)
  set(STATICLIB_EXT .a)
  set(DYNAMICLIB_EXT .so)
endif()

# ================ Fixed-dependencies
if(DRAGORA_BUILDCONFIG_MEMORYALLOCATOR MATCHES "ltalloc" )
	add_recursive(3rdParty/ltalloc LTALLOC_SOURCES)
	DRAGORA_CONFIGURE_STATIC_LIBRARY(ltalloc LTALLOC_SOURCES NONE NONE Dependencies)
endif()

add_recursive(3rdParty/LUrlParser LURLPARSER_SOURCES)
DRAGORA_CONFIGURE_STATIC_LIBRARY(LUrlParser LURLPARSER_SOURCES NONE NONE Dependencies)

add_recursive(3rdParty/miniz MINIZ_SOURCES)
DRAGORA_CONFIGURE_STATIC_LIBRARY(MiniZ MINIZ_SOURCES NONE NONE Dependencies)

# Slap Qt5 components/widgets needed to be built here.
if(DRAGORA_BUILDCONFIG_ENABLETOOLS)
	add_subdirectory(./3rdParty/QtPropertyBrowser)
	add_subdirectory(./3rdParty/burger-menu)
endif()

# ================ Build-time Downloadable Dependencies
# EASTL
if(UNIX)
    set(ENABLE_FPIC -DCMAKE_CXX_FLAGS=-fPIC ${CMAKE_CXX_FLAGS}) # NOTE: Dirty Af.
endif()

ExternalProject_Add(
  External.EASTL
  PREFIX ${CMAKE_BINARY_DIR}/DownloadedDeps/
  URL https://github.com/electronicarts/EASTL/archive/3.05.00.zip
  CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/Dependencies/EASTL ${ENABLE_FPIC}
  BUILD_IN_SOURCE 1
)

set_target_properties(External.EASTL PROPERTIES FOLDER Dependencies)

# Setting up imported targets.
add_library( EASTL STATIC IMPORTED) # The library will always be static.
set_property(TARGET EASTL PROPERTY IMPORTED_LOCATION ${CMAKE_BINARY_DIR}/Dependencies/EASTL/lib/${LIBPREFIX}EASTL${STATICLIB_EXT})
set_property(TARGET EASTL PROPERTY FOLDER Dependencies)
add_dependencies(EASTL External.EASTL)

set(EASTL_INCLUDES ${CMAKE_CURRENT_BINARY_DIR}/Dependencies/EASTL/include)


# cJSON

# This flag disables the capacity of building an shared library in MSVC due cJSON doesn't export any symbol via declspec.
if(MSVC)
 set(DISABLE_SHAREDLIB_BUILD -DBUILD_SHARED_LIBS=OFF)
endif()

ExternalProject_Add(
	External.cJSON
	PREFIX ${CMAKE_BINARY_DIR}/DownloadedDeps/
	URL https://github.com/DaveGamble/cJSON/archive/v1.1.0.zip
	CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DENABLE_CJSON_UTILS=ON -ENABLE_CJSON_TEST=OFF -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/Dependencies/cJSON ${DISABLE_SHAREDLIB_BUILD}
	BUILD_IN_SOURCE 1
)
 
set_target_properties(External.cJSON PROPERTIES FOLDER Dependencies)

# Setting up imported targets.
if(UNIX)
add_library( cJSON SHARED IMPORTED ) # we build the cJSON library as shared library.
add_library( cJSON_Utils SHARED IMPORTED ) # Also we built the cJSON utility library.

set_property( TARGET cJSON       PROPERTY IMPORTED_LOCATION ${CMAKE_BINARY_DIR}/Dependencies/cJSON/lib/${LIBPREFIX}cjson${DYNAMICLIB_EXT})
set_property( TARGET cJSON_Utils PROPERTY IMPORTED_LOCATION ${CMAKE_BINARY_DIR}/Dependencies/cJSON/lib/${LIBPREFIX}cjson_utils${DYNAMICLIB_EXT})

else()

add_library( cJSON STATIC IMPORTED ) # we build the cJSON library as static library.
add_library( cJSON_Utils STATIC IMPORTED ) # Also we built the cJSON utility library.

set_property( TARGET cJSON       PROPERTY IMPORTED_LOCATION ${CMAKE_BINARY_DIR}/Dependencies/cJSON/lib/${LIBPREFIX}cjson${STATICLIB_EXT})
set_property( TARGET cJSON_Utils PROPERTY IMPORTED_LOCATION ${CMAKE_BINARY_DIR}/Dependencies/cJSON/lib/${LIBPREFIX}cjson_utils${STATICLIB_EXT})

endif()
	
add_dependencies(cJSON       External.cJSON)
add_dependencies(cJSON_Utils External.cJSON)

set( CJSON_INCLUDES ${CMAKE_BINARY_DIR}/Dependencies/cJSON/include/)

# Binn
ExternalProject_Add(
    External.Binn
    PREFIX ${CMAKE_BINARY_DIR}/DownloadedDeps/
    URL https://github.com/liteserver/binn/archive/master.zip
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}/Dependencies/binn
    BUILD_IN_SOURCE 1
    PATCH_COMMAND ${CMAKE_COMMAND} -E copy
            ${CMAKE_SOURCE_DIR}/CMake/Patches/Binn.cmake ${CMAKE_BINARY_DIR}/DownloadedDeps/src/External.Binn/CMakeLists.txt
    )
set_target_properties(External.Binn PROPERTIES FOLDER Dependencies)

# Setting up imported targets.
add_library(Binn STATIC IMPORTED)
set_property(TARGET Binn PROPERTY IMPORTED_LOCATION ${CMAKE_BINARY_DIR}/Dependencies/binn/lib/${LIBPREFIX}Binn${STATICLIB_EXT})
set_property(TARGET Binn PROPERTY FOLDER Dependencies)
add_dependencies(Binn External.Binn)

set(BINN_INCLUDE_DIR ${CMAKE_BINARY_DIR}/Dependencies/binn/include)

# xxHash
ExternalProject_Add(
    External.xxHash
    PREFIX ${CMAKE_BINARY_DIR}/DownloadedDeps
    URL https://github.com/Cyan4973/xxHash/archive/v0.6.2.zip
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}/Dependencies/xxHash
    PATCH_COMMAND ${CMAKE_COMMAND} -E copy
         ${CMAKE_SOURCE_DIR}/CMake/Patches/xxHash.cmake ${CMAKE_BINARY_DIR}/DownloadedDeps/src/External.xxHash/CMakeLists.txt

    BUILD_IN_SOURCE 1
    )
    
set_target_properties(External.xxHash PROPERTIES FOLDER Dependencies)

# Setting up imported targets for xxHash
add_library(xxHash STATIC IMPORTED)
set_property(TARGET xxHash PROPERTY IMPORTED_LOCATION  ${CMAKE_BINARY_DIR}/Dependencies/xxHash/lib/${LIBPREFIX}xxhash${STATICLIB_EXT})
set_property(TARGET xxHash PROPERTY FOLDER Dependencies)
add_dependencies(External.xxHash xxHash)

set(XXHASH_INCLUDE_DIR  ${CMAKE_BINARY_DIR}/Dependencies/xxHash/include)

include_directories(${CMAKE_BINARY_DIR}/Dependencies/)

# Cpptoml
if(NOT EXISTS ${CMAKE_BINARY_DIR}/Dependencies/cpptoml/cpptoml.h)
	FILE(DOWNLOAD https://raw.githubusercontent.com/skystrife/cpptoml/master/include/cpptoml.h ${CMAKE_BINARY_DIR}/Dependencies/cpptoml/cpptoml.h)
endif()
# Catch Testing Framework
if(DRAGORA_BUILDCONFIG_ENABLETESTS)
	if(NOT EXISTS ${CMAKE_BINARY_DIR}/Dependencies/Catch/catch.hpp) 
		FILE(DOWNLOAD https://raw.githubusercontent.com/philsquared/Catch/master/single_include/catch.hpp ${CMAKE_BINARY_DIR}/Dependencies/Catch/catch.hpp)
	endif()
endif()

