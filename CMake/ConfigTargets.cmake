# Determine if we can embed ltalloc in our binaries...
# TODO: Implement Foonathan's memory library but as shared/static library instead of object one.
# NOTE: Should make ltalloc as static library?

# Somehow those lines below crashes CMake (3.6.2).
if(DRAGORA_BUILDCONFIG_MEMORYALLOCATOR MATCHES ltalloc)
  set(TARGET_MEMORY_ALLOCATOR ltalloc)
else()
  set(TARGET_MEMORY_ALLOCATOR) # None.
endif()

macro( add_recursive dir retVal )
  # *.ui files for Qt.
  if(NOT APPLE)
    file( GLOB_RECURSE ${retVal} ${dir}/*.h ${dir}/*.hpp ${dir}/*.cpp ${dir}/*.c ${dir}/*.cc ${dir}/*.inl ${dir}/*.ui ${dir}/*.qrc)
  else()
    file( GLOB_RECURSE ${retVal} ${dir}/*.h ${dir}/*.hpp ${dir}/*.cpp ${dir}/*.c ${dir}/*.cc ${dir}/*.inl ${dir}/*.ui ${dir}/*.mm ${dir}/*.m  ${dir}/*.qrc)
  endif()
endmacro()

macro( add_simple dir retVal )
  # *.ui files for Qt.
  if(NOT APPLE)
    file( GLOB ${retVal} ${dir}/*.h ${dir}/*.hpp ${dir}/*.cpp ${dir}/*.c ${dir}/*.inl ${dir}/*.ui ${dir}/*.qrc)
  else()
    file( GLOB ${retVal} ${dir}/*.h ${dir}/*.hpp ${dir}/*.cpp ${dir}/*.c ${dir}/*.inl ${dir}/*.ui ${dir}/*.mm ${dir}/*.m  ${dir}/*.qrc)
  endif()
endmacro()


FUNCTION(DRAGORA_CONFIGURE_MODULE TargetName ExportName SourceFileList LibraryLinkList ObjLinkList FolderName )
  add_library(${TargetName} ${DRAGORA_LIBRARY_TYPE} ${${SourceFileList}} ${${ObjLinkList}}  )
  
  STRING(TOUPPER ${ExportName} EXPORTNAME_UPPER )
  if(DRAGORA_BUILDCONFIG_SHAREDLIBRARIES)
    target_compile_definitions(${TargetName} PRIVATE DRAGORA_${EXPORTNAME_UPPER}_EXPORTS)
  endif()
  list(LENGTH ${LibraryLinkList} num_link_libs)
  if(${num_link_libs} GREATER 0)
    target_link_libraries(${TargetName} ${TARGET_MEMORY_ALLOCATOR} ${${LibraryLinkList}})
  endif()
  set_target_properties(${TargetName} PROPERTIES FOLDER ${FolderName})

  if("${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" AND NOT APPLE)
      set_property(TARGET ${TargetName} PROPERTY POSITION_INDEPENDENT_CODE TRUE)
  endif()
ENDFUNCTION()


#TODO: Set icon and version information i guess for Windows..
FUNCTION(DRAGORA_CONFIGURE_EXECUTABLE TargetName SourceFileList LibraryLinkList ObjLinkList FolderName isConsole)
  if(${isConsole} EQUAL FALSE)
    set(EXE_TYPE WIN32)
  else()
    set(EXE_TYPE)
  endif()
  
 add_executable(${TargetName} WIN32 ${${SourceFileList}}  ${${ObjLinkList}})

  list(LENGTH ${LibraryLinkList} num_link_libs)
  if(${num_link_libs} GREATER 0)
    target_link_libraries(${TargetName} ${TARGET_MEMORY_ALLOCATOR} ${${LibraryLinkList}})
  endif()
  set_target_properties(${TargetName} PROPERTIES FOLDER ${FolderName})
ENDFUNCTION()

#NOTE: Don't use in components of the engine which has their own Export macros!
FUNCTION(DRAGORA_CONFIGURE_STATIC_LIBRARY TargetName SourceFileList LibraryLinkList ObjLinkList FolderName )
    add_library(${TargetName} STATIC ${${SourceFileList}}  ${${ObjLinkList}})
    list(LENGTH ${LibraryLinkList} num_link_libs)
    if(${num_link_libs} GREATER 0)
      target_link_libraries(${TargetName} ${TARGET_MEMORY_ALLOCATOR} ${${LibraryLinkList}})
    endif()
    set_target_properties(${TargetName} PROPERTIES FOLDER ${FolderName})

    if("${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" AND NOT APPLE)
        set_property(TARGET ${TargetName} PROPERTY POSITION_INDEPENDENT_CODE TRUE)
    endif()
ENDFUNCTION()

FUNCTION(DRAGORA_CONFIGURE_OBJECT_LIBRARY TargetName SourceFileList  ObjLinkList FolderName )
    add_library(${TargetName} OBJECT ${${SourceFileList}}  ${TARGET_MEMORY_ALLOCATOR} ${${ObjLinkList}})
    set_target_properties(${TargetName} PROPERTIES FOLDER ${FolderName})

    if("${CMAKE_SYSTEM_PROCESSOR}" STREQUAL "x86_64" AND NOT APPLE)
        set_property(TARGET ${TargetName} PROPERTY POSITION_INDEPENDENT_CODE TRUE)
    endif()
ENDFUNCTION()

# Function: DRAGORA_SET_PROJECT_AS_WIP_BROKEN(TargetName)
# This function disables the target to be built in the ALL_BUILD and default builds, the only way is building the mentioned
# target explicitely, this is useful when the project itself is still really premature and doesn't able to build yet.
function(DRAGORA_SET_PROJECT_AS_WIP_BROKEN TargetName)
    set_property( TARGET ${TargetName} PROPERTY EXCLUDE_FROM_ALL TRUE)
    set_property( TARGET ${TargetName} PROPERTY EXCLUDE_FROM_DEFAULT_BUILD TRUE)

    MESSAGE(STATUS "NOTE: Project ${TargetName} is set as WIP/Broken and can't be built, this project will be not enabled/built from default and BUILD_ALL targets")
endfunction()

function (createSrcGroups FILE_LIST )
  # we want to get the relative path from the
  # current source dir
  string(LENGTH ${CMAKE_CURRENT_SOURCE_DIR} curDirLen)
  set(TMP_FILE_LIST ${${FILE_LIST}})

  foreach ( SOURCE ${TMP_FILE_LIST} )
    string(LENGTH ${SOURCE} fullPathLen)
    math(EXPR RelPathLen ${fullPathLen}-${curDirLen})
    string(SUBSTRING ${SOURCE} ${curDirLen} ${RelPathLen} curStr)

    string ( REGEX REPLACE "[\\/]" "\\\\" normPath ${curStr} )
    string ( REGEX MATCH "\\\\(.*)\\\\" ouput ${normPath} )
    if(NOT CMAKE_MATCH_1 STREQUAL "")
	  string(LENGTH ${CMAKE_MATCH_1} STR1_LENGTH)
	  if(STR1_LENGTH GREATER 2) # "./"
	    set(CHOP_LEN 2)
	  else()
	    #Chop dot.
		set(CHOP_LEN 1)
	  endif()
	
	    math(EXPR DotSlashLen ${STR1_LENGTH}-${CHOP_LEN})
		string(SUBSTRING ${CMAKE_MATCH_1} ${CHOP_LEN} ${DotSlashLen} SRCGRP_NAME)
		source_group ( ${SRCGRP_NAME} FILES ${SOURCE} )
    endif()
  endforeach()
endfunction()

SET(NONE) #Filer.

