#ifndef ASSETBROWSERFORM_H
#define ASSETBROWSERFORM_H

#include <QWidget>

namespace Ui {
class AssetBrowserForm;
}

class AssetBrowserForm : public QWidget
{
    Q_OBJECT

public:
    explicit AssetBrowserForm(QWidget *parent = 0);
    ~AssetBrowserForm();

private:
    Ui::AssetBrowserForm *ui;
};

#endif // ASSETBROWSERFORM_H
