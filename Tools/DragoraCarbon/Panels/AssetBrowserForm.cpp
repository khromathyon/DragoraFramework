#include "AssetBrowserForm.h"
#include "ui_AssetBrowserForm.h"

AssetBrowserForm::AssetBrowserForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AssetBrowserForm)
{
    ui->setupUi(this);
}

AssetBrowserForm::~AssetBrowserForm()
{
    delete ui;
}
