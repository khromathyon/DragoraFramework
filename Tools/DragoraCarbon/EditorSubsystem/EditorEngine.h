#pragma once

#include "Foundation/Event.h"
#include "Platform/Utility.h"

class QMenu;
class QMenuBar;

namespace Dragora
{
	/*
	Since there's no main or master window in our current editor which handles all the events or access to other
	parts of our game engine who requires one so will make our own in a singleton, yeah, it's ugly but ya know the 
	famous quote: "It's just works".
	*/
	class EditorSubsystemCentral : NonCopyable
	{
	public:
		static void Startup();
		static void Shutdown();
		static EditorSubsystemCentral& GetInstance();

	private:
		static EditorSubsystemCentral* sm_pInstance;

		EditorSubsystemCentral();
		~EditorSubsystemCentral();

		//Event Methods.
		void OnProjectCreatedOrOpened(Dragora::Void);
		void OnProjectClosed(Dragora::Void);

	};
}