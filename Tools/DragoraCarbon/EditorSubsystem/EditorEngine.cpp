#include "EditorEngine.h"

#include "EditorSubsystem/EditorEngine.h"
#include "CarbonRuntime/ProjectManager.h"
#include "OGRE2GraphicsSystem/OGRE2GraphicsManager.h"

#include "Foundation/FilePath.h"
#include "Platform/Process.h"

#include "cJSON/cJSON.h"

using namespace Dragora;

static unsigned short g_Init = 0;
EditorSubsystemCentral* EditorSubsystemCentral::sm_pInstance = NULL;

void EditorSubsystemCentral::Startup()
{
	if (++g_Init == 1)
	{
		DRAGORA_ASSERT(!sm_pInstance);
		sm_pInstance = new EditorSubsystemCentral;
		DRAGORA_ASSERT(sm_pInstance);
	}
}

void EditorSubsystemCentral::Shutdown()
{
	if (--g_Init == 0)
	{
		DRAGORA_ASSERT(sm_pInstance);
		delete sm_pInstance;
		sm_pInstance = NULL;
	}
}

EditorSubsystemCentral& EditorSubsystemCentral::GetInstance()
{
	DRAGORA_ASSERT(sm_pInstance);
	return *sm_pInstance;
}

EditorSubsystemCentral::EditorSubsystemCentral()
{
	ProjectManager::GetInstance().e_OnProjectCreated.AddMethod(this, &EditorSubsystemCentral::OnProjectCreatedOrOpened);
	ProjectManager::GetInstance().e_OnProjectOpened.AddMethod(this, &EditorSubsystemCentral::OnProjectCreatedOrOpened);
	ProjectManager::GetInstance().e_OnProjectClosed.AddMethod(this, &EditorSubsystemCentral::OnProjectClosed);
}

EditorSubsystemCentral::~EditorSubsystemCentral()
{
}

void EditorSubsystemCentral::OnProjectCreatedOrOpened(Dragora::Void)
{
	FilePath pluginPath;
#ifdef DRAGORA_OS_WIN
	pluginPath = FilePath(GetProcessPath()).Directory();
#else
	pluginPath = FilePath("/usr/local/lib/OGRE/");
#endif

	OGRE2GraphicsManager::Startup();

	FilePath::GuaranteeSeparator(pluginPath);
	/*
	OGRE2GRAPHICSMGR.setExtensionsPath(pluginPath);
	OGRE2GRAPHICSMGR.PrepareGraphicsSettings();
	OGRE2GRAPHICSMGR.setStockHLMSPath(pluginPath);

	OGRE2GRAPHICSMGR.FinalizeGraphicsSettings(1, false);
	*/
}	

void EditorSubsystemCentral::OnProjectClosed(Dragora::Void)
{
	EditorSubsystemCentral::Shutdown();
}