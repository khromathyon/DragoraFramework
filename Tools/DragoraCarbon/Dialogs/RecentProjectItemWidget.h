#ifndef RECENTPROJECTITEMWIDGET_H
#define RECENTPROJECTITEMWIDGET_H

#include <QWidget>

namespace Ui {
class RecentProjectItemWidget;
}

class RecentProjectItemWidget : public QWidget
{
    Q_OBJECT

public:
    explicit RecentProjectItemWidget(QWidget *parent = 0);
    ~RecentProjectItemWidget();

	QString ProjectFileLocation; // Screw Getters and Setters, this is not Java and we don't need validation.
	void setProjectFile(const QString& file);

private:
    Ui::RecentProjectItemWidget *ui;
};

#endif // RECENTPROJECTITEMWIDGET_H
