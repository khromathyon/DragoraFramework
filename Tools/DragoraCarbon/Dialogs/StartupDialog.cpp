#include "StartupDialog.h"
#include "ui_StartupDialog.h"

#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>

#include "CarbonRuntime/ProjectManager.h"
#include "Dialogs/RecentProjectItemWidget.h"

#include "EditorWindows/EditorMainWindow.h"

using namespace Dragora;

StartupDialog::StartupDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StartupDialog)
	, mainWnd(NULL)
{

    ui->setupUi(this);

	connect(ui->listWidget, &QListWidget::itemActivated, this, &StartupDialog::openRecentProjectItem);
	connect(ui->OpenOtherBtn, &QToolButton::clicked, this, &StartupDialog::openAnotherProject);
	connect(ui->NewProjectBtn, &QToolButton::clicked, this, &StartupDialog::newProjectAction);
	
	connect(ui->createProjectToolBtn, &QToolButton::clicked, this, &StartupDialog::createProject);
	connect(ui->cancelProjectCreate, &QToolButton::clicked, this, &StartupDialog::cancelCreateProject);
	connect(ui->ProjectLocationBrowseBtn, &QToolButton::clicked, this, &StartupDialog::browseNewProjectLocation);
	connect(ui->pageBtn_NewProject, &QToolButton::clicked, this, &StartupDialog::newProjectAction);

	checkEntries();
}

StartupDialog::~StartupDialog()
{
    delete ui;
}

void StartupDialog::checkEntries()
{
	//Determine if we have recent entries...
	QSettings s;
	QStringList items = s.value("RecentProjects").toStringList();

	//Check for orphan items (the project referenced not longer exists, moved or removed)
	for (int i = 0; i < items.size(); i++)
	{
		QFileInfo info(items[i]);
		if (!info.exists())
			items.removeAt(i);
	}

	int count = items.count();
	if (count > 0)
	{
		ui->stackedWidget->setCurrentWidget(ui->RecentProjectsPage);
		PopulateRecentProjectItems();
	}
	else
		ui->stackedWidget->setCurrentWidget(ui->NoProjectsPage);

}

void StartupDialog::closeAndOpenMainWindow()
{
	//Startup the EditorSubsystemCentral!
	mainWnd = new EditorMainWindow();
	mainWnd->show();
	this->close();
}

void StartupDialog::PopulateRecentProjectItems()
{
	ui->listWidget->clear();

	QSettings s;
	QStringList list = s.value("RecentProjects").toStringList();

	for (int i = 0; i < list.size(); i++)
	{
		RecentProjectItemWidget* item = new RecentProjectItemWidget();
		QListWidgetItem* lItem = new QListWidgetItem(ui->listWidget);

		item->setProjectFile(list[i]);
		lItem->setData(Qt::UserRole, list[i]);
		ui->listWidget->setItemWidget(lItem, item);
		lItem->setSizeHint(item->sizeHint());
		ui->listWidget->addItem(lItem);
	}
}

void StartupDialog::addNewRecentProjectEntry(const QString& path)
{
	QSettings s;
	QStringList list = s.value("RecentProjects").toStringList();
	list.removeAll(path);
	list.prepend(path);

	s.setValue("RecentProjects", list);
}

void StartupDialog::openAnotherProject(bool)
{
	QString filePath = QFileDialog::getOpenFileName(
        this, tr("Open DragoraCarbon Project"), "",
        tr("DragoraCarbon Projects (*.dcpj)"));

	if (!filePath.isEmpty())
	{
		addNewRecentProjectEntry(filePath);
        Dragora::ProjectManager::GetInstance().OpenProject(filePath);

		closeAndOpenMainWindow();
	}

}

void StartupDialog::newProjectAction(bool)
{
	ui->stackedWidget->setCurrentWidget(ui->NewProjectPage);
}

void StartupDialog::createProject(bool)
{
	QString projName = ui->projectNameLineEdit->text();
	QString projLocation = ui->projectLocationLineEdit->text();

	if (projName.isEmpty() && projLocation.isEmpty())
	{
		QMessageBox::critical(this, "Invalid or empty fields", "Seems the Project Location or/and Project Name are empty, please fill before continuing!");
		return;
	}

    ProjectManager::GetInstance().CreateNewProject(projName.toStdString().c_str(), projLocation);
	closeAndOpenMainWindow();
}

void StartupDialog::cancelCreateProject(bool)
{
	ui->projectLocationLineEdit->clear();
	ui->projectNameLineEdit->clear();

	checkEntries();
}

void StartupDialog::browseNewProjectLocation(bool)
{
	QString filePath = QFileDialog::getExistingDirectory(
		this, tr("Open Existing directory"), "");

	ui->projectLocationLineEdit->setText(filePath);
}

void StartupDialog::openRecentProjectItem(QListWidgetItem* item)
{
	QString filePath = item->data(Qt::UserRole).toString();
	
    ProjectManager::GetInstance().OpenProject(filePath);
	closeAndOpenMainWindow();
}
