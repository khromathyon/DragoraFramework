#include "RecentProjectItemWidget.h"
#include "ui_RecentProjectItemWidget.h"

#include <QFileInfo>

RecentProjectItemWidget::RecentProjectItemWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RecentProjectItemWidget)
{
    ui->setupUi(this);
	ui->label->setAttribute(Qt::WA_TranslucentBackground);
	ui->label_2->setAttribute(Qt::WA_TranslucentBackground);
	ui->label_3->setAttribute(Qt::WA_TranslucentBackground);
}

RecentProjectItemWidget::~RecentProjectItemWidget()
{
    delete ui;
}

void RecentProjectItemWidget::setProjectFile(const QString & file)
{
	ProjectFileLocation = file;
	QFileInfo info(file);

	ui->label->setText(info.fileName());
	ui->label_2->setText(file);
}
