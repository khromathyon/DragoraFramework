#ifndef STARTUPDIALOG_H
#define STARTUPDIALOG_H

#include <QDialog>

class QListWidgetItem;
class EditorMainWindow;

namespace Ui {
class StartupDialog;
}


class StartupDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StartupDialog(QWidget *parent = 0);
    ~StartupDialog();

private slots:
	void openRecentProjectItem(QListWidgetItem* item);
	void openAnotherProject(bool);
	void newProjectAction(bool);

	void createProject(bool);
	void cancelCreateProject(bool);
	void browseNewProjectLocation(bool);

private:
    Ui::StartupDialog *ui;
	EditorMainWindow* mainWnd;

	void checkEntries();
	void closeAndOpenMainWindow();
	void PopulateRecentProjectItems();
	void addNewRecentProjectEntry(const QString& path);

};

#endif // STARTUPDIALOG_H
