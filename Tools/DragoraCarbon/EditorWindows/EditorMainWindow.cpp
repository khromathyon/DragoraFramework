#include "EditorMainWindow.h"
#include "ui_EditorMainWindow.h"

#include "EditorWindows/AssetBrowserWindow.h"

EditorMainWindow::EditorMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EditorMainWindow)
{
	ui->setupUi(this);
	auto ass = new AssetBrowserWindow();
	ass->show();
}

EditorMainWindow::~EditorMainWindow()
{
    delete ui;
}
