#pragma once

#include <QApplication>
#include <QKeyEvent>
#include <QWindow>

#include <OGRE/Ogre.h>

class EditorCameraAdministrator;
class OGREContextSwitcher;

class QOgreWindow : public QWindow, public Ogre::FrameListener
{
	Q_OBJECT

public:
	explicit QOgreWindow(QWindow* parent = NULL);
	~QOgreWindow();

	virtual void render(QPainter *painter);
	virtual void render();
	virtual void initialize();
	virtual void createScene();
	virtual void createCompositor();
	
	void setAnimating(bool animating);

public slots:
	virtual void renderLater();
	virtual void renderNow();

	/*
	We use an event filter to be able to capture keyboard/mouse events. More on this later.
	*/
	virtual bool eventFilter(QObject *target, QEvent *event);

protected:
	Ogre::Root* m_ogreRoot;
	Ogre::RenderWindow* m_ogreWindow;
	Ogre::SceneManager* m_ogreSceneMgr;	
	Ogre::Camera* m_ogreCamera;
	Ogre::ColourValue m_ogreBackground;

	bool m_update_pending;
	bool m_animating;

	/*
	The below methods are what is actually fired when they keys on the keyboard are hit.
	Similar events are fired when the mouse is pressed or other events occur.
	*/
	virtual void keyPressEvent(QKeyEvent * ev);
	virtual void keyReleaseEvent(QKeyEvent * ev);
	virtual void mouseMoveEvent(QMouseEvent* e);
	virtual void wheelEvent(QWheelEvent* e);
	virtual void mousePressEvent(QMouseEvent* e);
	virtual void mouseReleaseEvent(QMouseEvent* e);
	virtual void exposeEvent(QExposeEvent *event);
	virtual bool event(QEvent *event);

	/*
	FrameListener method
	*/
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

	/*
	Write log messages to Ogre log
	*/

};
