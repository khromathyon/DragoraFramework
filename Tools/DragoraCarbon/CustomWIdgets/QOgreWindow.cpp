#include "QOgreWindow.h"

#include <OGRE/Compositor/OgreCompositorManager2.h>

QOgreWindow::QOgreWindow(QWindow* parent)
	: QWindow(parent)
	, m_update_pending(false)
	, m_animating(false)
	, m_ogreRoot(NULL)
	, m_ogreWindow(NULL)
	, m_ogreCamera(NULL)
{
	setAnimating(true);
	installEventFilter(this);
	m_ogreBackground = Ogre::ColourValue(0.0f, 0.5f, 1.0f);
}

QOgreWindow::~QOgreWindow()
{

}

void QOgreWindow::render(QPainter * painter)
{
}

void QOgreWindow::render()
{
}

void QOgreWindow::initialize()
{
}

void QOgreWindow::createScene()
{
}

void QOgreWindow::createCompositor()
{
}

void QOgreWindow::setAnimating(bool animating)
{
}

void QOgreWindow::renderNow()
{
}

void QOgreWindow::renderLater()
{
}

bool QOgreWindow::eventFilter(QObject * target, QEvent * event)
{
	return false;
}

void QOgreWindow::keyPressEvent(QKeyEvent * ev)
{
}

void QOgreWindow::keyReleaseEvent(QKeyEvent * ev)
{
}

void QOgreWindow::mouseMoveEvent(QMouseEvent * e)
{
}

void QOgreWindow::wheelEvent(QWheelEvent * e)
{
}

void QOgreWindow::mousePressEvent(QMouseEvent * e)
{
}

void QOgreWindow::mouseReleaseEvent(QMouseEvent * e)
{
}

void QOgreWindow::exposeEvent(QExposeEvent * event)
{
}

bool QOgreWindow::event(QEvent * event)
{
	return false;
}

bool QOgreWindow::frameRenderingQueued(const Ogre::FrameEvent & evt)
{
	return false;
}
