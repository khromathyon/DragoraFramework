#include <QApplication>

#include "CarbonRuntime/ProjectManager.h"
#include "Dialogs/StartupDialog.h"

#include "Platform/Trace.h"

#include "EditorSubsystem/EditorEngine.h"
#include "Runtime/RuntimeInfo.h"

#include "Appearance.h"

#include <QSettings>
#include <QFile>
#include <QTextStream>

#define USE_STATIC_PALETTES

using namespace Dragora;

int main(int argc, char *argv[])
{
    DRAGORA_TRACE_SET_LEVEL( TraceLevel::Debug );

    QCoreApplication::setOrganizationName("Khromathyon Software");
    QCoreApplication::setApplicationName("DragoraCarbon");

	//Apply to RuntimeInfo classes too.
	RuntimeInfo::SetRuntimeInfo("DragoraCarbon", "Khromathyon Software", "");

    QApplication a(argc, argv);

#ifndef USE_STATIC_PALETTES
    QFile f(":qdarkstyle/style.qss");
	if (f.exists())
	{
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}
	else
		DRAGORA_TRACE(TraceLevels::Warning, "Unable to set stylesheet, file not found\n");
#else
	Appearance::setWontonSoup(a);
#endif

	QSettings::setDefaultFormat(QSettings::IniFormat);
    a.setQuitOnLastWindowClosed(true);

	//NOTE: The order of those two lines DOES MATTER.
    ProjectManager::Startup();
	EditorSubsystemCentral::Startup();
	StartupDialog* dlg = new StartupDialog();
	dlg->show();
	
    return a.exec();
}

