#pragma once

#include "CarbonRuntime/CarbonRuntimeAPI.h"

#include "Foundation/String.h"
#include "Foundation/Map.h"
#include "Foundation/Functions.h"

namespace Dragora
{
    class Document;

    typedef void (*EditorCallFunctionPtr)(const String&);

    // This class will register the document type name, the associated extension, and what editor opens/saves the
    // such document types.
    class DRAGORA_CARBONRUNTIME_API DocumentTypeRegistry
    {
    public:
        struct DocumentTypeEntry
        {
            String documentTypeExtension;
            EditorCallFunctionPtr editorFuncPtr;
        };

        bool RegisterDocumentType( const String& documentTypeName, const String& documentTypeExtension,
                                   EditorCallFunctionPtr editorFuncPtr );
        bool UnregisterDocumentType( const String& documentTypeName );
        bool GetDocumentTypeEntry(const String& documentTypeName, DocumentTypeEntry& outEntry);


    private:
        Map<String, DocumentTypeEntry> m_Entries;
    };
}
