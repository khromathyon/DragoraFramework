#pragma once

#include "CarbonRuntime/CarbonRuntimeAPI.h"

#include "Foundation/Event.h"
#include "Foundation/String.h"

#include "cjson/cJSON.h"

#include <QString>

namespace Dragora
{
    class DocumentManager;

    // The simple and stupid Project Manager.
    class DRAGORA_CARBONRUNTIME_API ProjectManager
    {
    public:
        static void Startup();
        static void Shutdown();
        static ProjectManager& GetInstance();

        // Project Management
        bool CreateNewProject(String name, QString location);
        bool OpenProject(const QString& location);
        void CloseProject();
        void SaveChanges();

        // Project Data Functions.
        inline bool IsProjectCurrentlyOpen() { return m_activeProject; }
        inline bool IsProjectVanilla() { return m_isVanillaProject; }

        inline QString& GetProjectFile() { return m_ProjectFile;}
        inline QString& GetProjectRootDir() { return m_ProjectFile; }

        // Project JSON Object Access.
        void triggerModified();
        cJSON* GetRootJSONObject();

        // Access to the document manager.
        inline Dragora::DocumentManager* DocumentManager() { return m_DocManager; }

        // Events
        VoidSignature::Event e_OnProjectCreated;
        VoidSignature::Event e_OnProjectOpened;
        VoidSignature::Event e_OnProjectSaved;
        VoidSignature::Event e_OnProjectModified;
        VoidSignature::Event e_OnProjectClosed;

    private:
        ProjectManager();

        static ProjectManager* ms_Instance;

        bool m_isVanillaProject;
        bool m_activeProject;

        QString m_ProjectFile;
        QString m_ProjectRootDir;

        cJSON* m_ProjectJSONRootObj;
        Dragora::DocumentManager* m_DocManager;
    };
}
