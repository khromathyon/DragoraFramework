#include "DocumentManager.h"

#include "Platform/Assert.h"
#include "Foundation/Flags.h"
#include "Foundation/Log.h"

#include <algorithm>
#include <cctype>
#include <sstream>

#include <QMessageBox>
#include <QFileDialog>

using namespace Dragora;

Document::Document(const std::string& path, const char* docType)
	: m_Path( path )
	, m_DocumentStatus( DocumentStatus::Default )
	, m_AllowUnsavableChanges( false )
	, m_Revision( -1 )
	, m_docType(docType)
{
}

Document::~Document()
{
}

///////////////////////////////////////////////////////////////////////////////
bool Document::Save( std::string& error )
{
	SetFlag<uint32_t>( m_DocumentStatus, DocumentStatus::Saving, true );

	bool result = false;

	DocumentEventArgs savingArgs( this );
	e_Saving.Raise( savingArgs );

	if ( !savingArgs.m_Veto )
	{
		DocumentEventArgs saveArgs( this, &error );
		d_Save.Invoke( saveArgs );
		if ( saveArgs.m_Result )
		{
			SetFlag<uint32_t>( m_DocumentStatus, DocumentStatus::Saving, false );

			e_Saved.Raise( DocumentEventArgs( this ) );

			HasChanged( false );

			result = true;
		}
	}

	SetFlag<uint32_t>( m_DocumentStatus, DocumentStatus::Saving, false );

	return result;
}

///////////////////////////////////////////////////////////////////////////////
void Document::Close()
{
	e_Closing.Raise( DocumentEventArgs( this ) );

	d_Close.Invoke( DocumentEventArgs( this ) );

	e_Closed.Raise( DocumentEventArgs( this ) );
}

///////////////////////////////////////////////////////////////////////////////
void Document::Checkout() const
{
	e_CheckedOut.Raise( DocumentEventArgs( this ) );
}

///////////////////////////////////////////////////////////////////////////////
// Sets the path to this file.  The name of the file is also updated.  Notifies
// any interested listeners about this event.
//
void Document::SetPath( const Dragora::FilePath& newPath )
{
	Dragora::FilePath oldPath( m_Path.Get() );

	m_Path = newPath;
	UpdateRCSFileInfo();

	e_PathChanged.Raise( DocumentPathChangedArgs( this, oldPath ) );
}

///////////////////////////////////////////////////////////////////////////////
uint32_t Document::GetStatus() const
{
	return m_DocumentStatus;
}

///////////////////////////////////////////////////////////////////////////////
bool Document::HasChanged() const
{
	return HasFlags<uint32_t>( m_DocumentStatus, DocumentStatus::Changed );
}

///////////////////////////////////////////////////////////////////////////////
// Sets the internal flag indicating the the file has been modified (thus it
// should probably be saved before closing).
//
void Document::HasChanged( bool changed )
{
	if ( HasFlags<uint32_t>( m_DocumentStatus, DocumentStatus::Changed ) != changed )
	{
		SetFlag<uint32_t>( m_DocumentStatus, DocumentStatus::Changed, changed );

		e_Changed.Raise( DocumentEventArgs( this ) );
	}
}

void Document::OnObjectChanged( const DocumentObjectChangedArgs& args )
{
	HasChanged( args.m_HasChanged );
}

///////////////////////////////////////////////////////////////////////////////
// Returns true if the file is currently checked out by this user.
//
bool Document::IsCheckedOut() const
{
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Checks to see if the local revision of this file is the same as the head
// revision, and returns true if so.
//
bool Document::IsUpToDate() const
{
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Returns true if the user has specified that they want to make changes to
// this file even if it is not checked out by them.
//
bool Document::AllowUnsavableChanges() const
{
	return m_AllowUnsavableChanges;
}

///////////////////////////////////////////////////////////////////////////////
// Sets whether to allow changes regardless of file check out state.
//
void Document::AllowUnsavableChanges( bool allowUnsavableChanges )
{
	m_AllowUnsavableChanges = allowUnsavableChanges;
}

///////////////////////////////////////////////////////////////////////////////
void Document::UpdateRCSFileInfo()
{
}

DocumentManager::DocumentManager(QWidget* parent)
	: m_QParent(parent)
{
}

///////////////////////////////////////////////////////////////////////////////
// Returns the first document found with the specified path.
//
Document* DocumentManager::FindDocument( const Dragora::FilePath& path ) const
{
	OS_DocumentSmartPtr::Iterator docItr = m_Documents.Begin();
	OS_DocumentSmartPtr::Iterator docEnd = m_Documents.End();
	for ( ; docItr != docEnd; ++docItr )
	{
		Document* document = *docItr;
		if ( document->GetPath() == path )
		{
			return document;
		}
	}
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////
// Returns true if it successfully opens a document with the specified path.
//
bool DocumentManager::OpenDocument( const DocumentPtr& document, std::string& error )
{
	if ( !document->GetPath().Empty() )
	{
		if ( FindDocument( document->GetPath() ) )
		{
			error = TXT( "The specified file (" ) + document->GetPath().Filename().Get() + TXT( ") is already open." );
			return false;
		}
	}

	AddDocument( document );
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Adds a document to the list managed by this object.  Returns false if the
// document was already in the list belonging to this manager.
//
bool DocumentManager::AddDocument( const DocumentPtr& document )
{
	if ( m_Documents.Append( document ) )
	{
		document->e_Closed.AddMethod( this, &DocumentManager::OnDocumentClosed );

		e_DocumentOpened.Raise( DocumentEventArgs( document ) );

		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
// Iterates over all the documents, calling save on each one.
//
bool DocumentManager::SaveAll( std::string& error )
{
	bool savedAll = true;
	bool prompt = true;
	bool dirtyDocuments = true;

	// here, we loop until we couldn't save any more documents (or there was a failure to save a document),
	// because saving a document can cause other documents to need saving again
	while ( dirtyDocuments && savedAll )
	{
		dirtyDocuments = false;
		OS_DocumentSmartPtr::Iterator docItr = m_Documents.Begin();
		OS_DocumentSmartPtr::Iterator docEnd = m_Documents.End();
		for ( ; docItr != docEnd; ++docItr )
		{
			Document* document = *docItr;

			bool abort = false;
			bool save = true;
			if ( prompt )
			{
				switch ( QuerySave( document ) )
				{
				case SaveActions::SaveAll:
					save = true;
					prompt = false;
					break;

				case SaveActions::Save:
					save = true;
					prompt = true;
					break;

				case SaveActions::Skip:
					save = false;
					prompt = true;
					break;

				case SaveActions::SkipAll:
					save = false;
				case SaveActions::Abort:
				default:
					abort = true;
					break;
				}
			}

			if ( abort )
			{
				break;
			}

			if ( save )
			{
				dirtyDocuments = true;

				std::string msg;
				if ( !SaveDocument( document, msg ) )
				{
					savedAll = false;
					if ( !error.empty() )
					{
						error += TXT( "\n" );
					}
					error += msg;
				}
			}
		}
	}

	return savedAll;
}

///////////////////////////////////////////////////////////////////////////////
// Saves the specified document and returns true if successful.
//
// Derived classes should HELIUM_OVERRIDE this function to actually perform saving
// data to disk as appropriate.  The base implementation fires the appropriate
// events.  A derived class may want to call this implementation if the save
// is successful.
//
bool DocumentManager::SaveDocument( DocumentPtr document, std::string& error )
{
	// Check for "save as"
	if ( document->GetPath().Empty() || !document->GetPath().IsAbsolute() )
	{
		std::string filters = std::string(document->GetDocType()) + " (*." + document->GetPath().Extension() + ")" ;

		//FileDialogArgs args ( FileDialogTypes::SaveFile, TXT("Save As..."), filters, FilePath( document->GetPath().Directory() ), FilePath( document->GetPath().Filename() ) );
		QString file = QFileDialog::getSaveFileName(m_QParent, "Save As...", QString(document->GetPath().Directory().Get().c_str()), QString(filters.c_str()));

		if ( !file.isEmpty() )
		{
			document->SetPath(file.toStdString() );
		}
		else
		{
			error = TXT( "Cancelled saving of document." );
			return false;
		}
	}

	if ( document->Save( error ) )
	{
		return true;
	}

	if ( error.empty() )
	{
		error = TXT( "Failed to save " ) + document->GetPath().Filename().Get();
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
// Closes all currently open documents.
//
bool DocumentManager::CloseAll()
{
	return CloseDocuments( m_Documents );
}

///////////////////////////////////////////////////////////////////////////////
// Iterates over all the documents, closing each one.  The user will be prompted
// to save any modified documents before closing them.  Returns true if all
// documents were successfully closed.
//
bool DocumentManager::CloseDocuments( OS_DocumentSmartPtr documents )
{
	// NOTE: We purposefully make a copy of the files to iterate over
	// because the original list may be changing as we close files,
	// which will invalidate our iterator.

	if ( documents.Empty() )
	{
		return true;
	}

	bool prompt = true;
	bool save = true;
	bool abort = false;

	OS_DocumentSmartPtr::ReverseIterator docItr = documents.ReverseBegin();
	OS_DocumentSmartPtr::ReverseIterator docEnd = documents.ReverseEnd();
	for ( ; docItr != docEnd && !abort; ++docItr )
	{
		const DocumentPtr& document = *docItr;

		if ( prompt )
		{
			SaveActions::SaveAction action = SaveActions::Abort;
			if ( documents.Size() == 1 )
			{
				action = QueryClose( document );
			}
			else
			{
				action = QueryCloseAll( document );
			}

			switch ( action )
			{
			case SaveActions::SaveAll:
				prompt = false;
				save = true;
				break;

			case SaveActions::Save:
				prompt = true;
				save = true;
				break;

			case SaveActions::SkipAll:
				prompt = false;
				save = false;
				break;

			case SaveActions::Skip:
				prompt = true;
				save = false;
				break;

			case SaveActions::Abort:
				abort = true;
				break;
			}
		}

		if ( abort )
		{
			return false;
		}

		if ( save )
		{
			//if ( RCS::PathIsManaged( document->GetPath().Get() ) )
			//{
			//	if ( !CheckOut( document ) )
			//	{
			//		return false;
			//	}
			//}

			std::string error;
			if ( !SaveDocument( document, error ) )
			{
				error += TXT( "\nAborting operation." );
				QMessageBox::critical(m_QParent, "Error", error.c_str());
				return false;
			}
		}

		if ( !CloseDocument( document, false ) )
		{
			std::string error;
			error = TXT( "Failed to close '" ) + document->GetPath().Filename().Get() + TXT( "'.  Aborting operation." );
			QMessageBox::critical(m_QParent, "Error", error.c_str());
			return false;
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Closes the specified file.  Determines if the file needs to be saved, and
// handles any other revision control interactions.  Notifies any interested
// listeners if the file is successfully closed.  If prompt is set to true,
// the user will be prompted to save their file before it is closed.
//
// Fires an event to notify listeners that this document is now closed.  Any
// objects that are holding pointers to this document should release them.
//
bool DocumentManager::CloseDocument( DocumentPtr document, bool prompt )
{
	DRAGORA_ASSERT( document.ReferencesObject() );

	bool shouldClose = !prompt;
	bool wasClosed = false;

	if ( prompt )
	{
		std::string unused;
		switch ( QueryClose( document ) )
		{
		case SaveActions::Save:
			shouldClose = SaveDocument( document, unused );
			break;

		case SaveActions::Skip:
			shouldClose = true;
			break;

		case SaveActions::Abort:
			shouldClose = false;
			break;

		case SaveActions::SaveAll:
		case SaveActions::SkipAll:
			break;
		}
	}

	if ( shouldClose )
	{
		// This will raise the e_Closed event when the document is finished closing and
		// callback to OnDocumentClosed, which will remove the document from m_Documents
		document->Close();

		wasClosed = true;
	}

	return wasClosed;
}

///////////////////////////////////////////////////////////////////////////////
// Removes the document from the list managed by this object.
// NOTE: The document may be deleted if no one is holding a smart pointer to it
// after this function is called.
//
bool DocumentManager::RemoveDocument( const DocumentPtr& document )
{
	document->e_Closed.RemoveMethod( this, &DocumentManager::OnDocumentClosed );

	if ( m_Documents.Remove( document ) )
	{
		e_DocumenClosed.Raise( DocumentEventArgs( document ) );
		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
// Callback for when a document is closed.  Removes the document from the list
// managed by this class.
//
void DocumentManager::OnDocumentClosed( const DocumentEventArgs& args )
{
	RemoveDocument( args.m_Document );
}

///////////////////////////////////////////////////////////////////////////////
// Call this function if the user fails to checkout a file, or attempts to edit
// a file without checking it out.  The user is prompted as to whether they want
// to edit the file (even though they won't be able to save).  Returns true if the
// user wants to allow changes, false otherwise.
//
bool DocumentManager::QueryAllowChanges( Document* document ) const
{
	if ( !document->AllowUnsavableChanges() && !document->IsCheckedOut() )
	{
		QueryCheckOut( document );
		if ( !document->IsCheckedOut() )
		{

			int res = QMessageBox::information(m_QParent, "Edit anyway?", "Would you like to edit this file anyway?\n(NOTE: You may not be able to save your changes)", QMessageBox::Yes);
			
			if ( QMessageBox::Yes == res)
			{
				document->AllowUnsavableChanges( true );
			}
		}
	}

	return document->AllowUnsavableChanges() || document->IsCheckedOut();
}

///////////////////////////////////////////////////////////////////////////////
// Returns true if this file is allowed to be changed.  The file is allowed to
// be changed if the user has it checked out, or if they chose to edit the file
// anyway when their attempt to check out the file failed.  See QueryAllowChanges.
//
bool DocumentManager::AllowChanges( Document* document ) const
{
	if ( document->IsCheckedOut() || document->AllowUnsavableChanges() )
	{
		return true;
	}

	return QueryAllowChanges( document );
}

///////////////////////////////////////////////////////////////////////////////
// Asks the user if they want to check out the file.  Returns true if the user
// opts not to check out the file.  Also returns true if the user chooses to
// check out the file, and it is successfully checked out.  Use this function
// when opening a file to see if it should be opened for edit.
//
bool DocumentManager::QueryCheckOut( Document* document ) const
{
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Checks a file out from revision control.  Returns false if unable to check
// out the file.
//
bool DocumentManager::CheckOut( Document* document ) const
{
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Returns a flag indicating whether or not to continue the open process
// (not the checkout status).
//
bool DocumentManager::QueryOpen( Document* document ) const
{
	// File is not in source control, and shouldn't be (perhaps it's on the user's desktop
	// or something, so just let it be opened).
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// Checks to see if this file should be in revision control.  If the file should
// be in revision control, but isn't, the user is prompted to add the file.
// Returns false if the user attempts to add a file, but it fails for some reason.
//
bool DocumentManager::QueryAdd( Document* document ) const
{
	bool isOk = true;
	return isOk;
}

///////////////////////////////////////////////////////////////////////////////
// Opens a dialog prompting the user whether they want to save the current file
// or not.  The user has the option to specifiy that they want the same action
// taken for all subsequent files.
//
SaveAction DocumentManager::QueryCloseAll( Document* document ) const
{
	if ( document->HasChanged() )
	{
		bool attemptCheckOut = false;
		SaveActions::SaveAction action = SaveActions::Save;

		std::ostringstream msg;
		msg << "You are attempting to close file " << document->GetPath().Filename().Get() << " which has changed. Would you like to save your changes before closing?";
		
		int res = QMessageBox::information(NULL, "Save Changes?", msg.str().c_str(), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel | QMessageBox::YesToAll | QMessageBox::NoToAll);
		switch (res)
		{
		case QMessageBox::Yes:
			action = SaveActions::Save;
			break;

		case QMessageBox::YesToAll:
			action = SaveActions::SaveAll;
			break;

		case QMessageBox::No:
			action = SaveActions::Skip;
			break;

		case QMessageBox::NoToAll:
			action = SaveActions::SkipAll;
			break;

		case QMessageBox::Cancel:
			action = SaveActions::Abort;
			break;
		}

		return action;
	}

	return QueryClose( document );
}

///////////////////////////////////////////////////////////////////////////////
// Prompts for whether or not to save the file before closing.
//
SaveAction DocumentManager::QueryClose( Document* document ) const
{
	if ( !document->HasChanged() )
	{
		return SaveActions::Skip;
	}

	if ( document->IsCheckedOut() )
	{
		std::string msg( TXT( "Would you like to save changes to " ) );
		msg += TXT( "'" ) + document->GetPath().Filename().Get() + TXT( "' before closing?" );


		int res = QMessageBox::question(m_QParent, "Save Changes?", msg.c_str(), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel );
		switch ( res )
		{
		case QMessageBox::Yes:
			return SaveActions::Save;
			break;

		case QMessageBox::No:
			return SaveActions::Skip;
			break;

		case QMessageBox::Cancel:
		default:
			return SaveActions::Abort;
		}
	}

	return QuerySave( document );
}

///////////////////////////////////////////////////////////////////////////////
// Returns a value indicating whether the save operation should continue or not
// (not the checkout status).
//
SaveAction DocumentManager::QuerySave( Document* document ) const
{
	if ( !document->HasChanged() )
	{
		return SaveActions::Skip;
	}

	// File was already checked out, or was successfully checked out
	return SaveActions::Save;
}
