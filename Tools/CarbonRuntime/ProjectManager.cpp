#include "ProjectManager.h"

#include "DocumentManager.h"

#include <QSettings>
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QFileInfo>
#include <QTextStream>

#include "Runtime/FileLocations.h"
#include "Platform/Trace.h"

using namespace Dragora;

ProjectManager* ProjectManager::ms_Instance = NULL;
static unsigned short g_Init = 0;


void ProjectManager::Startup()
{
    if(++g_Init == 1)
    {
        DRAGORA_ASSERT(!ms_Instance);
        ms_Instance = new ProjectManager;
        DRAGORA_ASSERT(ms_Instance);
    }
}

void ProjectManager::Shutdown()
{
    if(--g_Init == 0)
    {
        DRAGORA_ASSERT(ms_Instance);
        delete ms_Instance;
        ms_Instance = NULL;
    }
}

ProjectManager &ProjectManager::GetInstance()
{
    DRAGORA_ASSERT(ms_Instance);
    return *ms_Instance;
}

bool ProjectManager::CreateNewProject(String name, QString location)
{
    if(m_activeProject)
        CloseProject();

    QFileInfo locInfo(location);
    if (!locInfo.exists())
        QDir().mkdir(location);

    FilePath destination(location.toStdString().c_str());
    QString projectFile =location  + "/" +*name + ".dcpj";
    m_ProjectFile = projectFile;
    m_ProjectRootDir = location;

    m_ProjectJSONRootObj = cJSON_CreateObject();
    cJSON_AddStringToObject(m_ProjectJSONRootObj, "ProjectName", *name);

    QFile file(projectFile);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);

        stream << cJSON_Print(m_ProjectJSONRootObj);

        file.flush(); file.close();
    }
    else
    {
        DRAGORA_ASSERT(false);
        return false;
    }

    //Now create the directories
    QDir tmp;
    tmp.mkdir(location + "/Content/");
    tmp.mkdir(location + "/Config/");

    m_isVanillaProject = true;
    FileLocations::SetBaseDirectory(m_ProjectRootDir.toStdString() + "/");
    DRAGORA_TRACE(TraceLevels::Info, "m_ProjectRootDir On Create Project: %s\n", m_ProjectRootDir.toStdString().c_str());

    m_DocManager = new Dragora::DocumentManager(NULL);

    e_OnProjectCreated.Raise(Dragora::Void());

	return true;
}

bool ProjectManager::OpenProject(const QString &location)
{
    if (!QFileInfo(location).exists())
        return false;

    if (m_activeProject)
        CloseProject();

    QFileInfo info(location);
    m_ProjectRootDir = info.absolutePath();

    m_ProjectFile = location;
    FileLocations::SetBaseDirectory(info.absolutePath().toStdString() + "/");
    DRAGORA_TRACE(TraceLevels::Info, "m_ProjectRootDir On Create Project: %s\n", info.absolutePath().toStdString().c_str());

    //Load the cJSON buffer.
    QFile f(location);
    if (!f.open(QFile::ReadOnly | QFile::Text)) return false;
    QTextStream in(&f);
    m_ProjectJSONRootObj = cJSON_Parse( in.readAll().toUtf8().toStdString().c_str() );
    if(!m_ProjectJSONRootObj) return false;

    m_isVanillaProject = false;
    m_DocManager = new Dragora::DocumentManager(NULL);

    e_OnProjectOpened.Raise(Dragora::Void());

	return true;
}

void ProjectManager::CloseProject()
{
    cJSON_Delete( m_ProjectJSONRootObj );
    m_ProjectJSONRootObj = NULL;

    delete m_DocManager;
    m_DocManager = NULL;


    m_isVanillaProject = false;
    m_activeProject = false;
    m_ProjectFile = "";
    m_ProjectRootDir = "";

    e_OnProjectClosed.Raise(Dragora::Void());
}

void ProjectManager::SaveChanges()
{
    // TODO: XdxdXdXdDdx
}

void ProjectManager::triggerModified()
{
    e_OnProjectModified.Raise(Dragora::Void());
}

cJSON *ProjectManager::GetRootJSONObject()
{
    DRAGORA_ASSERT(m_ProjectJSONRootObj);
    return m_ProjectJSONRootObj;
}

ProjectManager::ProjectManager()
    : m_isVanillaProject(false)
    , m_activeProject(false)
    , m_ProjectFile("")
    , m_ProjectRootDir("")
    , m_ProjectJSONRootObj(NULL)
{
}
