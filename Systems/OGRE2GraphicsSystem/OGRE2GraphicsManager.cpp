#include "OGRE2GraphicsManager.h"

#include "Engine/Runtime/FileLocations.h"
#include "Platform/Process.h"

#include <OGRE/Ogre.h>
#include <OGRE/OgreHlms.h>
#include <OGRE/Hlms/Pbs/OgreHlmsPbs.h>
#include <OGRE/Hlms/Unlit/OgreHlmsUnlit.h>

using namespace Dragora;

static unsigned short g_OGMInit = 0;
OGRE2GraphicsManager* OGRE2GraphicsManager::sm_pInstance = NULL;
FilePath OGRE2GraphicsManager::m_ExtensionsPath = FilePath();

void OGRE2GraphicsManager::Startup()
{
    if(++g_OGMInit == 1)
    {
        DRAGORA_ASSERT(!sm_pInstance);
        sm_pInstance = new OGRE2GraphicsManager();
        DRAGORA_ASSERT(sm_pInstance);
    }
}

void OGRE2GraphicsManager::Shutdown()
{
    if(--g_OGMInit == 0)
    {
        DRAGORA_ASSERT(sm_pInstance);
        delete sm_pInstance;
        sm_pInstance = NULL;
    }
}

OGRE2GraphicsManager &OGRE2GraphicsManager::GetInstance()
{
    DRAGORA_ASSERT(sm_pInstance);
    return *sm_pInstance;
}


OGRE2GraphicsManager::OGRE2GraphicsManager()
	: m_OgreRoot(NULL)
	, m_SceneMgr(NULL)
	, m_CompositorMgr(NULL)
#ifdef DRAGORA_TOOLS
	, m_EditorMode(false)
	, m_UsingStockHLMSShaders(false)
#endif
{
}

OGRE2GraphicsManager::~OGRE2GraphicsManager()
{
	if (m_OgreRoot)
		OGRE_DELETE m_OgreRoot;
}

void OGRE2GraphicsManager::PrepareGraphicsSettings()
{
	DRAGORA_ASSERT(!m_ExtensionsPath.Empty());
	FilePath CfgFile;

#ifdef DRAGORA_TOOLS
	DRAGORA_VERIFY(FileLocations::GetUserDirectory(CfgFile));
#else
	DRAGORA_VERIFY(FileLocations::GetPrefDirectory(CfgFile));
#endif

	FilePath::GuaranteeSeparator(CfgFile);
	CfgFile += "RenderSettings.ini";

	m_OgreRoot = OGRE_NEW Ogre::Root("", "", "");

#ifdef DRAGORA_OS_WIN
	const char* DX11RSPlugin = "RenderSystem_Direct3D11";
#endif
	const char* GL3RSPlugin = "RenderSystem_GL3Plus";

#ifdef DRAGORA_OS_WIN
	FilePath pluginLocationDX11(m_ExtensionsPath);
	pluginLocationDX11 += std::string(DX11RSPlugin) + "." + DRAGORA_MODULE_EXTENSION;
	DRAGORA_ASSERT(pluginLocationDX11.Exists());
#endif

	FilePath pluginLocationOGL3(m_ExtensionsPath);
	pluginLocationOGL3 += std::string(GL3RSPlugin) + "." + DRAGORA_MODULE_EXTENSION;
		
#ifdef DRAGORA_OS_WIN
	m_OgreRoot->loadPlugin(pluginLocationDX11.Get());
#endif
	m_OgreRoot->loadPlugin(pluginLocationOGL3.Get());


	if (m_OgreRoot->showConfigDialog())
	{
		m_OgreRoot->initialise(false);
	}
}


void OGRE2GraphicsManager::FinalizeGraphicsSettings(unsigned short numThread, bool multithreadCulling)
{
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	m_SceneMgr = m_OgreRoot->createSceneManager(Ogre::ST_GENERIC, numThread, (multithreadCulling) ? Ogre::INSTANCING_CULLING_THREADED : Ogre::INSTANCING_CULLING_SINGLETHREAD, "Khromathyon_SceneManagerInstance");
}

Ogre::SceneManager* OGRE2GraphicsManager::GetSceneManager() 
{
	DRAGORA_ASSERT(m_SceneMgr);
	return m_SceneMgr;
}


#ifdef DRAGORA_TOOLS
void OGRE2GraphicsManager::setStockHLMSPath(const FilePath& path, bool load)
{
	(void)load;
	//We assume the user validated their FilePath guaranting their separator
	Ogre::RenderSystem *renderSystem = m_OgreRoot->getRenderSystem();

	Ogre::String shaderSyntax = "GLSL";
	if (renderSystem->getName() == "Direct3D11 Rendering Subsystem")
		shaderSyntax = "HLSL";
	else if (renderSystem->getName() == "Metal Rendering Subsystem")
		shaderSyntax = "Metal";

	Ogre::Archive *archiveLibrary = Ogre::ArchiveManager::getSingletonPtr()->load(
		path.Get() + "Hlms/Common/" + shaderSyntax,
		"FileSystem", true);
	Ogre::Archive *archiveLibraryAny = Ogre::ArchiveManager::getSingletonPtr()->load(
		path.Get() + "Hlms/Common/Any",
		"FileSystem", true);

	Ogre::ArchiveVec library;
	library.push_back(archiveLibrary);
	library.push_back(archiveLibraryAny);

	Ogre::Archive *archiveUnlit = Ogre::ArchiveManager::getSingletonPtr()->load(
		path.Get() + "Hlms/Unlit/" + shaderSyntax,
		"FileSystem", true);

	Ogre::HlmsUnlit *hlmsUnlit = OGRE_NEW Ogre::HlmsUnlit(archiveUnlit, &library);
	Ogre::Root::getSingleton().getHlmsManager()->registerHlms(hlmsUnlit);

	Ogre::Archive *archivePbs = Ogre::ArchiveManager::getSingletonPtr()->load(
		path.Get() + "Hlms/Pbs/" + shaderSyntax,
		"FileSystem", true);
	Ogre::HlmsPbs *hlmsPbs = OGRE_NEW Ogre::HlmsPbs(archivePbs, &library);
	Ogre::Root::getSingleton().getHlmsManager()->registerHlms(hlmsPbs);

	if (renderSystem->getName() == "Direct3D11 Rendering Subsystem")
	{
		//Set lower limits 512kb instead of the default 4MB per Hlms in D3D 11.0
		//and below to avoid saturating AMD's discard limit (8MB) or
		//saturate the PCIE bus in some low end machines.
		bool supportsNoOverwriteOnTextureBuffers;
		renderSystem->getCustomAttribute("MapNoOverwriteOnDynamicBufferSRV",
			&supportsNoOverwriteOnTextureBuffers);

		if (!supportsNoOverwriteOnTextureBuffers)
		{
			hlmsPbs->setTextureBufferDefaultSize(512 * 1024);
			hlmsUnlit->setTextureBufferDefaultSize(512 * 1024);
		}
	}

	m_UsingStockHLMSShaders = true;
}

void OGRE2GraphicsManager::SetEditorResources()
{

}
#endif