#pragma once

#include "KMath/DataTypes.h"
#include "KMath/KMath.h"

#include <OGRE/OgreVector2.h>
#include <OGRE/OgreVector3.h>
#include <OGRE/OgreVector4.h>
#include <OGRE/OgreQuaternion.h>
#include <OGRE/OgreMatrix3.h>
#include <OGRE/OgreMatrix4.h>
#include <OGRE/OgreColourValue.h>

template<class DragoraType, class OgreType>
inline void ConvertToOgreType(const DragoraType &rDragora, OgreType& rOgre);

template<class OgreType, class DragoraType>
inline void ConvertFromOgreType(const OgreType &rOgre, DragoraType &rDragora );

//From Dragora to OGRE.

template<>
inline void ConvertToOgreType< Dragora::Math::Vector2, Ogre::Vector2>(const Dragora::Math::Vector2 &rDragora, Ogre::Vector2 &rOgre)
{
    rOgre.x = rDragora.x;
    rOgre.y = rDragora.y;
}

template<>
inline void ConvertToOgreType< Dragora::Math::Vector3, Ogre::Vector3>(const Dragora::Math::Vector3 &rDragora, Ogre::Vector3 &rOgre)
{
    rOgre.x = rDragora.x;
    rOgre.y = rDragora.y;
    rOgre.z = rDragora.z;
}


template<>
inline void ConvertToOgreType< Dragora::Math::Vector4, Ogre::Vector4>(const Dragora::Math::Vector4 &rDragora, Ogre::Vector4 &rOgre)
{
    rOgre.x = rDragora.x;
    rOgre.y = rDragora.y;
    rOgre.z = rDragora.z;
    rOgre.w = rDragora.w;
}


template<>
inline void ConvertToOgreType< Dragora::Math::Quaternion, Ogre::Quaternion>(const Dragora::Math::Quaternion &rDragora, Ogre::Quaternion &rOgre)
{
    rOgre.x = rDragora.x;
    rOgre.y = rDragora.y;
    rOgre.z = rDragora.z;
    rOgre.w = rDragora.w;
}

template<>
inline void ConvertToOgreType< Dragora::Math::Matrix3, Ogre::Matrix3>(const Dragora::Math::Matrix3 &rDragora, Ogre::Matrix3 &rOgre)
{
    rOgre[0][0] = rDragora[0][0];
    rOgre[0][1] = rDragora[0][1];
    rOgre[0][2] = rDragora[0][2];
    
    rOgre[1][0] = rDragora[1][0];
    rOgre[1][1] = rDragora[1][1];
    rOgre[1][2] = rDragora[1][2];
    
    rOgre[2][0] = rDragora[2][0];
    rOgre[2][1] = rDragora[2][1];
    rOgre[2][2] = rDragora[2][2];
}

template<>
inline void ConvertToOgreType< Dragora::Math::Matrix4, Ogre::Matrix4>(const Dragora::Math::Matrix4 &rDragora, Ogre::Matrix4 &rOgre)
{

    rOgre[0][0] = rDragora._m[0][0];
    rOgre[0][1] = rDragora._m[0][1];
    rOgre[0][2] = rDragora._m[0][2];
    rOgre[0][3] = rDragora._m[0][3];
    
    rOgre[1][0] = rDragora._m[1][0];
    rOgre[1][1] = rDragora._m[1][1];
    rOgre[1][2] = rDragora._m[1][2];
    rOgre[1][3] = rDragora._m[1][3];
    
    rOgre[2][0] = rDragora._m[2][0];
    rOgre[2][1] = rDragora._m[2][1];
    rOgre[2][2] = rDragora._m[2][2];
    rOgre[2][3] = rDragora._m[2][3];
    
    rOgre[3][0] = rDragora._m[3][0];
    rOgre[3][1] = rDragora._m[3][1];
    rOgre[3][2] = rDragora._m[3][2];
    rOgre[3][3] = rDragora._m[3][3];
}

template<>
inline void ConvertToOgreType< Dragora::Math::Color, Ogre::ColourValue>(const Dragora::Math::Color &rDragora, Ogre::ColourValue &rOgre)
{
    rOgre.r = rDragora.r;
    rOgre.g = rDragora.g;
    rOgre.b = rDragora.b;
    rOgre.a = rDragora.a;
}


//From OGRE to Dragora.
template<>
inline void ConvertFromOgreType<Ogre::Vector2, Dragora::Math::Vector2>(const Ogre::Vector2 &rOgre, Dragora::Math::Vector2 &rDragora)
{
    rDragora.x = rOgre.x;
    rDragora.y = rOgre.y;
}

template<>
inline void ConvertFromOgreType<Ogre::Vector3, Dragora::Math::Vector3>(const Ogre::Vector3 &rOgre, Dragora::Math::Vector3 &rDragora)
{
    rDragora.x = rOgre.x;
    rDragora.y = rOgre.y;
    rDragora.z = rOgre.z;
}

template<>
inline void ConvertFromOgreType<Ogre::Vector4, Dragora::Math::Vector4>(const Ogre::Vector4 &rOgre, Dragora::Math::Vector4 &rDragora)
{
    rDragora.x = rOgre.x;
    rDragora.y = rOgre.y;
    rDragora.z = rOgre.z;
    rDragora.w = rOgre.w;
}

template<>
inline void ConvertFromOgreType<Ogre::Quaternion, Dragora::Math::Quaternion>(const Ogre::Quaternion &rOgre, Dragora::Math::Quaternion &rDragora)
{
    rDragora.x = rOgre.x;
    rDragora.y = rOgre.y;
    rDragora.z = rOgre.z;
    rDragora.w = rOgre.w;
}

template<>
inline void ConvertFromOgreType<Ogre::Matrix3, Dragora::Math::Matrix3>(const Ogre::Matrix3 &rOgre, Dragora::Math::Matrix3 &rDragora)
{
    rDragora[0][0] = rOgre[0][0];
    rDragora[0][1] = rOgre[0][1];
    rDragora[0][2] = rOgre[0][2];

    rDragora[1][0] = rOgre[1][0];
    rDragora[1][1] = rOgre[1][1];
    rDragora[1][2] = rOgre[1][2];

    rDragora[2][0] = rOgre[2][0];
    rDragora[2][1] = rOgre[2][1];
    rDragora[2][2] = rOgre[2][2];
    
}

template<>
inline void ConvertFromOgreType<Ogre::Matrix4, Dragora::Math::Matrix4>(const Ogre::Matrix4 &rOgre, Dragora::Math::Matrix4 &rDragora)
{
    rDragora._m[0][0] = rOgre[0][0];
    rDragora._m[0][1] = rOgre[0][1];
    rDragora._m[0][2] = rOgre[0][2];
    rDragora._m[0][3] = rOgre[0][3];

    rDragora._m[1][0] = rOgre[1][0];
    rDragora._m[1][1] = rOgre[1][1];
    rDragora._m[1][2] = rOgre[1][2];
    rDragora._m[1][3] = rOgre[1][3];

    rDragora._m[2][0] = rOgre[2][0];
    rDragora._m[2][1] = rOgre[2][1];
    rDragora._m[2][2] = rOgre[2][2];
    rDragora._m[2][3] = rOgre[2][3];
}

template<>
inline void ConvertFromOgreType<Ogre::ColourValue, Dragora::Math::Color>(const Ogre::ColourValue &rOgre, Dragora::Math::Color &rDragora)
{
    rDragora.r = rOgre.r;
    rDragora.g = rOgre.g;
    rDragora.b = rOgre.b;
    rDragora.a = rOgre.a;
}

