#pragma once

#include "OGRE2GraphicsSystem/OGRE2GSAPI.h"

#include "Platform/Utility.h"
#include "Foundation/FilePath.h"

namespace Ogre
{
    class Root;
    class SceneManager;
    class RenderWindow;
    class CompositorWorkspace;
    class CompositorManager2;
    class Camera;
}

namespace Dragora
{
    class DRAGORA_OGRE2GRAPHICS_API OGRE2GraphicsManager : NonCopyable
    {
    public:
        static void Startup();
        static void Shutdown();
        static OGRE2GraphicsManager& GetInstance();

		//OGRE Preparation Functions.
		void PrepareGraphicsSettings();
		void FinalizeGraphicsSettings(unsigned short numThread = 1, bool multithreadCulling = false);
		void setExtensionsPath(const FilePath& path) { m_ExtensionsPath = path; }


		//OGRE Subsystem Access.
		inline Ogre::Root* GetRoot() { return m_OgreRoot; }
		inline Ogre::CompositorManager2* GetCompositorManager() { return m_CompositorMgr; }
		Ogre::SceneManager* GetSceneManager();

		// Tools only API.
#ifdef DRAGORA_TOOLS
		inline void setEditorMode(bool val) { m_EditorMode = val; }

		void setStockHLMSPath(const FilePath& path, bool load = false);
		inline FilePath getStockHLMSPath() const { return m_StockHLMSShadersPath; }
		inline bool isUsingStockHLMS() { return m_UsingStockHLMSShaders; }
#endif

    private:
        static OGRE2GraphicsManager* sm_pInstance;

		Ogre::Root* m_OgreRoot;
		Ogre::SceneManager* m_SceneMgr;
		Ogre::CompositorManager2* m_CompositorMgr;

		OGRE2GraphicsManager();
		~OGRE2GraphicsManager();

		static FilePath m_ExtensionsPath;

#ifdef DRAGORA_TOOLS
		void SetEditorResources();

		bool m_EditorMode;
		bool m_UsingStockHLMSShaders;
		FilePath m_StockHLMSShadersPath;
#endif

    };

}

#define OGRE2GRAPHICSMGR Dragora::OGRE2GraphicsManager::GetInstance()
#define OGRE2GRAPHICSMGR_ROOT Dragora::OGRE2GraphicsManager::GetInstance().GetRoot()
#define OGRE2GRAPHICSMGR_SCENEMGR Dragora::OGRE2GraphicsManager::GetInstance().GetSceneManager()
#define OGRE2GRAPHICSMGR_COMPOSITORMGR Dragora::OGRE2GraphicsManager::GetInstance().GetCompositorManager()

