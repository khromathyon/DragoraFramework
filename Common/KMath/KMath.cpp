/* Parts of this file derived from Smoke Demo of Intel Corporation
   Copyright message is below                                      */

// Copyright (C) 2008-2009 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED "AS IS."
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.

#include <cassert>
#include <stdlib.h>
#include <algorithm>

#include "KMath.h"

namespace Dragora
{

    using namespace Math;


    const f32 Angle::Pi = 3.1415926535897932384626433832795f;

    const Vector2 Vector2::Zero( 0.0f, 0.0f );
    const Vector2 Vector2::One( 1.0f, 1.0f );

    const Vector3 Vector3::Zero(0.0f);
    const Vector3 Vector3::One(1.0f);
    const Vector3 Vector3::UnitX(1.0f, 0.0f, 0.0f);
    const Vector3 Vector3::UnitY(0.0f, 1.0f, 0.0f);
    const Vector3 Vector3::UnitZ(0.0f, 0.0f, 1.0f);

    const Vector4 Vector4::Zero( 0.0f, 0.0f, 0.0f, 1.0f );
    const Vector4 Vector4::One( 1.0f, 1.0f, 1.0f, 1.0f );

    const Quaternion Quaternion::Identity( 0.0f, 0.0f, 0.0f, 1.0f );

    const Matrix4 Matrix4::Identity(
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );
	
    const Color Color::Black( 0.0f, 0.0f, 0.0f, 1.0f );
    const Color Color::White( 1.0f, 1.0f, 1.0f, 1.0f );


    const Quaternion& Quaternion::Set( const Vector3& Axis, f32 Angle )
    {
	    assert( 1.0f - Axis.Length() < 0.0001f);

        const f32 Sin = Angle::Sin( Angle / 2.0f );

        x = Axis.x * Sin;
        y = Axis.y * Sin;
        z = Axis.z * Sin;
        w = Angle::Cos( Angle / 2.0f );

        return *this;
    }


    const Quaternion& Quaternion::Set( Vector3& Angles )
    {
        //
        // This function is by no means optimal since it it known that several of the coordinates
        //  are zero so can be ignored in the quaternion multiply.
        //
        Quaternion qx;
        Quaternion qy;
        Quaternion qz;

        qx.Set( Vector3::UnitX, Angles.x );
        qy.Set( Vector3::UnitY, Angles.y );
        qz.Set( Vector3::UnitZ, Angles.z );

        *this = qx * qy;
        *this *= qz;

        return *this;
    }

    const Quaternion& Quaternion::FromAxes (const Vector3& xAxis, const Vector3& yAxis, const Vector3& zAxis)
    {
        Matrix3 kRot;

        kRot[0][0] = xAxis.x;
        kRot[1][0] = xAxis.y;
        kRot[2][0] = xAxis.z;

        kRot[0][1] = yAxis.x;
        kRot[1][1] = yAxis.y;
        kRot[2][1] = yAxis.z;

        kRot[0][2] = zAxis.x;
        kRot[1][2] = zAxis.y;
        kRot[2][2] = zAxis.z;

        return FromRotationMatrix(kRot);
    }

    void Quaternion::FromAngleAxis (const f32& rfAngle, const Vector3& rkAxis)
    {
        // assert:  axis[] is unit length
        //
        // The quaternion representing the rotation is
        //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

        f32 fHalfAngle ( 0.5 * rfAngle );
        f32 fSin = sin( fHalfAngle );
        x = fSin * rkAxis.x;
        y = fSin * rkAxis.y;
        z = fSin * rkAxis.z;
        w = cos( fHalfAngle );
    }

    f32 Quaternion::Normalise(void)
    {
        f32 len = Norm();
        f32 factor = 1.0f / sqrt( len );
        *this = *this * factor;
        return len;
    }

    void Quaternion::ToRotationMatrix( Matrix3& kRot ) const
    {
        f32 fTx  = x+x;
        f32 fTy  = y+y;
        f32 fTz  = z+z;
        f32 fTwx = fTx*w;
        f32 fTwy = fTy*w;
        f32 fTwz = fTz*w;
        f32 fTxx = fTx*x;
        f32 fTxy = fTy*x;
        f32 fTxz = fTz*x;
        f32 fTyy = fTy*y;
        f32 fTyz = fTz*y;
        f32 fTzz = fTz*z;

        kRot[0][0] = 1.0f-(fTyy+fTzz);
        kRot[0][1] = fTxy-fTwz;
        kRot[0][2] = fTxz+fTwy;
        kRot[1][0] = fTxy+fTwz;
        kRot[1][1] = 1.0f-(fTxx+fTzz);
        kRot[1][2] = fTyz-fTwx;
        kRot[2][0] = fTxz-fTwy;
        kRot[2][1] = fTyz+fTwx;
        kRot[2][2] = 1.0f-(fTxx+fTyy);
    }


    //-----------------------------------------------------------------------------
    // Apply a quaternion rotation to a vector.
    //
    void Quaternion::Rotate( Vector3& a )
    {
        Math::Quaternion p, r, q, qInverse;

        // Convert vector to a quaternion
        p.x = a.x;
        p.y = a.y;
        p.z = a.z;
        p.w = 0.0f;

        q.x = x;
        q.y = y;
        q.z = z;
        q.w = w;

        // This inverse only applicable if we are of unit length
        qInverse.x = -x;
        qInverse.y = -y;
        qInverse.z = -z;
        qInverse.w = w;

        // perform rotation
        r = q * p * qInverse;

        a.x = r.x;
        a.y = r.y;
        a.z = r.z;

        return;
    }

    const Quaternion& Quaternion::FromRotationMatrix (const Matrix3& kRot)
    {
        // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
        // article "Quaternion Calculus and Fast Animation".

        f32 fTrace = kRot[0][0]+kRot[1][1]+kRot[2][2];
        f32 fRoot;

        if ( fTrace > 0.0 )
        {
            // |w| > 1/2, may as well choose w > 1/2
            fRoot = sqrt(fTrace + 1.0f);  // 2w
            w = 0.5f*fRoot;
            fRoot = 0.5f/fRoot;  // 1/(4w)
            x = (kRot[2][1]-kRot[1][2])*fRoot;
            y = (kRot[0][2]-kRot[2][0])*fRoot;
            z = (kRot[1][0]-kRot[0][1])*fRoot;
        }
        else
        {
            // |w| <= 1/2
            static size_t s_iNext[3] = { 1, 2, 0 };
            size_t i = 0;
            if ( kRot[1][1] > kRot[0][0] )
                i = 1;
            if ( kRot[2][2] > kRot[i][i] )
                i = 2;
            size_t j = s_iNext[i];
            size_t k = s_iNext[j];

            fRoot = sqrt(kRot[i][i]-kRot[j][j]-kRot[k][k] + 1.0f);
            f32* apkQuat[3] = { &x, &y, &z };
            *apkQuat[i] = 0.5f*fRoot;
            fRoot = 0.5f/fRoot;
            w = (kRot[k][j]-kRot[j][k])*fRoot;
            *apkQuat[j] = (kRot[j][i]+kRot[i][j])*fRoot;
            *apkQuat[k] = (kRot[k][i]+kRot[i][k])*fRoot;
        }

        return *this;
    }


    const Matrix3 Matrix3::Zero(0,0,0,0,0,0,0,0,0);
    const Matrix3 Matrix3::Identity(1,0,0,0,1,0,0,0,1);

    //-----------------------------------------------------------------------
    Vector3 Matrix3::GetColumn (size_t iCol) const
    {
        assert( 0 <= iCol && iCol < 3 );
        return Vector3(m[0][iCol],m[1][iCol],
            m[2][iCol]);
    }
    //-----------------------------------------------------------------------
    void Matrix3::SetColumn(size_t iCol, const Vector3& vec)
    {
        assert( 0 <= iCol && iCol < 3 );
        m[0][iCol] = vec.x;
        m[1][iCol] = vec.y;
        m[2][iCol] = vec.z;

    }
    //-----------------------------------------------------------------------
    void Matrix3::FromAxes(const Vector3& xAxis, const Vector3& yAxis, const Vector3& zAxis)
    {
        SetColumn(0,xAxis);
        SetColumn(1,yAxis);
        SetColumn(2,zAxis);

    }

    //-----------------------------------------------------------------------
    bool Matrix3::operator== (const Matrix3& rkMatrix) const
    {
        for (size_t iRow = 0; iRow < 3; iRow++)
        {
            for (size_t iCol = 0; iCol < 3; iCol++)
            {
                if ( m[iRow][iCol] != rkMatrix.m[iRow][iCol] )
                    return false;
            }
        }

        return true;
    }
    //-----------------------------------------------------------------------
    Matrix3 Matrix3::operator+ (const Matrix3& rkMatrix) const
    {
        Matrix3 kSum;
        for (size_t iRow = 0; iRow < 3; iRow++)
        {
            for (size_t iCol = 0; iCol < 3; iCol++)
            {
                kSum.m[iRow][iCol] = m[iRow][iCol] +
                    rkMatrix.m[iRow][iCol];
            }
        }
        return kSum;
    }
    //-----------------------------------------------------------------------
    Matrix3 Matrix3::operator- (const Matrix3& rkMatrix) const
    {
        Matrix3 kDiff;
        for (size_t iRow = 0; iRow < 3; iRow++)
        {
            for (size_t iCol = 0; iCol < 3; iCol++)
            {
                kDiff.m[iRow][iCol] = m[iRow][iCol] -
                    rkMatrix.m[iRow][iCol];
            }
        }
        return kDiff;
    }
    //-----------------------------------------------------------------------
    Matrix3 Matrix3::operator* (const Matrix3& rkMatrix) const
    {
        Matrix3 kProd;
        for (size_t iRow = 0; iRow < 3; iRow++)
        {
            for (size_t iCol = 0; iCol < 3; iCol++)
            {
                kProd.m[iRow][iCol] =
                    m[iRow][0]*rkMatrix.m[0][iCol] +
                    m[iRow][1]*rkMatrix.m[1][iCol] +
                    m[iRow][2]*rkMatrix.m[2][iCol];
            }
        }
        return kProd;
    }
    //-----------------------------------------------------------------------
    Vector3 Matrix3::operator* (const Vector3& rkPoint) const
    {
        Vector3 kProd;
        for (size_t iRow = 0; iRow < 3; iRow++)
        {
            kProd[iRow] =
                m[iRow][0]*rkPoint[0] +
                m[iRow][1]*rkPoint[1] +
                m[iRow][2]*rkPoint[2];
        }
        return kProd;
    }
    //-------------------------------------------------------------------
    Matrix3 Matrix3::operator- () const
    {
        Matrix3 kNeg;
        for (size_t iRow = 0; iRow < 3; iRow++)
        {
            for (size_t iCol = 0; iCol < 3; iCol++)
                kNeg[iRow][iCol] = -m[iRow][iCol];
        }
        return kNeg;
    }
    //-----------------------------------------------------------------------
    Matrix3 Matrix3::operator* (f32 fScalar) const
    {
        Matrix3 kProd;
        for (size_t iRow = 0; iRow < 3; iRow++)
        {
            for (size_t iCol = 0; iCol < 3; iCol++)
                kProd[iRow][iCol] = fScalar*m[iRow][iCol];
        }
        return kProd;
    }
    //-----------------------------------------------------------------------
    Matrix3 Matrix3::Transpose () const
    {
        Matrix3 kTranspose;
        for (size_t iRow = 0; iRow < 3; iRow++)
        {
            for (size_t iCol = 0; iCol < 3; iCol++)
                kTranspose[iRow][iCol] = m[iCol][iRow];
        }
        return kTranspose;
    }
    //-----------------------------------------------------------------------
    bool Matrix3::Inverse (Matrix3& rkInverse, f32 fTolerance) const
    {
        // Invert a 3x3 using cofactors.  This is about 8 times faster than
        // the Numerical Recipes code which uses Gaussian elimination.

        rkInverse[0][0] = m[1][1]*m[2][2] -
            m[1][2]*m[2][1];
        rkInverse[0][1] = m[0][2]*m[2][1] -
            m[0][1]*m[2][2];
        rkInverse[0][2] = m[0][1]*m[1][2] -
            m[0][2]*m[1][1];
        rkInverse[1][0] = m[1][2]*m[2][0] -
            m[1][0]*m[2][2];
        rkInverse[1][1] = m[0][0]*m[2][2] -
            m[0][2]*m[2][0];
        rkInverse[1][2] = m[0][2]*m[1][0] -
            m[0][0]*m[1][2];
        rkInverse[2][0] = m[1][0]*m[2][1] -
            m[1][1]*m[2][0];
        rkInverse[2][1] = m[0][1]*m[2][0] -
            m[0][0]*m[2][1];
        rkInverse[2][2] = m[0][0]*m[1][1] -
            m[0][1]*m[1][0];

        f32 fDet =
            m[0][0]*rkInverse[0][0] +
            m[0][1]*rkInverse[1][0]+
            m[0][2]*rkInverse[2][0];

        if ( fabs(fDet) <= fTolerance )
            return false;

        f32 fInvDet = 1.0f/fDet;
        for (size_t iRow = 0; iRow < 3; iRow++)
        {
            for (size_t iCol = 0; iCol < 3; iCol++)
                rkInverse[iRow][iCol] *= fInvDet;
        }

        return true;
    }
    //-----------------------------------------------------------------------
    Matrix3 Matrix3::Inverse (f32 fTolerance) const
    {
        Matrix3 kInverse = Matrix3::Zero;
        Inverse(kInverse,fTolerance);
        return kInverse;
    }
    //-----------------------------------------------------------------------
    f32 Matrix3::Determinant () const
    {
        f32 fCofactor00 = m[1][1]*m[2][2] -
            m[1][2]*m[2][1];
        f32 fCofactor10 = m[1][2]*m[2][0] -
            m[1][0]*m[2][2];
        f32 fCofactor20 = m[1][0]*m[2][1] -
            m[1][1]*m[2][0];

        f32 fDet =
            m[0][0]*fCofactor00 +
            m[0][1]*fCofactor10 +
            m[0][2]*fCofactor20;

        return fDet;
    }
    //-----------------------------------------------------------------------
    void Matrix3::ToAxisAngle (Vector3& rkAxis, f32& rff32s) const
    {
        // Let (x,y,z) be the unit-length axis and let A be an angle of rotation.
        // The rotation matrix is R = I + sin(A)*P + (1-cos(A))*P^2 where
        // I is the identity and
        //
        //       +-        -+
        //   P = |  0 -z +y |
        //       | +z  0 -x |
        //       | -y +x  0 |
        //       +-        -+
        //
        // If A > 0, R represents a counterclockwise rotation about the axis in
        // the sense of looking from the tip of the axis vector towards the
        // origin.  Some algebra will show that
        //
        //   cos(A) = (trace(R)-1)/2  and  R - R^t = 2*sin(A)*P
        //
        // In the event that A = pi, R-R^t = 0 which prevents us from extracting
        // the axis through P.  Instead note that R = I+2*P^2 when A = pi, so
        // P^2 = (R-I)/2.  The diagonal entries of P^2 are x^2-1, y^2-1, and
        // z^2-1.  We can solve these for axis (x,y,z).  Because the angle is pi,
        // it does not matter which sign you choose on the square roots.

        f32 fTrace = m[0][0] + m[1][1] + m[2][2];
        f32 fCos = 0.5f*(fTrace-1.0f);
        rff32s = acos(fCos);  // in [0,PI]

        if ( rff32s > f32(0.0) )
        {
            if ( rff32s < f32(Angle::Pi) )
            {
                rkAxis.x = m[2][1]-m[1][2];
                rkAxis.y = m[0][2]-m[2][0];
                rkAxis.z = m[1][0]-m[0][1];
                rkAxis.Normalize();
            }
            else
            {
                // angle is PI
                float fHalfInverse;
                if ( m[0][0] >= m[1][1] )
                {
                    // r00 >= r11
                    if ( m[0][0] >= m[2][2] )
                    {
                        // r00 is maximum diagonal term
                        rkAxis.x = 0.5f*sqrt(m[0][0] -
                            m[1][1] - m[2][2] + 1.0f);
                        fHalfInverse = 0.5f/rkAxis.x;
                        rkAxis.y = fHalfInverse*m[0][1];
                        rkAxis.z = fHalfInverse*m[0][2];
                    }
                    else
                    {
                        // r22 is maximum diagonal term
                        rkAxis.z = 0.5f*sqrt(m[2][2] -
                            m[0][0] - m[1][1] + 1.0f);
                        fHalfInverse = 0.5f/rkAxis.z;
                        rkAxis.x = fHalfInverse*m[0][2];
                        rkAxis.y = fHalfInverse*m[1][2];
                    }
                }
                else
                {
                    // r11 > r00
                    if ( m[1][1] >= m[2][2] )
                    {
                        // r11 is maximum diagonal term
                        rkAxis.y = 0.5f*sqrt(m[1][1] -
                            m[0][0] - m[2][2] + 1.0f);
                        fHalfInverse  = 0.5f/rkAxis.y;
                        rkAxis.x = fHalfInverse*m[0][1];
                        rkAxis.z = fHalfInverse*m[1][2];
                    }
                    else
                    {
                        // r22 is maximum diagonal term
                        rkAxis.z = 0.5f*sqrt(m[2][2] -
                            m[0][0] - m[1][1] + 1.0f);
                        fHalfInverse = 0.5f/rkAxis.z;
                        rkAxis.x = fHalfInverse*m[0][2];
                        rkAxis.y = fHalfInverse*m[1][2];
                    }
                }
            }
        }
        else
        {
            // The angle is 0 and the matrix is the identity.  Any axis will
            // work, so just use the x-axis.
            rkAxis.x = 1.0;
            rkAxis.y = 0.0;
            rkAxis.z = 0.0;
        }
    }
    //-----------------------------------------------------------------------
    void Matrix3::FromAxisAngle (const Vector3& rkAxis, const f32& ff32s)
    {
        f32 fCos = cos(ff32s);
        f32 fSin = sin(ff32s);
        f32 fOneMinusCos = 1.0f-fCos;
        f32 fX2 = rkAxis.x*rkAxis.x;
        f32 fY2 = rkAxis.y*rkAxis.y;
        f32 fZ2 = rkAxis.z*rkAxis.z;
        f32 fXYM = rkAxis.x*rkAxis.y*fOneMinusCos;
        f32 fXZM = rkAxis.x*rkAxis.z*fOneMinusCos;
        f32 fYZM = rkAxis.y*rkAxis.z*fOneMinusCos;
        f32 fXSin = rkAxis.x*fSin;
        f32 fYSin = rkAxis.y*fSin;
        f32 fZSin = rkAxis.z*fSin;

        m[0][0] = fX2*fOneMinusCos+fCos;
        m[0][1] = fXYM-fZSin;
        m[0][2] = fXZM+fYSin;
        m[1][0] = fXYM+fZSin;
        m[1][1] = fY2*fOneMinusCos+fCos;
        m[1][2] = fYZM-fXSin;
        m[2][0] = fXZM-fYSin;
        m[2][1] = fYZM+fXSin;
        m[2][2] = fZ2*fOneMinusCos+fCos;
    }
    //-----------------------------------------------------------------------

    Vector3 Matrix4::operator*( Vector3& a ) const
    {
        Vector3 r;

        r.x = m[ 0 ] * a.x + m[ 4 ] * a.y + m[  8 ] * a.z + m[ 12 ];
        r.y = m[ 1 ] * a.x + m[ 5 ] * a.y + m[  9 ] * a.z + m[ 13 ];
        r.z = m[ 2 ] * a.x + m[ 6 ] * a.y + m[ 10 ] * a.z + m[ 14 ];

        return r;
    }


    Vector4 Matrix4::operator*( Vector4& a ) const
    {
        Vector4 r;

        r.x = m[ 0 ] * a.x + m[ 4 ] * a.y + m[  8 ] * a.z + m[ 12 ] * a.w;
        r.y = m[ 1 ] * a.x + m[ 5 ] * a.y + m[  9 ] * a.z + m[ 13 ] * a.w;
        r.z = m[ 2 ] * a.x + m[ 6 ] * a.y + m[ 10 ] * a.z + m[ 14 ] * a.w;
        r.w = m[ 3 ] * a.x + m[ 7 ] * a.y + m[ 11 ] * a.z + m[ 15 ] * a.w;

        return r;
    }


    #pragma warning( push )
    #pragma warning( disable : 4701 )
    Matrix4 Matrix4::operator*( Matrix4& a ) const
    {
        Matrix4 r;

        u32 idx = 0;
        for ( u32 Row=0; Row < 16; Row+=4 )
        {
            for ( u32 Col=0; Col < 4; Col++ )
            {
                r.m[ idx ] = 0.0f;

                for ( u32 i=0; i < 4; i++ )
                {
                    r.m[ idx ] += m[ Col + i*4 ] * a.m[ Row + i ];
                }

                idx++;
            }
        }

        return r;
    }
    #pragma warning( pop )

    const Matrix4& Matrix4::operator*=( Matrix4& a )
    {
        *this = *this * a;
        return *this;
    }


    const Matrix4& Matrix4::Transformation( const Vector3& Translation, const Quaternion& Orientation )
    {
        SetTranslation( Translation );

        SetOrientation( Orientation );

        m[  3 ] = 0.0f;
        m[  7 ] = 0.0f;
        m[ 11 ] = 0.0f;
        m[ 15 ] = 1.0f;

        return *this;
    }


    void Matrix4::GetOrientation( Quaternion& Orientation ) const
    {
        const f32 T = 1.0f + m[ 0 ] + m[ 5 ] + m[ 10 ];

        if ( T > 0.00000001f )
        {
            const f32 S = sqrtf( T ) * 2.0f;
            const f32 invS = 1.0f / S;

            Orientation.x = (m[ 6 ] - m[ 9 ]) * invS;
            Orientation.y = (m[ 8 ] - m[ 2 ]) * invS;
            Orientation.z = (m[ 1 ] - m[ 4 ]) * invS;
            Orientation.w = 0.25f * S;
        }
        else if ( m[ 0 ] > m[ 5 ] && m[ 0 ] > m[ 10 ] )
        {
            const f32 S = sqrtf( 1.0f + m[ 0 ] - m[ 5 ] - m[ 10 ] ) * 2.0f;
            const f32 invS = 1.0f / S;

            Orientation.x = 0.25f * S;
            Orientation.y = (m[ 1 ] + m[ 4 ]) * invS;
            Orientation.z = (m[ 8 ] + m[ 2 ]) * invS;
            Orientation.w = (m[ 6 ] - m[ 9 ]) * invS;
        }
        else if ( m[ 5 ] > m[ 10 ] )
        {
            const f32 S  = sqrt( 1.0f + m[ 5 ] - m[ 0 ] - m[ 10 ] ) * 2.0f;
            const f32 invS = 1.0f / S;

            Orientation.x = (m[ 1 ] + m[ 4 ]) * invS;
            Orientation.y = 0.25f * S;
            Orientation.z = (m[ 6 ] + m[ 9 ]) * invS;
            Orientation.w = (m[ 8 ] - m[ 2 ]) * invS;
        }
        else
        {
            const f32 S  = sqrt( 1.0f + m[ 10 ] - m[ 0 ] - m[ 5 ] ) * 2.0f;
            const f32 invS = 1.0f / S;

            Orientation.x = (m[ 8 ] + m[ 2 ]) * invS;
            Orientation.y = (m[ 6 ] + m[ 9 ]) * invS;
            Orientation.z = 0.25f * S;
            Orientation.w = (m[ 1 ] - m[ 4 ]) * invS;
        }
    }


    const Matrix4& Matrix4::SetOrientation( const Quaternion& Orientation )
    {
        f32 xx = Orientation.x * Orientation.x;
        f32 xy = Orientation.x * Orientation.y;
        f32 xz = Orientation.x * Orientation.z;
        f32 xw = Orientation.x * Orientation.w;
        f32 yy = Orientation.y * Orientation.y;
        f32 yz = Orientation.y * Orientation.z;
        f32 yw = Orientation.y * Orientation.w;
        f32 zz = Orientation.z * Orientation.z;
        f32 zw = Orientation.z * Orientation.w;

        m[  0 ] = 1.0f - ((yy + zz) * 2.0f);
        m[  1 ] =        ((xy - zw) * 2.0f);
        m[  2 ] =        ((xz + yw) * 2.0f);
        m[  4 ] =        ((xy + zw) * 2.0f);
        m[  5 ] = 1.0f - ((xx + zz) * 2.0f);
        m[  6 ] =        ((yz - xw) * 2.0f);
        m[  8 ] =        ((xz - yw) * 2.0f);
        m[  9 ] =        ((yz + xw) * 2.0f);
        m[ 10 ] = 1.0f - ((xx + yy) * 2.0f);

        return *this;
    }

    const Matrix4& Matrix4::Transpose( Matrix4& T )
    {
        Matrix4 temp;
        temp = temp.Identity;
        for (int i = 0; i < 4; i++) {
        temp[i]      = T[i*4];
        temp[i + 4]  = T[i*4 + 1];
        temp[i + 8]  = T[i*4 + 2];
        temp[i + 12] = T[i*4 + 3];
        }

        T = temp;
        return T;
    }

    const Matrix4& Matrix4::Inverse( Matrix4& Inverse )
    {

        Matrix4 A;

        A = Inverse;
        A = A.Transpose(A);



        f32 cofactor_00 = Cofactor9(A.m[5],A.m[6],A.m[7],A.m[9],A.m[10],A.m[11],A.m[13],A.m[14],A.m[15]);
        f32 cofactor_01 = -Cofactor9(A.m[4],A.m[6],A.m[7],A.m[8],A.m[10],A.m[11],A.m[12],A.m[14],A.m[15]);
        f32 cofactor_02 = Cofactor9(A.m[4],A.m[5],A.m[7],A.m[8],A.m[9],A.m[11],A.m[12],A.m[13],A.m[15]);
        f32 cofactor_03 = -Cofactor9(A.m[4],A.m[5],A.m[6],A.m[8],A.m[9],A.m[10],A.m[12],A.m[13],A.m[14]);
        f32 cofactor_10 = Cofactor9(A.m[1],A.m[2],A.m[3],A.m[9],A.m[10],A.m[11],A.m[13],A.m[14],A.m[15]);
        f32 cofactor_11 = Cofactor9(A.m[0],A.m[2],A.m[3],A.m[8],A.m[10],A.m[11],A.m[12],A.m[14],A.m[15]);
        f32 cofactor_12 = -Cofactor9(A.m[0],A.m[1],A.m[3],A.m[8],A.m[9],A.m[11],A.m[12],A.m[13],A.m[15]);
        f32 cofactor_13 = Cofactor9(A.m[0],A.m[1],A.m[2],A.m[8],A.m[9],A.m[10],A.m[12],A.m[13],A.m[14]);
        f32 cofactor_20 = Cofactor9(A.m[1],A.m[2],A.m[3],A.m[5],A.m[6],A.m[7],A.m[13],A.m[14],A.m[15]);
        f32 cofactor_21 = -Cofactor9(A.m[0],A.m[2],A.m[3],A.m[4],A.m[6],A.m[7],A.m[12],A.m[14],A.m[15]);
        f32 cofactor_22 = Cofactor9(A.m[0],A.m[1],A.m[3],A.m[4],A.m[5],A.m[7],A.m[12],A.m[13],A.m[15]);
        f32 cofactor_23 = -Cofactor9(A.m[0],A.m[1],A.m[2],A.m[4],A.m[5],A.m[6],A.m[12],A.m[13],A.m[14]);
        f32 cofactor_30 = -Cofactor9(A.m[1],A.m[2],A.m[3],A.m[5],A.m[6],A.m[7],A.m[9],A.m[10],A.m[11]);
        f32 cofactor_31 = Cofactor9(A.m[0],A.m[2],A.m[3],A.m[4],A.m[6],A.m[7],A.m[8],A.m[10],A.m[11]);
        f32 cofactor_32 = -Cofactor9(A.m[0],A.m[1],A.m[3],A.m[4],A.m[5],A.m[7],A.m[8],A.m[9],A.m[11]);
        f32 cofactor_33 = Cofactor9(A.m[0],A.m[1],A.m[2],A.m[4],A.m[5],A.m[6],A.m[8],A.m[9],A.m[10]);

        f32 cofactorGiven_00 = Cofactor9(Inverse.m[5],Inverse.m[6],Inverse.m[7],Inverse.m[9],Inverse.m[10],Inverse.m[11],Inverse.m[13],Inverse.m[14],Inverse.m[15]);
        f32 cofactorGiven_01 = Cofactor9(Inverse.m[4],Inverse.m[6],Inverse.m[7],Inverse.m[8],Inverse.m[10],Inverse.m[11],Inverse.m[12],Inverse.m[14],Inverse.m[15]);
        f32 cofactorGiven_02 = Cofactor9(Inverse.m[4],Inverse.m[5],Inverse.m[7],Inverse.m[8],Inverse.m[9],Inverse.m[11],Inverse.m[12],Inverse.m[13],Inverse.m[15]);
        f32 cofactorGiven_03 = Cofactor9(Inverse.m[4],Inverse.m[5],Inverse.m[6],Inverse.m[8],Inverse.m[9],Inverse.m[10],Inverse.m[12],Inverse.m[13],Inverse.m[14]);
        f32 detA = Inverse.m[0] * cofactorGiven_00 - Inverse.m[1]*cofactorGiven_01 + Inverse.m[2]*cofactorGiven_02 - Inverse.m[3]*cofactorGiven_03;
        f32 IdetA = 1.0f/detA;

        Inverse.m[0]   = IdetA * cofactor_00;
        Inverse.m[1]   = IdetA * cofactor_01;
        Inverse.m[2]   = IdetA * cofactor_02;
        Inverse.m[3]   = IdetA * cofactor_03;
        Inverse.m[4]   = IdetA * cofactor_10;
        Inverse.m[5]   = IdetA * cofactor_11;
        Inverse.m[6]   = IdetA * cofactor_12;
        Inverse.m[7]   = IdetA * cofactor_13;
        Inverse.m[8]   = IdetA * cofactor_20;
        Inverse.m[9]   = IdetA * cofactor_21;
        Inverse.m[10]  = IdetA * cofactor_22;
        Inverse.m[11]  = IdetA * cofactor_23;
        Inverse.m[12]  = IdetA * cofactor_30;
        Inverse.m[13]  = IdetA * cofactor_31;
        Inverse.m[14]  = IdetA * cofactor_32;
        Inverse.m[15]  = IdetA * cofactor_33;

        return Inverse;
    }

    f32 Matrix4::Cofactor4(f32 a, f32 b, f32 c, f32 d)
    {
        return (a*d)-(b*c);
    }

    f32 Matrix4::Cofactor9(f32 a, f32 b, f32 c, f32 d, f32 e, f32 f, f32 g, f32 h, f32 i)
    {
        return (a*Cofactor4(e,f,h,i)) + (b*Cofactor4(d,f,g,i)) + (c*Cofactor4(d,e,g,h));

    }


    f32 Random::GetRandomFloat(float a, float b)
    {
        if( a >= b ) // bad input
            return a;

        // Get random float in [0, 1] interval.
        float f = (rand()%10001) * 0.0001f;

        return (f*(b-a))+a;
    }

}
