//  Copyright (C) 2013-2015 Khromathyon Software, S.A.
//  All Rights Reserved.

/*
Parts of this file derived from Smoke Demo of Intel Corporation
Copyright message is below
*/

// Copyright (C) 2008-2009 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED "AS IS."
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.


#pragma once

namespace Dragora
{
	typedef char i8;
	typedef short i16;
	typedef int i32;
	typedef long long i64;

	typedef unsigned char u8;
	typedef unsigned short u16;
	typedef unsigned int u32;
	typedef unsigned long long u64;


	typedef float f32;
	typedef double f64;

	/*
	typedef void* Handle;
	typedef u32 Error;

	typedef u32 Id;      // Local id, GUIDs are too slow and unnecessary for our purposes.
	typedef u32 ObjectId;
	*/

	typedef char* pstr;
	typedef const char* pcstr;

}
