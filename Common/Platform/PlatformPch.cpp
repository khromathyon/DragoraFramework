#include "PlatformPch.h"

#include "Platform/MemoryHeap.h"

#if DRAGORA_HEAP

DRAGORA_DEFINE_DEFAULT_MODULE_HEAP( Platform );

#if DRAGORA_SHARED
#include "Platform/NewDelete.h"
#endif

#endif // DRAGORA_HEAP
