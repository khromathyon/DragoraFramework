#pragma once

#include "Platform/System.h"

#if DRAGORA_SHARED
# ifdef DRAGORA_PLATFORM_EXPORTS
#  define DRAGORA_PLATFORM_API DRAGORA_API_EXPORT
# else
#  define DRAGORA_PLATFORM_API DRAGORA_API_IMPORT
# endif
#else
# define DRAGORA_PLATFORM_API
#endif
