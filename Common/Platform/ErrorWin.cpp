#include "PlatformPch.h"
#include "Platform/Error.h"
#include "Platform/Encoding.h"

using namespace Dragora;

uint32_t Dragora::GetLastError()
{
	return ::GetLastError();
}

std::string Dragora::GetErrorString( uint32_t errorOverride )
{
	// get the system error
	DWORD error = ( errorOverride != 0 ) ? errorOverride : ::GetLastError();

	LPTSTR lpMsgBuf = NULL;
	::FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		lpMsgBuf, 0, NULL );

	std::string result;
	if (lpMsgBuf)
	{
		DRAGORA_WIDE_TO_TCHAR(lpMsgBuf, convertedMsg);
		result = convertedMsg;
		::LocalFree( lpMsgBuf );
	}
	else
	{
		result = "Unknown error (the error code could not be translated)";
	}

	// trim enter chracters from message
	while (!result.empty() && (*result.rbegin() == '\n' || *result.rbegin() == '\r'))
	{
		result.resize( result.size() - 1 );
	}

	std::string str;
	ConvertString( result, str );
	return str;
}
