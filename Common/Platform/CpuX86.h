#pragma once

#include <xmmintrin.h>

/// Defined as 1 for little-endian platforms.
#define DRAGORA_ENDIAN_LITTLE 1

#ifdef DRAGORA_ENDIAN_BIG
#undef DRAGORA_ENDIAN_BIG
#endif

#if DRAGORA_CPU_X86_64
/// Number of bits in a pointer/size_t/ptrdiff_t/etc.
#define DRAGORA_WORDSIZE 64
#else
/// Number of bits in a pointer/size_t/ptrdiff_t/etc.
#define DRAGORA_WORDSIZE 32
#endif

/// Cache line size, in bytes (general case).
#define DRAGORA_CACHE_LINE_SIZE_IN_BYTES 64

namespace Dragora
{
    /// Prefetch a cache line of data from the specified address.
    ///
    /// @param[in] pData  Address to prefetch.
    inline void Prefetch( void* pData )
    {
        _mm_prefetch( static_cast< char* >( pData ), _MM_HINT_T0 );
    }
}
