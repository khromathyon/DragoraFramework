#pragma once

#include "Platform/API.h"
#include "Platform/Types.h"
#include "Platform/TypeTraits.h"
#include "Platform/Assert.h"
#include "Platform/System.h"

#include <string>
#include <vector>
#include <memory>

#include <EASTL/shared_ptr.h>
#include <EASTL/weak_ptr.h>

#if DRAGORA_CC_CL
# pragma warning( push )
# pragma warning( disable : 4530 )  // C++ exception handler used, but unwind semantics are not enabled. Specify /EHsc
# if _MSC_VER < 1600 // VC2008
#  pragma warning( disable : 4985 )  // 'symbol name': attributes not present on previous declaration : see http://connect.microsoft.com/VisualStudio/feedback/details/381422/warning-of-attributes-not-present-on-previous-declaration-on-ceil-using-both-math-h-and-intrin-h
# endif
#endif

#if DRAGORA_CC_CL
#pragma warning( pop )
#endif

#include <cstdarg>
#include <string.h>

/// @defgroup utilitymacros General Utility Macros
//@{

/// Specify a variable as purposefully unreferenced.
///
/// @param[in] VAR  Variable that may be unreferenced across its scoped.
#define DRAGORA_UNREF( VAR ) ( void )( VAR )

/// Get the number of elements in a static array.
///
/// @param[in] ARRAY  Static array.
///
/// @return  Number of elements in the array.
#define DRAGORA_ARRAY_COUNT( ARRAY ) ( sizeof( ARRAY ) / sizeof( ARRAY[ 0 ] ) )

/// Get the byte offset of a member of a struct or class.
///
/// @param[in] TYPE    Struct or class type name.
/// @param[in] MEMBER  Member name.
///
/// @return  Byte offset of the specified member.
#define DRAGORA_OFFSET_OF( TYPE, MEMBER ) ( reinterpret_cast< size_t >( &static_cast< TYPE* >( NULL )->MEMBER ) )

/// Get the bitfield with only the most significant bit set.
///
/// @param[in] TYPE    Any type (should be numeric).
///
/// @return  Constant number of the highest bit set, all other bits zero.
#define DRAGORA_HIGH_BIT( TYPE ) ( 1 << ( sizeof( TYPE ) * 8 ) )

//@}

namespace Dragora
{
    template<bool B> struct constant_expression    { static bool result() { return true; } };
    template<> struct constant_expression<false>   { static bool result() { return false; } };

    /// @defgroup utilitymem Memory Utility Functions
    //@{
    inline void MemoryCopy( void* pDest, const void* pSource, size_t size );
    inline void MemoryMove( void* pDest, const void* pSource, size_t size );
    inline void MemorySet( void* pDest, int value, size_t size );
    inline void MemoryZero( void* pDest, size_t size );
    inline int MemoryCompare( const void* pMemory0, const void* pMemory1, size_t size );

    template< typename T > void InPlaceConstruct( void* pMemory );
    template< typename T > void InPlaceDestruct( void* pMemory );

	template< typename T > void ArrayCopy( T* pDest, const T* pSource, size_t count );
    template< typename T > void ArrayMove( T* pDest, const T* pSource, size_t count );
    template< typename T > void ArraySet( T* pDest, const T& rValue, size_t count );

    template< typename T > T* ArrayInPlaceConstruct( void* pMemory, size_t count );
    template< typename T > void ArrayInPlaceDestruct( T* pMemory, size_t count );
    template< typename T > void ArrayUninitializedCopy( T* pDest, const T* pSource, size_t count );
    template< typename T > void ArrayUninitializedFill( T* pDest, const T& rValue, size_t count );

    template< typename T > T Align( const T& rValue, size_t alignment );

    template< typename T > void Swap( T& rValue0, T& rValue1 );
    //@}

    /// @defgroup utilitystring String Utility Functions
    //@{
    template< typename T > uint32_t StringHash( const T* pString );
    template< typename T > size_t StringLength( const T* pString );

	template< class T, size_t N >
	inline void CopyString( T (&dest)[N], const T* src, size_t count = 0 );
	template< class T >
	inline void CopyString( T* dest, size_t destCount, const T* src, size_t count = 0 );

	template< class T, size_t N >
	inline void AppendString( T (&dest)[N], const T* src, size_t count = 0 );
	template< class T >
	inline void AppendString( T* dest, size_t destCount, const T* src, size_t count = 0 );

	template < class T >
	inline int CompareString( const T* a, const T* b, size_t count = 0 );
	template < class T >
	inline int CaseInsensitiveCompareString( const T* a, const T* b, size_t count = 0 );

	template< class T >
	inline const T* FindCharacter( const T* data, const T value, size_t count = 0 );
	template< class T >
	inline const T* FindNextToken( const T* data, const T delim, size_t count = 0 );
	//@}


	template <typename CharType>
	const CharType* StringEnd(const CharType nullTermStr[])
	{ return &nullTermStr[StringLength<CharType>(nullTermStr)]; }

	template <typename CharType>
	CharType* StringEnd(CharType nullTermStr[])
	{ return &nullTermStr[StringLength<CharType>(nullTermStr)]; }


	/// @defgroup utilityindex Index Utility Functions
    //@{
    template< typename IndexType > IndexType Invalid();
    template< typename IndexType > bool IsValid( IndexType index );
    template< typename IndexType > bool IsInvalid( IndexType index );
    template< typename IndexType > void SetInvalid( IndexType& rIndex );
    template< typename DestIndexType, typename SourceIndexType > DestIndexType CastIndex( SourceIndexType index );
    //@}

    /// @defgroup utilitybit Bit Manipulation Utility Functions
    //@{
    template< typename ElementType > void GetBitElementAndMaskIndex( size_t bitIndex, size_t& rElementIndex, size_t& rMaskIndex );

    template< typename ElementType > bool GetBit( const ElementType& rElement, size_t maskIndex );
    template< typename ElementType > void SetBit( ElementType& rElement, size_t maskIndex );
    template< typename ElementType > void ClearBit( ElementType& rElement, size_t maskIndex );
    template< typename ElementType > void ToggleBit( ElementType& rElement, size_t maskIndex );

    template< typename ElementType > void SetBitRange( ElementType* pElements, size_t bitStart, size_t bitCount );
    template< typename ElementType > void ClearBitRange( ElementType* pElements, size_t bitStart, size_t bitCount );
    template< typename ElementType > void ToggleBitRange( ElementType* pElements, size_t bitStart, size_t bitCount );
    //@}

    /// @defgroup utilitydatareading Data Reading & Byte-swapping Utility Functions
    /// Note that all of these functions can perform in-place byte swapping (source and destination arguments are the same).
    //@{
    template< typename T > const void* LoadValue( T& rDest, const void* pSource );
    template< typename T > const void* LoadValueSwapped( T& rDest, const void* pSource );

    inline void ReverseByteOrder( void* pDest, const void* pSource, size_t size );
    //@}

	template<typename Type, int Count>
	Type* ArrayEnd(Type (&input)[Count]) { return &input[Count]; }

	template <typename Type>
	Type * PtrAdd( Type * input, ptrdiff_t offset )  { return (Type*)( size_t(input) + offset ); }

	template <typename TypeA>
	ptrdiff_t PtrDiff(TypeA * lhs, TypeA * rhs) { return ptrdiff_t(lhs) - ptrdiff_t(rhs); }

	template <typename TypeA>
	ptrdiff_t PtrDiff(TypeA * lhs, size_t rhs) { return ptrdiff_t(lhs) - ptrdiff_t(rhs); }


	template <typename Type>
	Type * AsPointer( Type * i )                     { return i; }

#ifdef _MSC_VER

	template <typename VecType>
	typename VecType::value_type * AsPointer(const std::_Vector_iterator<VecType> & i)
	{ return i._Ptr; }

	template <typename VecType>
	const typename VecType::value_type * AsPointer(const std::_Vector_const_iterator<VecType> & i)
	{ return i._Ptr; }

#if _MSC_VER >= 1700
	template <typename StringType>
				typename StringType::value_type * AsPointer(const std::_String_iterator<StringType>& i)
				{
					return (typename StringType::value_type*)i._Ptr;
				}

			template <typename StringType>
				const typename StringType::value_type * AsPointer(const std::_String_const_iterator<StringType>& i)
				{
					return (typename StringType::value_type*)i._Ptr;
				}
#else
	template <typename Type, typename Traits, typename Alloc>
	Type * AsPointer(const std::_String_iterator<Type, Traits, Alloc>& i)
	{
		return (Type*)i._Ptr;
	}

	template <typename Type, typename Traits, typename Alloc>
	const Type * AsPointer(const std::_String_const_iterator<Type, Traits, Alloc>& i)
	{
		return (Type*)i._Ptr;
	}
#endif

#else

	template <typename Type>
    Type * AsPointer( const typename std::vector<Type>::iterator & i )                   { return &(*i); }

    template <typename Type>
    Type * AsPointer(  typename std::vector<Type>::iterator & i )                   { return &(*i); }

    template <typename Type>
    const Type * AsPointer( const typename std::vector<Type>::const_iterator & i )       { return &(*i); }

    template <typename Type>
    Type * AsPointer( const typename __gnu_cxx::__normal_iterator<Type, std::vector<Type> > & i )       { return &(*i); }

#endif

    // STL-weak_ptr

    template<typename T>
    static bool Equivalent(const std::weak_ptr<T>& lhs, const std::weak_ptr<T>& rhs)
    {
        //  "owner_before" should compare the control block of these pointers in
        //  most cases. We want to check to see if both pointers have the same
        //  control block (and then consider them equivalent)
        return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
    }

    template<typename T>
    static bool Equivalent(const std::shared_ptr<T>& lhs, const std::weak_ptr<T>& rhs)
    {
        return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
    }

    template<typename T>
    static bool Equivalent(const std::weak_ptr<T>& lhs, const std::shared_ptr<T>& rhs)
    {
        return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
    }

    // EASTL-weak_ptr
    template<typename T>
    static bool Equivalent(const eastl::weak_ptr<T>& lhs, const eastl::weak_ptr<T>& rhs)
    {
        //  "owner_before" should compare the control block of these pointers in
        //  most cases. We want to check to see if both pointers have the same
        //  control block (and then consider them equivalent)
        return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
    }

    template<typename T>
    static bool Equivalent(const eastl::shared_ptr<T>& lhs, const eastl::weak_ptr<T>& rhs)
    {
        return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
    }

    template<typename T>
    static bool Equivalent(const eastl::weak_ptr<T>& lhs, const eastl::shared_ptr<T>& rhs)
    {
        return !lhs.owner_before(rhs) && !rhs.owner_before(lhs);
    }

	template<typename CharType> struct DemoteCharType { using Value = CharType; };
	template<> struct DemoteCharType<uint8_t> { using Value = char; };
	template<> struct DemoteCharType<uint16_t> { using Value = wchar_t; };
	template<> struct DemoteCharType<char16_t> { using Value = wchar_t; };
	template<> struct DemoteCharType<uint32_t> { using Value = char; };
	template<> struct DemoteCharType<char32_t> { using Value = wchar_t; };



    /// Non-copyable base class.
    class DRAGORA_PLATFORM_API NonCopyable
    {
    public:
        /// @name Construction/Destruction
        //@{
        inline NonCopyable();
        //@}

    private:
        /// @name Construction/Destruction, Private
        //@{
        NonCopyable( const NonCopyable& );  // Not implemented.
        //@}

        /// @name Overloaded Operators
        //@{
        NonCopyable& operator=( const NonCopyable& );  // Not implemented.
        //@}
    };
}

#include "Platform/Utility.inl"
