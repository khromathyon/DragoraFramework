#pragma once

#define MaxPath 260

// Operating system (DRAGORA_OS_*) macros:
// - DRAGORA_OS_PC: PC operating system (any)
//   - DRAGORA_OS_WIN: Windows (any architecture/version)
//     - DRAGORA_OS_WIN32: 32-bit Windows
//     - DRAGORA_OS_WIN64: 64-bit Windows

// CPU architecture (DRAGORA_CPU_*) macros:
// - DRAGORA_CPU_X86: Intel x86-based (any architecture)
//   - DRAGORA_CPU_X86_32: 32-bit Intel x86
//   - DRAGORA_CPU_X86_64: 64-bit Intel x86

// Compiler (DRAGORA_CC_*) macros:
// - DRAGORA_CC_CL: Microsoft Visual C++
// - DRAGORA_CC_GCC: GCC

#if defined( _WIN64 )
# define DRAGORA_OS_WIN 1
# define DRAGORA_OS_WIN64 1
# define DRAGORA_CPU_X86 1
# define DRAGORA_CPU_X86_64 1
#elif defined( _WIN32 )
# define DRAGORA_OS_WIN 1
# define DRAGORA_OS_WIN32 1
# define DRAGORA_CPU_X86 1
# define DRAGORA_CPU_X86_32 1
#elif defined( __APPLE__ )
# define DRAGORA_OS_MAC 1
# if defined( __x86_64__ )
#  define DRAGORA_OS_MAC64 1
#  define DRAGORA_CPU_X86 1
#  define DRAGORA_CPU_X86_64 1
# else
#  define DRAGORA_OS_MAC32 1
#  define DRAGORA_CPU_X86 1
#  define DRAGORA_CPU_X86_32 1
# endif
#elif defined( __gnu_linux__ )
# define DRAGORA_OS_LINUX 1
# if defined( __x86_64__ )
#  define DRAGORA_CPU_X86 1
#  define DRAGORA_CPU_X86_64 1
# else
#  define DRAGORA_CPU_X86 1
#  define DRAGORA_CPU_X86_32 1
# endif
#else
# error Unsupported platform.
#endif

#if DRAGORA_OS_WIN
# define DRAGORA_OS_PC 1
#endif

#if defined( _MSC_VER )
# if ( _MSC_FULL_VER < 150030729 )
#  error Dragora requires at least VS2008 with SP1 applied to compile.  Please update your compiler.
# endif
# include "Platform/CompilerCl.h"
#elif defined( __clang__ )
# include "Platform/CompilerClang.h"
#elif defined( __GNUC__ )
# include "Platform/CompilerGcc.h"
#else
# error Unsupported compiler.
#endif

#if DRAGORA_CPU_X86
# include "Platform/CpuX86.h"
#endif

#if DRAGORA_OS_WIN
# include "Platform/SystemWin.h"
#endif

#if DRAGORA_OS_MAC
# include "Platform/SystemMac.h"
#endif

#if DRAGORA_OS_LINUX
# include "Platform/SystemLinux.h"
#endif
