#include <algorithm>

#if defined(DRAGORA_CC_GCC)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-fpermissive"
#endif

namespace Dragora
{
    /////////////////////////////////////////////////////////////////////////////////
    //////   S I M P L E   S P A N N I N G   H E A P   (Still annoying shit, tho)  //
    /////////////////////////////////////////////////////////////////////////////////

    template <typename Marker>
    unsigned    SpanningHeap<Marker>::Allocate(unsigned size)
    {
        const Marker sentinel = Marker(~0x0);
        Marker bestSize = sentinel;
        Marker internalSize = this->ToInternalSize(this->AlignSize(size));
        DRAGORA_ASSERT(this->ToExternalSize(internalSize)>=size);

        if ((_largestFreeBlockValid && _largestFreeBlock<internalSize) || !_markers.size()) {
            return ~unsigned(0x0);  // threading can cause false return here -- but that shouldn't be a major issue
        }

        MutexScopeLock locker(_lock);

        //  Marker array is simple -- just a list of positions. It will alternate between
        //  allocated and unallocated
        Marker largestFreeBlock[2] = {0,0};
        Marker largestFreeBlockPosition = 0;

        typename std::vector<Marker>::iterator best = _markers.end();
        for (typename std::vector<Marker>::iterator i=_markers.begin(); i<(_markers.end()-1);i+=2) {
            Marker blockSize = *(i+1) - *i;
            if (blockSize >= internalSize && blockSize < bestSize) {
                bestSize = blockSize;
                best = i;
            }
            if (blockSize >= largestFreeBlock[0]) {
                largestFreeBlock[1] = largestFreeBlock[0];
                largestFreeBlock[0] = blockSize;
                largestFreeBlockPosition = *i;
            } else if (blockSize > largestFreeBlock[1]) {
                largestFreeBlock[1] = blockSize;
            }
        }

        if (bestSize == sentinel) {
            _largestFreeBlock = largestFreeBlock[0];
            _largestFreeBlockValid = true;
            DRAGORA_ASSERT(largestFreeBlock[0] < size);
            return ~unsigned(0x0);
        }

        {
            if (largestFreeBlockPosition==*best) {
                _largestFreeBlock = std::max(Marker(largestFreeBlock[0]-internalSize), largestFreeBlock[1]);
            } else {
                _largestFreeBlock = largestFreeBlock[0];
            }
            _largestFreeBlockValid = true;
        }

        if (bestSize == internalSize) {
            //  Got an exact match. In this case we remove 2 markers, because the entire span has become
            //  allocated, and should just merge into the spans around it.
            unsigned result = ToExternalSize(*best);
            if (best == _markers.begin()) {
                if (_markers.size()==2) {   // special case for unallocated heap to fully allocated heap (0,0) -> (0,0,size)
                    _markers.insert(_markers.begin(), 0);
                } else {
                    *(best+1) = 0;
                }
            } else {
                if (best+2 >= _markers.end()) {
                    _markers.erase(best);
                } else {
                    _markers.erase(best, best+2);
                }
            }
            DRAGORA_ASSERT(_markers[0]==0);
            DRAGORA_ASSERT(_largestFreeBlock==CalculateLargestFreeBlock_Internal());
            if (_markers.size()==2) {DRAGORA_ASSERT(_markers[0]==0 && _markers[1]!=0);}
            return result;
        } else {
            //  We'll allocate from the start of the span space.
            unsigned result = ToExternalSize(*best);
            if (best == _markers.begin()) {
                //      We're allocating from the start of the heap. But we can't move the marker
                //      at the start of the heap, so we have to insert 2 more...
                Marker insertion[] = {0, internalSize};
                _markers.insert(_markers.begin()+1, insertion, &insertion[DRAGORA_ARRAY_COUNT(insertion)]);
            } else {
                *best += internalSize;
            }
            if (_markers.size()==2) {DRAGORA_ASSERT(_markers[0]==0 && _markers[1]!=0);}
            DRAGORA_ASSERT(_largestFreeBlock==CalculateLargestFreeBlock_Internal());
            return result;
        }
    }

    template <typename Marker>
    bool        SpanningHeap<Marker>::Allocate(unsigned ptr, unsigned size)
    {
        return BlockAdjust_Internal(ptr, size, true);
    }

    template <typename Marker>
    bool        SpanningHeap<Marker>::Deallocate(unsigned ptr, unsigned size)
    {
        return BlockAdjust_Internal(ptr, size, false);
    }

    template <typename Marker>
    bool        SpanningHeap<Marker>::BlockAdjust_Internal(unsigned ptr, unsigned size, bool allocateOperation)
    {
        MutexScopeLock locker(_lock);
        Marker internalOffset = this->ToInternalSize(ptr);
        Marker internalSize = this->ToInternalSize(this->AlignSize(size));
        if (_markers.size()==2) {DRAGORA_ASSERT(_markers[0]==0 && _markers[1]!=0);}

        _largestFreeBlockValid = false; // have to recalculate largest free after deallocate. We could update based on local changes, but...

        // find the span in which this belongs, and mark the space deallocated
        typename std::vector<Marker>::iterator i = _markers.begin()+(allocateOperation?0:1);
        for (; (i+1)<_markers.end();i+=2) {
            Marker start = *i;
            Marker end = *(i+1);
            if (internalOffset >= start && internalOffset < end) {
                DRAGORA_ASSERT((internalOffset+internalSize) <= end);
                if (start == internalOffset) {
                    if (end == (internalOffset+internalSize)) {
                        // the entire span is begin destroyed.
                        if (i == _markers.begin() && allocateOperation) {
                            *(i+1) = 0;
                        } else if (i+2 >= _markers.end()) {
                            _markers.erase(i);
                        } else {
                            _markers.erase(i, i+2);
                        }
                        DRAGORA_ASSERT(_markers[0]==0);
                        if (_markers.size()==2) {DRAGORA_ASSERT(_markers[0]==0 && _markers[1]!=0);}
                        return true;
                    }

                    // we can just move the start marker up to cover the unallocated space
                    if (i == _markers.begin() && allocateOperation) {
                        Marker insertion[] = {internalOffset, Marker(internalOffset+internalSize)};
                        _markers.insert(i+1, insertion, &insertion[DRAGORA_ARRAY_COUNT(insertion)]);
                    } else {
                        *i = internalOffset+internalSize;
                    }
                    DRAGORA_ASSERT(_markers[0]==0);
                    if (_markers.size()==2) {DRAGORA_ASSERT(_markers[0]==0 && _markers[1]!=0);}
                    return true;
                } else if (end == (internalOffset+internalSize)) {
                    // move the end marker back to cover the space (but not if it's the end sentinel)
                    if (i+2 >= _markers.end()) {
                        _markers.insert(i+1, internalOffset);
                    } else {
                        *(i+1) = internalOffset;
                    }
                    DRAGORA_ASSERT(_markers[0]==0);
                    if (_markers.size()==2) {DRAGORA_ASSERT(_markers[0]==0 && _markers[1]!=0);}
                    return true;
                } else {
                    // create new markers to match the deallocated space
                    Marker insertion[] = {internalOffset, Marker(internalOffset+internalSize)};
                    _markers.insert(i+1, insertion, &insertion[DRAGORA_ARRAY_COUNT(insertion)]);
                    if (_markers.size()==2) {DRAGORA_ASSERT(_markers[0]==0 && _markers[1]!=0);}
                    return true;
                }
            }
        }

        DRAGORA_ASSERT(0);      // couldn't find it within our heap
        return false;
    }

    template <typename Marker>
    auto SpanningHeap<Marker>::CalculateLargestFreeBlock_Internal() const -> Marker
    {
        Marker largestBlock = 0;
        DRAGORA_ASSERT(!_markers.empty());
        typename std::vector<Marker>::const_iterator i = _markers.begin();
        for (; i<(_markers.end()-1);i+=2) {
            Marker start = *i;
            Marker end = *(i+1);
            largestBlock = std::max(largestBlock, Marker(end-start));
        }
        return largestBlock;
    }

    template <typename Marker>
    unsigned    SpanningHeap<Marker>::CalculateAvailableSpace() const
    {
        MutexScopeLock locker(_lock);
        unsigned result = 0;
        typename std::vector<Marker>::const_iterator i = _markers.begin();
        for (; (i+1)<_markers.end();i+=2) {
            Marker start = *i;
            Marker end = *(i+1);
            result += end-start;
        }
        return ToExternalSize(Marker(result));
    }

    template <typename Marker>
    unsigned    SpanningHeap<Marker>::CalculateLargestFreeBlock() const
    {
        MutexScopeLock locker(_lock);
        if (!_largestFreeBlockValid) {
            _largestFreeBlock = CalculateLargestFreeBlock_Internal();
            _largestFreeBlockValid = true;
        }
        return ToExternalSize(_largestFreeBlock);
    }

    template <typename Marker>
    unsigned    SpanningHeap<Marker>::CalculateAllocatedSpace() const
    {
        MutexScopeLock locker(_lock);
        if (_markers.empty()) return 0;

        unsigned result = 0;
        typename std::vector<Marker>::const_iterator i = _markers.begin()+1;
        for (; i<(_markers.end()-1);i+=2) {
            Marker start = *i;
            Marker end = *(i+1);
            result += end-start;
        }
        return ToExternalSize(Marker(result));
    }

    template <typename Marker>
    unsigned    SpanningHeap<Marker>::CalculateHeapSize() const
    {
        MutexScopeLock locker(_lock);
        if (_markers.empty()) {
            return 0;
        }
        return ToExternalSize(_markers[_markers.size()-1]);
    }

    template <typename Marker>
    unsigned        SpanningHeap<Marker>::AppendNewBlock(unsigned size)
    {
        MutexScopeLock locker(_lock);
        if (!_markers.size()) {
            _markers.push_back(0);
            _markers.push_back(0);
            _markers.push_back(this->ToInternalSize(this->AlignSize(size)));
            return 0;
        }

        // append a new block in an allocated status

        const bool endsInAllocatedBlock = _markers.size()&1;  // odd markers == ends in allocated block
        auto finalMarker = _markers[_markers.size()-1];
        auto newBlockInternalSize = this->ToInternalSize(this->AlignSize(size));
        DRAGORA_ASSERT((unsigned(finalMarker) + unsigned(newBlockInternalSize)) <= std::numeric_limits<Marker>::max());
        auto newEnd = Marker(finalMarker + newBlockInternalSize);
        if (endsInAllocatedBlock) {
            _markers[_markers.size()-1] = newEnd;     // just shift the end marker back
        } else {
            _markers.push_back(newEnd);               // add a final allocated block
        }

        return ToExternalSize(finalMarker);
    }

    template <typename Marker>
    uint64_t      SpanningHeap<Marker>::CalculateHash() const
    {
        MutexScopeLock locker(_lock);
        //return Hash64(AsPointer(_markers.begin()), AsPointer(_markers.end()));
    }

    template <typename Marker>
    bool        SpanningHeap<Marker>::IsEmpty() const
    {
        MutexScopeLock locker(_lock);
        return _markers.size() <= 2;
    }

    template <typename Marker>
    std::vector<unsigned> SpanningHeap<Marker>::CalculateMetrics() const
    {
        MutexScopeLock locker(_lock);
        std::vector<unsigned> result;
        result.reserve(_markers.size());
        typename std::vector<Marker>::const_iterator i = _markers.begin();
        for (; i!=_markers.end();++i) {
            result.push_back(ToExternalSize(*i));
        }
#if defined(DRAGORA_DEBUG)
        DRAGORA_ASSERT(!_largestFreeBlockValid || _largestFreeBlock==CalculateLargestFreeBlock_Internal());
#endif
        return result;
    }

    // static bool SortAllocatedBlocks_LargestToSmallest(const std::pair<MarkerHeap::Marker, MarkerHeap::Marker>& lhs, const std::pair<MarkerHeap::Marker, MarkerHeap::Marker>& rhs)
    // {
    //     return (lhs.second-lhs.first)>(rhs.second-rhs.first);
    // }

    template<typename Marker>
    static bool SortAllocatedBlocks_SmallestToLargest(
            const std::pair<Marker, Marker>& lhs,
            const std::pair<Marker, Marker>& rhs)
    {
        return (lhs.second-lhs.first)<(rhs.second-rhs.first);
    }

    static bool SortDefragStep_SourceStart(const DefragStep& lhs, const DefragStep& rhs)
    {
        return lhs._sourceStart < rhs._sourceEnd;
    }

    static bool SortDefragStep_Destination(const DefragStep& lhs, const DefragStep& rhs)
    {
        return lhs._destination < rhs._destination;
    }

    template <typename Marker>
    std::vector<DefragStep> SpanningHeap<Marker>::CalculateDefragSteps() const
    {
        MutexScopeLock locker(_lock);

        std::vector<std::pair<Marker, Marker> > allocatedBlocks;
        allocatedBlocks.reserve(_markers.size()/2);
        typename std::vector<Marker>::const_iterator i = _markers.begin()+1;
        for (; (i+1)<_markers.end();i+=2) {
            Marker start = *i;
            Marker end   = *(i+1);
            DRAGORA_ASSERT(start < end);
            allocatedBlocks.push_back(std::make_pair(start, end));
        }

        //
        //      Very simple heap compression method...
        //          we're going to be writing to a new buffer, so we don't have to worry about
        //          writing over data that we'll read later on. So we can safely re-order the
        //          blocks how we like.
        //
        //      We could write a method that only moves smaller blocks, without creating mirror
        //      resources... But that would require a convenient method to copy data from one
        //      area in a resource to another -- and that might not be possible efficiently in D3D.
        //

        std::sort(allocatedBlocks.begin(), allocatedBlocks.end(), SortAllocatedBlocks_SmallestToLargest<Marker>);

        std::vector<DefragStep> result;
        result.reserve(allocatedBlocks.size());

        Marker compressedPosition = 0;
        for (typename std::vector<std::pair<Marker, Marker> >::const_iterator i=allocatedBlocks.begin(); i!=allocatedBlocks.end(); ++i) {
            DRAGORA_ASSERT(i->first < i->second);
            DefragStep step;
            step._sourceStart    = ToExternalSize(i->first);
            step._sourceEnd      = ToExternalSize(i->second);
            step._destination    = ToExternalSize(compressedPosition);
            DRAGORA_ASSERT(step._destination < 512*1024);
            DRAGORA_ASSERT((step._destination + step._sourceEnd - step._sourceStart) <= ToExternalSize(_markers[_markers.size()-1]));
            DRAGORA_ASSERT(step._sourceStart < step._sourceEnd);
            compressedPosition += i->second - i->first;
            result.push_back(step);
        }

        std::sort(result.begin(), result.end(), SortDefragStep_SourceStart);

        // check for sane boundary
#if defined(XL_DEBUG)
        for (std::vector<DefragStep>::iterator i=result.begin(); i!=result.end(); ++i) {
                DRAGORA_ASSERT(i->_destination < 512*1024);
            }
#endif

        return result;
    }

    template <typename Marker>
    void        SpanningHeap<Marker>::PerformDefrag(const std::vector<DefragStep>& defrag)
    {
        MutexScopeLock locker(_lock);

        //
        //      All of the spans in the heap have moved about we have to recalculate the
        //      allocated spans from scratch, based on the positions of the new blocks
        //
        unsigned startingAvailableSize = CalculateAvailableSpace(); (void)startingAvailableSize;
        unsigned startingLargestBlock = CalculateLargestFreeBlock(); (void)startingLargestBlock;

        Marker heapEnd = _markers[_markers.size()-1];
        _markers.erase(_markers.begin(), _markers.end());
        _markers.push_back(0);
        if (!defrag.empty()) {
            std::vector<DefragStep> defragByDestination(defrag);
            std::sort(defragByDestination.begin(), defragByDestination.end(), SortDefragStep_Destination);

            Marker currentAllocatedBlockBegin    = this->ToInternalSize(defragByDestination.begin()->_destination);
            Marker currentAllocatedBlockEnd      = this->ToInternalSize(defragByDestination.begin()->_destination + this->AlignSize(defragByDestination.begin()->_sourceEnd-defragByDestination.begin()->_sourceStart));

            for (std::vector<DefragStep>::const_iterator i=defragByDestination.begin()+1; i!=defragByDestination.end(); ++i) {
                Marker blockBegin    = this->ToInternalSize(i->_destination);
                Marker blockEnd      = this->ToInternalSize(i->_destination+ this->AlignSize(i->_sourceEnd-i->_sourceStart));

                if (blockBegin == currentAllocatedBlockEnd) {
                    currentAllocatedBlockEnd = blockEnd;
                } else {
                    _markers.push_back(currentAllocatedBlockBegin);
                    _markers.push_back(currentAllocatedBlockEnd);
                    currentAllocatedBlockBegin = blockBegin;
                    currentAllocatedBlockEnd = blockEnd;
                }
            }

            _markers.push_back(currentAllocatedBlockBegin);
            _markers.push_back(currentAllocatedBlockEnd);
        }
        _markers.push_back(heapEnd);
        _largestFreeBlockValid = false;

        unsigned newAvailableSpace = CalculateAvailableSpace(); (void)newAvailableSpace;
        unsigned newLargestBlock = CalculateLargestFreeBlock(); (void)newLargestBlock;
        DRAGORA_ASSERT(newAvailableSpace == startingAvailableSize);
        DRAGORA_ASSERT(newLargestBlock >= startingLargestBlock);        // sometimes the tests will run a defrag that doesn't reduce the largest block
    }

    template <typename Marker>
    std::pair<std::unique_ptr<uint8_t[]>, size_t> SpanningHeap<Marker>::Flatten() const
    {
        // return a "serialized" / flattened representation of this heap
        //  -- useful to write it out to disk, or store in a compact form
        MutexScopeLock locker(_lock);

        if (_markers.size() >= 2) {
            for (auto i=_markers.cbegin()+1; i!=_markers.cend(); ++i) {
                DRAGORA_ASSERT(*(i-1) <= *i);
            }
        }

        size_t resultSize = sizeof(Marker) * _markers.size();
        auto result = std::make_unique<uint8_t[]>(resultSize);
        MemoryCopy(result.get(), AsPointer(_markers.begin()), resultSize);
        return std::make_pair(std::move(result), resultSize);
    }

    template <typename Marker>
    SpanningHeap<Marker>::SpanningHeap(unsigned size)
    {
        _markers.reserve(64);
        _markers.push_back(0);
        _markers.push_back(ToInternalSize(this->AlignSize(size)));
        _largestFreeBlockValid = false;
        _largestFreeBlock = 0;
    }

    template <typename Marker>
    SpanningHeap<Marker>::SpanningHeap(const uint8_t flattened[], size_t flattenedSize)
    {
        _largestFreeBlockValid = false;
        _largestFreeBlock = 0;
        // flattened rep is just a copy of the markers array... we can copy it straight in...
        auto markerCount = flattenedSize / sizeof(Marker);
        _markers.resize(markerCount);
        std::copy(
                (const Marker*)flattened, (const Marker*)PtrAdd(flattened, markerCount * sizeof(Marker)),
                _markers.begin());

        // make sure things are in the right order
        if (_markers.size() >= 2) {
            for (auto i=_markers.cbegin()+1; i!=_markers.cend(); ++i) {
                DRAGORA_ASSERT(*(i-1) <= *i);
            }
        }
    }

    template <typename Marker>
    SpanningHeap<Marker>::SpanningHeap(SpanningHeap&& moveFrom) never_throws
            : _markers(std::move(moveFrom._markers))
            , _largestFreeBlock(moveFrom._largestFreeBlock)
            , _largestFreeBlockValid(moveFrom._largestFreeBlockValid)
    {
    }

    template <typename Marker>
    auto SpanningHeap<Marker>::operator=(SpanningHeap&& moveFrom) never_throws -> const SpanningHeap&
    {
        _markers = std::move(moveFrom._markers);
        _largestFreeBlock = moveFrom._largestFreeBlock;
        _largestFreeBlockValid = moveFrom._largestFreeBlockValid;
        return *this;
    }

    template <typename Marker>
    SpanningHeap<Marker>::SpanningHeap(const SpanningHeap<Marker>& cloneFrom)
            : _markers(cloneFrom._markers)
            , _largestFreeBlock(cloneFrom._largestFreeBlock)
            , _largestFreeBlockValid(cloneFrom._largestFreeBlockValid)
    {}

    template <typename Marker>
    const SpanningHeap<Marker>& SpanningHeap<Marker>::operator=(const SpanningHeap<Marker>& cloneFrom)
    {
        _markers = cloneFrom._markers;
        _largestFreeBlock = cloneFrom._largestFreeBlock;
        _largestFreeBlockValid = cloneFrom._largestFreeBlockValid;
        return *this;
    }

    template <typename Marker>
    SpanningHeap<Marker>::SpanningHeap() : _largestFreeBlock(0), _largestFreeBlockValid(false) {}

    template <typename Marker>
    SpanningHeap<Marker>::~SpanningHeap()
    {}

}

#if defined(DRAGORA_CC_GCC)
#pragma GCC diagnostic pop
#endif