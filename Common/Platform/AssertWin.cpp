#include "PlatformPch.h"
#include "Platform/System.h"
#include "Platform/Assert.h"
#include "Platform/Console.h"
#include "Platform/Encoding.h"

#if DRAGORA_OS_WIN

using namespace Dragora;

/// Terminate the application on a fatal error.
///
/// @param[in] exitCode  Error code associated with the exit.
void Dragora::FatalExit( int exitCode )
{
	::FatalExit( exitCode );
}

#if DRAGORA_ASSERT_ENABLED

/// Handle an assertion.
///
/// @param[in] pMessageText  Assert message text.
bool Assert::TriggerImplementation( const char* pMessageText )
{
	if ( ::IsDebuggerPresent() )
	{
		return true;
	}

	char messageBoxText[ 1024 ];
	StringPrint(
		messageBoxText,
		( TXT( "%s\n\nChoose \"Abort\" to terminate the program, \"Retry\" to debug the program (if a debugger " )
		  TXT( "is attached), or \"Ignore\" to attempt to skip over the error." ) ),
		pMessageText );

	DRAGORA_TCHAR_TO_WIDE( messageBoxText, wideMessageBoxText );
	switch( MessageBoxW( NULL, wideMessageBoxText, L"Assert", MB_ABORTRETRYIGNORE ) )
	{
	case IDABORT:
		Dragora::FatalExit( -1 );

	case IDIGNORE:
		return false;

	default:
		return true;
	}
}

#endif  // DRAGORA_OS_WIN

#endif  // DRAGORA_ASSERT_ENABLED
