#pragma once

#include "Platform/System.h"


//
// Windows
//

#if DRAGORA_OS_WIN


# if DRAGORA_CPP11
#include <type_traits>

#if _MSC_VER >= 1900
namespace std
{
	template< class T > struct has_trivial_assign : is_trivially_copy_assignable< T > {};
	template< class T > struct has_trivial_constructor : is_trivially_default_constructible< T > {};
	template< class T > struct has_trivial_destructor : is_trivially_destructible< T > {};
	template< class T > struct has_trivial_copy : is_trivially_copy_constructible< T > {};
}
#endif

# else // DRAGORA_CPP11
#  include <type_traits>

namespace std
{
	namespace tr1 {}
	using namespace tr1;
}

# endif // DRAGORA_CPP11


//
// Linux
//

#elif DRAGORA_OS_LINUX // DRAGORA_OS_WIN

# if DRAGORA_CPP11
#  include <type_traits>
#if __GLIBCXX__ > 20131008 && __GNUC__ >= 5 // GCC 5.0 series, no fucking idea what version of GLib deprecated the old C++0x versions.
namespace std
{
    template< class T > struct has_trivial_assign : is_trivially_copy_assignable< T > {};
    template< class T > struct has_trivial_constructor : is_trivially_default_constructible< T > {};
    template< class T > struct has_trivial_destructor : is_trivially_destructible< T > {};
    template< class T > struct has_trivial_copy : is_trivially_copy_constructible< T > {};
}
# elif __GLIBCXX__ >= 20131008 // 4.8 or later
namespace std
{
	template< class T > struct has_trivial_assign : has_trivial_copy_assign< T > {};
	template< class T > struct has_trivial_constructor : has_trivial_default_constructor< T > {};
	template< class T > struct has_trivial_destructor : is_trivially_destructible< T > {};
	template< class T > struct has_trivial_copy : has_trivial_copy_assign< T > {};
}

#  else // earlier than 4.8

namespace std
{
	template< class T > struct has_trivial_assign : has_trivial_copy_assign< T > {};
	template< class T > struct has_trivial_constructor : has_trivial_default_constructor< T > {};
	template< class T > struct has_trivial_copy : has_trivial_copy_assign< T > {};
}

#  endif // glib version check

# else // DRAGORA_CPP11

#  include <tr1/type_traits>

namespace std
{
	namespace tr1 {}
	using tr1::true_type;
	using tr1::false_type;
	using tr1::integral_constant;
	using tr1::is_base_of;
	using tr1::is_pointer;
	using tr1::is_reference;
	using tr1::is_signed;
	using tr1::is_array;
	using tr1::remove_cv;
	using tr1::remove_extent;
	using tr1::remove_reference;
	using tr1::alignment_of;
	using tr1::has_trivial_assign;
	using tr1::has_trivial_constructor;
	using tr1::has_trivial_destructor;
	using tr1::has_trivial_copy;
}

# endif // DRAGORA_CPP11


//
// Mac
//

#elif DRAGORA_OS_MAC // DRAGORA_OS_LINUX

# if DRAGORA_CC_GCC

#  include <tr1/type_traits>

namespace std
{
	namespace tr1 {}
	using tr1::true_type;
	using tr1::false_type;
	using tr1::integral_constant;
	using tr1::is_base_of;
	using tr1::is_pointer;
	using tr1::is_reference;
	using tr1::is_signed;
	using tr1::is_array;
	using tr1::remove_cv;
	using tr1::remove_extent;
	using tr1::remove_reference;
	using tr1::alignment_of;
	using tr1::has_trivial_assign;
	using tr1::has_trivial_constructor;
	using tr1::has_trivial_destructor;
	using tr1::has_trivial_copy;
}

# elif DRAGORA_CC_CLANG // DRAGORA_CC_GCC

#  include <type_traits>

namespace std
{
	template< class T > struct has_trivial_assign : is_trivially_assignable< T, T > {};
	template< class T > struct has_trivial_constructor : is_trivially_constructible< T > {};
	template< class T > struct has_trivial_destructor : is_trivially_destructible< T > {};
	template< class T > struct has_trivial_copy : is_trivially_copyable< T > {};
}

# endif // DRAGORA_CC_CLANG

#endif // DRAGORA_OS_MAC
