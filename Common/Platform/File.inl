const std::string& Dragora::Directory::GetPath()
{
	return m_Path;
}

void Dragora::Directory::SetPath( const std::string& path )
{
	Close();
	m_Path = path;
}
