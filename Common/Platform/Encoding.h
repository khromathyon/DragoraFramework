#pragma once

#include <string.h>

#include "Platform/API.h"
#include "Platform/Types.h"

namespace Dragora
{
	DRAGORA_PLATFORM_API inline bool ConvertString( const char* src, char* dest, size_t destCount );
	DRAGORA_PLATFORM_API inline bool ConvertString( const std::string& src, std::string& dest );
	DRAGORA_PLATFORM_API inline bool ConvertString( const wchar_t* src, wchar_t* dest, size_t destCount );
	DRAGORA_PLATFORM_API inline bool ConvertString( const std::wstring& src, std::wstring& dest );

	DRAGORA_PLATFORM_API bool ConvertString( const char* src, wchar_t* dest, size_t destCount );
	DRAGORA_PLATFORM_API bool ConvertString( const wchar_t* src, char* dest, size_t destCount );

	DRAGORA_PLATFORM_API size_t GetConvertedStringLength( const char* src );
	DRAGORA_PLATFORM_API size_t GetConvertedStringLength( const wchar_t* src );

	DRAGORA_PLATFORM_API bool ConvertString( const std::string& src, std::wstring& dest );
	DRAGORA_PLATFORM_API bool ConvertString( const std::wstring& src, std::string& dest );
}

#define DRAGORA_CONVERT_TO_CHAR( chars, convertedChars ) \
	char* convertedChars = NULL; \
	if ( chars ) \
	{ \
		size_t convertedChars##Count = Dragora::GetConvertedStringLength( chars ); \
		convertedChars = (char*)alloca( convertedChars##Count * sizeof( char ) ); \
		Dragora::ConvertString( chars, convertedChars, convertedChars##Count ); \
	}
#define DRAGORA_CONVERT_TO_WIDE( chars, convertedChars ) \
	wchar_t* convertedChars = NULL; \
	if ( chars ) \
	{ \
		size_t convertedChars##Count = Dragora::GetConvertedStringLength( chars ); \
		convertedChars = (wchar_t*)alloca( convertedChars##Count * sizeof( wchar_t ) ); \
		Dragora::ConvertString( chars, convertedChars, convertedChars##Count ); \
	}

#define DRAGORA_TCHAR_TO_CHAR( chars, convertedChars ) const wchar_t* convertedChars = chars;
#define DRAGORA_CHAR_TO_TCHAR( chars, convertedChars ) const wchar_t* convertedChars = chars;
#define DRAGORA_TCHAR_TO_WIDE( chars, convertedChars ) DRAGORA_CONVERT_TO_WIDE( chars, convertedChars )
#define DRAGORA_WIDE_TO_TCHAR( chars, convertedChars ) DRAGORA_CONVERT_TO_CHAR( chars, convertedChars )

#include "Platform/Encoding.inl"
