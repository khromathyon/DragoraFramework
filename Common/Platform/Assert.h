#pragma once

#include "Platform/API.h"
#include "Platform/Types.h"

//
// Setup state
//

// this is for cross-platform code that uses gcc's unused attribute to remove locals
#define DRAGORA_ASSERT_ONLY

// this sets the master flag if we are going to compile in asserts or not
#if !defined( DRAGORA_ASSERT_ENABLED )
# if !defined( DRAGORA_RELEASE ) && !defined( DRAGORA_PROFILE )
#  define DRAGORA_ASSERT_ENABLED 1
# else
#  define DRAGORA_ASSERT_ENABLED 0
# endif
#endif


//
// Define the main macros
//

#if DRAGORA_CC_CL
# ifdef _MANAGED
#  define DRAGORA_ISSUE_BREAK() System::Diagnostics::Debugger::Break()
# else //_MANAGED
#  define DRAGORA_ISSUE_BREAK() __debugbreak()
# endif //_MANAGED
# define DRAGORA_FUNCTION_NAME __FUNCSIG__
#else
# if DRAGORA_CC_GCC
#  define DRAGORA_ISSUE_BREAK() __builtin_trap()
# elif DRAGORA_CC_CLANG
#  define DRAGORA_ISSUE_BREAK() __builtin_debugtrap()
# elif DRAGORA_CC_SNC
#  define DRAGORA_ISSUE_BREAK() __builtin_snpause()
# else
#  error Platform has no break intrinsic!
# endif
# define DRAGORA_FUNCTION_NAME __PRETTY_FUNCTION__
#endif

#if DRAGORA_CC_CLANG

# define DRAGORA_CC_CLANG_PRAGMA( PRAGMA ) _Pragma( #PRAGMA )
# define DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	DRAGORA_CC_CLANG_PRAGMA( clang diagnostic push ) \
	DRAGORA_CC_CLANG_PRAGMA( clang diagnostic ignored "-Wunused-value" )
# define DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	DRAGORA_CC_CLANG_PRAGMA( clang diagnostic pop )

#else // DRAGORA_CC_CLANG

# define DRAGORA_CC_CLANG_PRAGMA( PRAGMA )
# define DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING
# define DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING

#endif // DRAGORA_CC_CLANG

namespace Dragora
{
	DRAGORA_PLATFORM_API void FatalExit( int exitCode );
}

#if DRAGORA_ASSERT_ENABLED

namespace Dragora
{
	/// Assert utility functions.
	class DRAGORA_PLATFORM_API Assert
	{
	public:
		/// @name Static Utility Functions
		//@{
		static bool Trigger( const char* pExpression, const char* pFunction, const char* pFile, int line, const char* pMessage, ... );
		//@}

	private:
		/// Non-zero if the assert handler is currently active, zero if not.
		static volatile int32_t sm_active;

		/// @name Private Static Utility Functions
		//@{
		static bool TriggerImplementation( const char* pMessageText );
		//@}
	};
}

/// Trigger the assert handler function and trigger a break if necessary
///
/// @param[in] EXP      Expression string.
/// @param[in] MESSAGE  Message string.
#define DRAGORA_TRIGGER_ASSERT_HANDLER( EXP, ... ) \
	( Dragora::Assert::Trigger( EXP, TXT(DRAGORA_FUNCTION_NAME), TXT(__FILE__), __LINE__, __VA_ARGS__ ) ? ( DRAGORA_ISSUE_BREAK(), true ) : false )

/// Trigger a debug breakpoint unconditionally in non-release builds.
///
/// @see DRAGORA_ASSERT(), DRAGORA_ASSERT_MSG(), DRAGORA_BREAK_MSG(), DRAGORA_VERIFY(), DRAGORA_VERIFY_MSG()
#define DRAGORA_BREAK() \
DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	DRAGORA_TRIGGER_ASSERT_HANDLER( NULL, NULL ); \
DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING

/// Trigger a debug breakpoint with a customized message unconditionally in non-release builds.
///
/// @param[in] ...  Message to display if the assertion is triggered.
///
/// @see DRAGORA_ASSERT(), DRAGORA_ASSERT_MSG(), DRAGORA_BREAK(), DRAGORA_VERIFY(), DRAGORA_VERIFY_MSG()
#define DRAGORA_BREAK_MSG( ... ) \
DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	DRAGORA_TRIGGER_ASSERT_HANDLER( NULL, __VA_ARGS__ ); \
DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING

/// Trigger a debug breakpoint if the result of an expression is false in non-release builds.
///
/// @param[in] EXP  Expression to evaluate.
///
/// @see DRAGORA_ASSERT_MSG(), DRAGORA_BREAK(), DRAGORA_BREAK_MSG(), DRAGORA_VERIFY(), DRAGORA_VERIFY_MSG()
#define DRAGORA_ASSERT( EXP ) \
	{ \
		if( !( EXP ) ) \
DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
			DRAGORA_TRIGGER_ASSERT_HANDLER( TXT( #EXP ), NULL ); \
DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	}

/// Trigger a debug breakpoint with a customized message if the result of an expression is false in non-release builds.
///
/// @param[in] EXP      Expression to evaluate.
/// @param[in] ...      Message to display if the assertion is triggered.
///
/// @see DRAGORA_ASSERT(), DRAGORA_BREAK(), DRAGORA_BREAK_MSG() DRAGORA_VERIFY(), DRAGORA_VERIFY_MSG()
#define DRAGORA_ASSERT_MSG( EXP, ... ) \
	{ \
		if( !( EXP ) ) \
DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
			DRAGORA_TRIGGER_ASSERT_HANDLER( TXT( #EXP ), __VA_ARGS__ ); \
DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	}

/// Trigger a debug breakpoint if the result of an expression is false in non-release builds while still evaluating the
/// expression in release builds, and evaluating to the result of the expression.
///
/// @param[in] EXP  Expression to evaluate.
///
/// @see DRAGORA_ASSERT(), DRAGORA_ASSERT_MSG(), DRAGORA_BREAK(), DRAGORA_BREAK_MSG(), DRAGORA_VERIFY_MSG()
#define DRAGORA_VERIFY( EXP ) \
DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	( ( EXP ) ? true : ( DRAGORA_TRIGGER_ASSERT_HANDLER( TXT( #EXP ), NULL ), false ) ) \
DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING

/// Trigger a debug breakpoint with a customized message if the result of an expression is false in non-release builds
/// while still evaluating the expression in release builds, and evaluating to the result of the expression.
///
/// @param[in] EXP      Expression to evaluate.
/// @param[in] ...      Message to display if the assertion is triggered.
///
/// @see DRAGORA_ASSERT(), DRAGORA_ASSERT_MSG(), DRAGORA_BREAK(), DRAGORA_BREAK_MSG(), DRAGORA_VERIFY()
#define DRAGORA_VERIFY_MSG( EXP, ... ) \
DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	( ( EXP ) ? true : ( DRAGORA_TRIGGER_ASSERT_HANDLER( TXT( #EXP ), NULL ), false ) ) \
DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING

#else  // DRAGORA_ASSERT_ENABLED

/// Trigger a debug breakpoint with a customized message unconditionally in non-release builds.
///
/// @param[in] ...  Message to display if the assertion is triggered.
///
/// @see DRAGORA_ASSERT(), DRAGORA_ASSERT_MSG(), DRAGORA_BREAK(), DRAGORA_VERIFY(), DRAGORA_VERIFY_MSG()
#define DRAGORA_BREAK()

/// Trigger a debug breakpoint with a customized message unconditionally in non-release builds.
///
/// @param[in] ...  Message to display if the assertion is triggered.
///
/// @see DRAGORA_ASSERT(), DRAGORA_ASSERT_MSG(), DRAGORA_BREAK(), DRAGORA_VERIFY(), DRAGORA_VERIFY_MSG()
#define DRAGORA_BREAK_MSG( ... )

/// Trigger a debug breakpoint if the result of an expression is false in non-release builds.
///
/// @param[in] EXP  Expression to evaluate.
///
/// @see DRAGORA_ASSERT_MSG(), DRAGORA_BREAK(), DRAGORA_BREAK_MSG(), DRAGORA_VERIFY(), DRAGORA_VERIFY_MSG()
#define DRAGORA_ASSERT( EXP )

/// Trigger a debug breakpoint with a customized message if the result of an expression is false in non-release builds.
///
/// @param[in] EXP      Expression to evaluate.
/// @param[in] ...      Message to display if the assertion is triggered.
///
/// @see DRAGORA_ASSERT(), DRAGORA_BREAK(), DRAGORA_BREAK_MSG() DRAGORA_VERIFY(), DRAGORA_VERIFY_MSG()
#define DRAGORA_ASSERT_MSG( EXP, ... )

/// Trigger a debug breakpoint if the result of an expression is false in non-release builds while still evaluating the
/// expression in release builds, and evaluating to the result of the expression.
///
/// @param[in] EXP      Expression to evaluate.
///
/// @see DRAGORA_ASSERT(), DRAGORA_ASSERT_MSG(), DRAGORA_BREAK(), DRAGORA_BREAK_MSG(), DRAGORA_VERIFY_MSG()
#define DRAGORA_VERIFY( EXP ) \
DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	( ( EXP ) ? true : false ) \
DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING

/// Trigger a debug breakpoint if the result of an expression is false in non-release builds while still evaluating the
/// expression in release builds, and evaluating to the result of the expression.
///
/// @param[in] EXP      Expression to evaluate.
/// @param[in] ...      Message to display if the assertion is triggered.
///
/// @see DRAGORA_ASSERT(), DRAGORA_ASSERT_MSG(), DRAGORA_BREAK(), DRAGORA_BREAK_MSG(), DRAGORA_VERIFY()
#define DRAGORA_VERIFY_MSG( EXP, ... ) \
DRAGORA_CC_PUSH_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING \
	( ( EXP ) ? true : false ) \
DRAGORA_CC_POP_SILENCE_UNUSED_EXPRESSION_RESULT_WARNING

#endif

//
// Compile time
//

#if DRAGORA_CC_CL
# if _MSC_VER >= 1600
#  define DRAGORA_COMPILE_ASSERT(exp) static_assert(exp, #exp)
# endif
#endif

#ifndef DRAGORA_COMPILE_ASSERT
# define DRAGORA_COMPILE_ASSERT_JOIN(X, Y) DRAGORA_COMPILE_ASSERT_DO_JOIN(X, Y)
# define DRAGORA_COMPILE_ASSERT_DO_JOIN(X, Y) DRAGORA_COMPILE_ASSERT_DO_JOIN2(X, Y)
# define DRAGORA_COMPILE_ASSERT_DO_JOIN2(X, Y) X##Y
# define DRAGORA_COMPILE_ASSERT(exp) typedef char DRAGORA_COMPILE_ASSERT_JOIN(__DRAGORA_COMPILE_ASSERT__,__LINE__)[(exp)?1:-1]
#endif
