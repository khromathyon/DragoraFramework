#pragma once

#include "Platform/API.h"
#include "Platform/Exception.h"
#include "Platform/Locks.h"
#include "Platform/Utility.h"

#include <memory>
#include <vector>
#include <limits>


//TO XLE: WHY YOU NEED SO. MUCH. SHIT.???.

namespace Dragora
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    template <typename Marker>
    class /*DRAGORA_PLATFORM_API*/ MarkerHeap
    {
    public:
        static Marker      ToInternalSize(unsigned size);
        static unsigned    ToExternalSize(Marker size);
        static unsigned    AlignSize(unsigned size);
    };

    class  DefragStep
    {
    public:
        unsigned _sourceStart, _sourceEnd;
        unsigned _destination;
    };

    template <typename Marker>
    class  SpanningHeap : public MarkerHeap<Marker>
    {
    public:

        //
        //      It's a simple heap that deals only in spans. It doesn't record the
        //      size of the blocks allocated from within it. It just knows what space
        //      is allocated, and what is unallocated. The client must deallocate the
        //      correct space from the buffer when it's done.
        //

        unsigned            Allocate(unsigned size);
        bool                Allocate(unsigned ptr, unsigned size);
        bool                Deallocate(unsigned ptr, unsigned size);

        unsigned            CalculateAvailableSpace() const;
        unsigned            CalculateLargestFreeBlock() const;
        unsigned            CalculateAllocatedSpace() const;
        unsigned            CalculateHeapSize() const;
        uint64_t              CalculateHash() const;
        bool                IsEmpty() const;

        unsigned            AppendNewBlock(unsigned size);

        std::vector<unsigned>       CalculateMetrics() const;
        std::vector<DefragStep>     CalculateDefragSteps() const;
        void                        PerformDefrag(const std::vector<DefragStep>& defrag);

        std::pair<std::unique_ptr<uint8_t[]>, size_t> Flatten() const;

        SpanningHeap();
        SpanningHeap(unsigned size);
        SpanningHeap(const uint8_t flattened[], size_t flattenedSize);
        ~SpanningHeap();

        SpanningHeap(SpanningHeap&& moveFrom) never_throws;
        const SpanningHeap& operator=(SpanningHeap&& moveFrom) never_throws;
        SpanningHeap(const SpanningHeap& cloneFrom);
        const SpanningHeap& operator=(const SpanningHeap& cloneFrom);
    protected:
        std::vector<Marker>         _markers;
        mutable Mutex               _lock;
        mutable bool                _largestFreeBlockValid;
        mutable Marker              _largestFreeBlock;

        Marker      CalculateLargestFreeBlock_Internal() const;
        bool        BlockAdjust_Internal(unsigned ptr, unsigned size, bool allocateOperation);
    };

    typedef SpanningHeap<uint16_t> SimpleSpanningHeap;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    template <typename Marker>
    auto MarkerHeap<Marker>::ToInternalSize(unsigned size) -> Marker
    {
        DRAGORA_ASSERT((size>>4) <= std::numeric_limits<Marker>::max());
        return Marker(size>>4);
    }

    template <typename Marker>
    unsigned MarkerHeap<Marker>::ToExternalSize(Marker size)
    {
        DRAGORA_ASSERT(size <= (std::numeric_limits<unsigned>::max()>>4));
        return unsigned(size)<<4;
    }

    template <typename Marker>
    unsigned MarkerHeap<Marker>::AlignSize(unsigned size)
    {
        DRAGORA_ASSERT((size>>4) <= std::numeric_limits<Marker>::max());
        return (size&(~((1<<4)-1)))+((size&((1<<4)-1))?(1<<4):0);
    }
}

#include "Platform/HeapUtils.inl"