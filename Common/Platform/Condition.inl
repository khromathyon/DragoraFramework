/// Get the platform-specific condition handle.
///
/// @return  Reference to the condition handle.
const Dragora::Condition::Handle& Dragora::Condition::GetHandle() const
{
    return m_Handle;
}
