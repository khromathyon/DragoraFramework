#include "PlatformPch.h"
#include "Console.h"

#include "Platform/Assert.h"

#include <stdlib.h>

using namespace Dragora;

struct ColorEntry
{
	int m_Key;
	const char* m_Value;
};

ColorEntry g_ColorTable[] =
{
	{ ConsoleColors::None,   "\033[0m" },
	{ ConsoleColors::Red, 	 "\033[1;31m" },
	{ ConsoleColors::Green,  "\033[1;32m" },
	{ ConsoleColors::Blue, 	 "\033[1;34m" },
	{ ConsoleColors::Yellow, "\033[1;33m" },
	{ ConsoleColors::Aqua,   "\033[1;36m" },
	{ ConsoleColors::Purple, "\033[1;35m" },
	{ ConsoleColors::White,  "\033[1;37m" }
};

static inline const char* LookupColor( ConsoleColor color )
{
	for ( int i=0; i<sizeof(g_ColorTable)/sizeof(g_ColorTable[0]); i++ )
	{
		if ( g_ColorTable[i].m_Key == color )
		{
			return g_ColorTable[i].m_Value;
		}
	}

	DRAGORA_ASSERT( false );
	return "\033[0m";
}

int Dragora::Scan(const char* fmt, ...)
{
	char buf[1024];
	if ( fgets( buf, sizeof( buf ), stdin ) )
	{
		va_list args;
		va_start(args, fmt);
		int result = vsscanf( buf, fmt, args );
		va_end(args);
		return result;
	}
	else
	{
		return 0;
	}
}

int Dragora::Scan(const wchar_t* fmt, ...)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::ScanArgs(const char* fmt, va_list args)
{
	char buf[1024];
	if ( fgets( buf, sizeof( buf ), stdin ) )
	{
		return vsscanf( buf, fmt, args );
	}
	else
	{
		return 0;
	}
}

int Dragora::ScanArgs(const wchar_t* fmt, va_list args)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::FileScan(FILE* f, const char* fmt, ...)
{
	char buf[1024];
	if ( fgets( buf, sizeof( buf ), f ) )
	{
		va_list args;
		va_start(args, fmt);
		int result = vsscanf( buf, fmt, args );
		va_end(args);
		return result;
	}
	else
	{
		return 0;
	}
}

int Dragora::FileScan(FILE* f, const wchar_t* fmt, ...)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::FileScanArgs(FILE* f, const char* fmt, va_list args)
{
	char buf[1024];
	if ( fgets( buf, sizeof( buf ), f ) )
	{
		return vsscanf( buf, fmt, args );
	}
	else
	{
		return 0;
	}
}

int Dragora::FileScanArgs(FILE* f, const wchar_t* fmt, va_list args)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::StringScan(const char* str, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vsscanf(str, fmt, args);
	va_end(args);
	return result;
}

int Dragora::StringScan(const wchar_t* str, const wchar_t* fmt, ...)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::StringScanArgs(const char* str, const char* fmt, va_list args)
{
	return vsscanf(str, fmt, args);
}

int Dragora::StringScanArgs(const wchar_t* str, const wchar_t* fmt, va_list args)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::Print(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vprintf(fmt, args);
	va_end(args);
	return result;
}

int Dragora::Print(const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vwprintf(fmt, args);
	va_end(args);
	return result;
}

int Dragora::PrintArgs(const char* fmt, va_list args)
{
	return vprintf(fmt, args);
}

int Dragora::PrintArgs(const wchar_t* fmt, va_list args)
{
	return vwprintf(fmt, args);
}

int Dragora::FilePrint(FILE* f, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vfprintf(f, fmt, args);
	va_end(args);
	return result;
}

int Dragora::FilePrint(FILE* f, const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vfwprintf(f, fmt, args);
	va_end(args);
	return result;
}

int Dragora::FilePrintArgs(FILE* f, const char* fmt, va_list args)
{
	return vfprintf(f, fmt, args);
}

int Dragora::FilePrintArgs(FILE* f, const wchar_t* fmt, va_list args)
{
	return vfwprintf(f, fmt, args);
}

int Dragora::StringPrint(char* dest, size_t destCount, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vsnprintf(dest, destCount, fmt, args);
	va_end(args);
	return result;
}

int Dragora::StringPrint(wchar_t* dest, size_t destCount, const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vswprintf(dest, destCount, fmt, args);
	va_end(args);
	return result;
}

int Dragora::StringPrintArgs(char* dest, size_t destCount, const char* fmt, va_list args)
{
	return vsnprintf(dest, destCount, fmt, args);
}

int Dragora::StringPrintArgs(wchar_t* dest, size_t destCount, const wchar_t* fmt, va_list args)
{
	return vswprintf(dest, destCount, fmt, args);
}

template< class T >
int PrintArgs(ConsoleColor color, FILE* stream, const T* fmt, va_list args)
{
	if (color != ConsoleColors::None)
	{
		FilePrint( stream, LookupColor( color ) );
	}

	int result = FilePrint(stream, fmt, args);

	if (color != ConsoleColors::None)
	{
		DRAGORA_ASSERT( g_ColorTable[0].m_Key == ConsoleColors::None );
		FilePrint( stream, g_ColorTable[0].m_Value );
	}

	fflush(stream);

	return result;
}

template< class T >
int PrintString(ConsoleColor color, FILE* stream, const std::basic_string< T >& tstring)
{
	if (color != ConsoleColors::None)
	{
		FilePrint( stream, LookupColor( color ) );
	}

	int result = FilePrint(stream, TXT("%s"), tstring.c_str());

	if (color != ConsoleColors::None)
	{
		DRAGORA_ASSERT( g_ColorTable[0].m_Key == ConsoleColors::None );
		FilePrint( stream, g_ColorTable[0].m_Value );
	}

	fflush(stream);

	return result;
}

int Dragora::Print(ConsoleColor color, FILE* stream, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = ::PrintArgs<char>( color, stream, fmt, args );
	va_end(args);
	return result;
}

int Dragora::Print(ConsoleColor color, FILE* stream, const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = ::PrintArgs<wchar_t>( color, stream, fmt, args );
	va_end(args);
	return result;
}

int Dragora::PrintArgs(ConsoleColor color, FILE* stream, const char* fmt, va_list args)
{
	return ::PrintArgs<char>( color, stream, fmt, args );
}

int Dragora::PrintArgs(ConsoleColor color, FILE* stream, const wchar_t* fmt, va_list args)
{
	return ::PrintArgs<wchar_t>( color, stream, fmt, args );
}

int Dragora::PrintString(ConsoleColor color, FILE* stream, const std::string& string)
{
	return ::PrintString<char>( color, stream, string );
}

int Dragora::PrintString(ConsoleColor color, FILE* stream, const std::wstring& string)
{
	return ::PrintString<wchar_t>( color, stream, string );
}