#pragma once

#include "Platform/API.h"
#include "Platform/Assert.h"
#include "Platform/Locks.h"

/// @defgroup tracemacros Trace Macros
//@{

#if !defined( DRAGORA_ENABLE_TRACE ) && !( DRAGORA_RELEASE || DRAGORA_PROFILE )
/// Defined and non-zero if tracing is enabled.
#define DRAGORA_ENABLE_TRACE 1
#endif

#if defined( DRAGORA_ENABLE_TRACE ) && !DRAGORA_ENABLE_TRACE
#undef DRAGORA_ENABLE_TRACE
#endif

#if DRAGORA_ENABLE_TRACE

namespace Dragora
{
	/// Trace levels.  These are intentionally declared outside the scope of the Trace class and abbreviated for ease of
	/// use.
	namespace TraceLevels
	{
		enum Type
		{
			Debug,    ///< Debug logging messages.
			Info,     ///< General info messages.
			Warning,  ///< Warning messages.
			Error,    ///< Critical error messages.
		};
	}
	typedef TraceLevels::Type TraceLevel;

	/// Trace interface.
	class DRAGORA_PLATFORM_API Trace
	{
	public:
		/// Default size for formatted trace message buffers without requiring dynamic memory allocation.
		static const size_t DEFAULT_MESSAGE_BUFFER_SIZE = 1024;

		/// @name Logging Interface
		//@{
		static void SetLevel( TraceLevel level );
		static inline TraceLevel GetLevel();

		static void Output( TraceLevel level, const char* pFormat, ... );
		static void OutputVa( TraceLevel level, const char* pFormat, va_list argList );
		//@}

	protected:
		/// Current logging level.
		static TraceLevel sm_level;

		/// Logging level of the last message.
		static TraceLevel sm_lastMessageLevel;

		/// True if logging just started a fresh line.
		static bool sm_bNewLine;

		/// @name Logging Implementation
		//@{
		static void OutputImplementation( const char* pMessage );
		//@}

		/// @name Static Utility Functions
		//@{
		static const char* GetLevelString( TraceLevel level );
		//@}
	};
}

/// Write out a message to the trace output.
///
/// @param[in] LEVEL  Trace level.
/// @param[in] ...    Message.  This can be a "printf"-style format string and arguments or a Dragora::String.
#define DRAGORA_TRACE( LEVEL, ... ) { Dragora::Trace::Output( LEVEL, __VA_ARGS__ ); DRAGORA_ASSERT( LEVEL != Dragora::TraceLevels::Error ); }

/// Write out a formatted message to the trace output using a variable argument list.
///
/// @param[in] LEVEL     Trace level.
/// @param[in] FORMAT    Format string.
/// @param[in] ARG_LIST  Variable argument list initialized to the format arguments (va_start() should have already been
///                      called.
#define DRAGORA_TRACE_VA( LEVEL, FORMAT, ARG_LIST ) { Dragora::Trace::OutputVa( LEVEL, FORMAT, ARG_LIST ); DRAGORA_ASSERT( LEVEL != Dragora::TraceLevels::Error ); }

/// Set the current trace level.
///
/// @param[in] LEVEL  Level to set.
///
/// @see DRAGORA_TRACE_GET_LEVEL()
#define DRAGORA_TRACE_SET_LEVEL( LEVEL ) Dragora::Trace::SetLevel( LEVEL )

/// Get the current trace level.
///
/// @return  Current trace level.
///
/// @see DRAGORA_TRACE_SET_LEVEL()
#define DRAGORA_TRACE_GET_LEVEL() Dragora::Trace::GetLevel()

#else  // DRAGORA_ENABLE_TRACE

/// Write out a message to the trace output.
///
/// @param[in] LEVEL  Trace level.
/// @param[in] ...    Message.  This can be a "printf"-style format string and arguments or a Dragora::String.
#define DRAGORA_TRACE( LEVEL, ... )

/// Write out a formatted message to the trace output using a variable argument list.
///
/// @param[in] LEVEL     Trace level.
/// @param[in] FORMAT    Format string.
/// @param[in] ARG_LIST  Variable argument list initialized to the format arguments (va_start() should have already been
///                      called.
#define DRAGORA_TRACE_VA( LEVEL, FORMAT, ARG_LIST )

/// Set the current trace level.
///
/// @param[in] LEVEL  Level to set.
///
/// @see DRAGORA_TRACE_GET_LEVEL()
#define DRAGORA_TRACE_SET_LEVEL( LEVEL )

/// Get the current trace level.
///
/// @return  Current trace level.
///
/// @see DRAGORA_TRACE_SET_LEVEL()
#define DRAGORA_TRACE_GET_LEVEL() ( Dragora::Trace::LEVEL_INVALID )

#endif  // DRAGORA_ENABLE_TRACE

//@}

#include "Platform/Trace.inl"
