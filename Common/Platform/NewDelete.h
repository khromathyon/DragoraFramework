#pragma once

#ifndef DRAGORA_NEW_DELETE_SPEC
#define DRAGORA_NEW_DELETE_SPEC
#endif

#ifndef DRAGORA_HEAP
#error DRAGORA_HEAP not defined!
#endif

#ifndef DRAGORA_NEW_DELETE
#error DRAGORA_NEW_DELETE not defined!
#endif

#if DRAGORA_NEW_DELETE

/// Global "new" operator.
///
/// @param[in] size  Allocation size.
///
/// @return  Base address of the requested allocation.
DRAGORA_NEW_DELETE_SPEC void* operator new( size_t size ) throw( std::bad_alloc )
{
    Dragora::DefaultAllocator allocator;
    void* pMemory = Dragora::AllocateAlignmentHelper( allocator, size );
    DRAGORA_ASSERT( pMemory );
    if( !pMemory )
    {
        throw std::bad_alloc();
    }

    return pMemory;
}

/// Global "new" operator, no-throw variant.
///
/// @param[in] size  Allocation size.
///
/// @return  Base address of the requested allocation if successful, null pointer if not successful.
DRAGORA_NEW_DELETE_SPEC void* operator new( size_t size, const std::nothrow_t& ) throw()
{
    Dragora::DefaultAllocator allocator;
    void* pMemory = Dragora::AllocateAlignmentHelper( allocator, size );
    DRAGORA_ASSERT( pMemory );

    return pMemory;
}

/// Global array "new" operator.
///
/// @param[in] size  Allocation size.
///
/// @return  Base address of the requested allocation if successful.
DRAGORA_NEW_DELETE_SPEC void* operator new[]( size_t size ) throw( std::bad_alloc )
{
    Dragora::DefaultAllocator allocator;
    void* pMemory = Dragora::AllocateAlignmentHelper( allocator, size );
    DRAGORA_ASSERT( pMemory );
    if( !pMemory )
    {
        throw std::bad_alloc();
    }

    return pMemory;
}

/// Global array "new" operator, no-throw variant.
///
/// @param[in] size  Allocation size.
///
/// @return  Base address of the requested allocation if successful, null pointer if not successful.
DRAGORA_NEW_DELETE_SPEC void* operator new[]( size_t size, const std::nothrow_t& ) throw()
{
    Dragora::DefaultAllocator allocator;
    void* pMemory = Dragora::AllocateAlignmentHelper( allocator, size );
    DRAGORA_ASSERT( pMemory );

    return pMemory;
}

/// Global "delete" operator.
///
/// @param[in] pMemory  Base address of the memory to free.
DRAGORA_NEW_DELETE_SPEC void operator delete( void* pMemory ) throw()
{
    Dragora::DefaultAllocator().Free( pMemory );
}

/// Global "delete" operator, no-throw variant.
///
/// @param[in] pMemory  Base address of the memory to free.
DRAGORA_NEW_DELETE_SPEC void operator delete( void* pMemory, const std::nothrow_t& ) throw()
{
    Dragora::DefaultAllocator().Free( pMemory );
}

/// Global array "delete" operator.
///
/// @param[in] pMemory  Base address of the memory to free.
DRAGORA_NEW_DELETE_SPEC void operator delete[]( void* pMemory ) throw()
{
    Dragora::DefaultAllocator().Free( pMemory );
}

/// Global array "delete" operator, no-throw variant.
///
/// @param[in] pMemory  Base address of the memory to free.
DRAGORA_NEW_DELETE_SPEC void operator delete[]( void* pMemory, const std::nothrow_t& ) throw()
{
    Dragora::DefaultAllocator().Free( pMemory );
}

#endif // DRAGORA_NEW_DELETE

// EASTL Required allocators.
void* operator new[](size_t size, const char* pName, int flags, unsigned debugFlags, const char* file, int line) {
    return Dragora::DefaultAllocator().Allocate(size);
}
void* operator new[](size_t size, size_t alignment, size_t alignmentOffset, const char* pName, int flags, unsigned debugFlags, const char* file, int line) {
    return Dragora::DefaultAllocator().AllocateAligned(alignment, size);
}
