#if DRAGORA_ENABLE_TRACE

/// Get the current logging level.
///
/// @return  Current logging level.
///
/// @see SetLevel()
Dragora::TraceLevel Dragora::Trace::GetLevel()
{
    return sm_level;
}

#endif  // DRAGORA_ENABLE_TRACE
