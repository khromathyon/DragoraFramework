#pragma once


#include <memory>
#include <new>

#if DRAGORA_OS_MAC
# include <malloc/malloc.h>
#else
# include <malloc.h>
#include <limits>

#endif

#include "Platform/Types.h"
#include "Platform/Utility.h"
#ifdef DRAGORA_MEMORYALLOCATOR_LTMALLOC
#include "ltalloc/ltalloc.h"
#endif

/// @defgroup defaultheapmacro Default Module Memory Heap Declaration
//@{

#ifndef DRAGORA_HEAP
/// Enable internal heap functionality
#define DRAGORA_HEAP 0
#endif

#ifndef DRAGORA_NEW_DELETE
/// Take control of heap allocations, as invasively as possible.
#define DRAGORA_NEW_DELETE 0
#endif

#if DRAGORA_HEAP

#ifndef DRAGORA_USE_MODULE_HEAPS
/// Non-zero if module-specific heaps should be enabled.
#define DRAGORA_USE_MODULE_HEAPS 1
#endif

#ifndef DRAGORA_USE_EXTERNAL_HEAP
/// Non-zero if a separate heap should be used for external libraries.
#define DRAGORA_USE_EXTERNAL_HEAP 1
#endif

/// Define the parameter list for a dynamic memory heap, associating it with the given name in non-release builds.
///
/// @param[in] NAME_STRING  Name string to assign to the heap.
#if DRAGORA_RELEASE || DRAGORA_PROFILE
#define DRAGORA_DYNAMIC_MEMORY_HEAP_INIT( NAME_STRING )
#else
#define DRAGORA_DYNAMIC_MEMORY_HEAP_INIT( NAME_STRING ) ( NAME_STRING )
#endif

/// Define the parameter list for a fixed-capacity dynamic memory heap, associating it with the given name in
/// non-release builds.
///
/// @param[in] NAME_STRING  Name string to assign to the heap.
/// @param[in] CAPACITY     Heap capacity, in bytes.
#if DRAGORA_RELEASE || DRAGORA_PROFILE
#define DRAGORA_DYNAMIC_MEMORY_HEAP_CAP_INIT( NAME_STRING, CAPACITY ) ( CAPACITY )
#else
#define DRAGORA_DYNAMIC_MEMORY_HEAP_CAP_INIT( NAME_STRING, CAPACITY ) ( NAME_STRING, CAPACITY )
#endif

/// Build the name of the module heap function from preprocessor defines (for THIS module)
#ifdef DRAGORA_MODULE
#define DRAGORA_MAKE_MODULE_HEAP_FUNCTION_TOKEN_PASTE(PREFIX, MODULE, SUFFIX) PREFIX ## MODULE ## SUFFIX
#define DRAGORA_MAKE_MODULE_HEAP_FUNCTION(PREFIX, MODULE, SUFFIX) DRAGORA_MAKE_MODULE_HEAP_FUNCTION_TOKEN_PASTE(PREFIX, MODULE, SUFFIX)
#define DRAGORA_MODULE_HEAP_FUNCTION DRAGORA_MAKE_MODULE_HEAP_FUNCTION( Get, DRAGORA_MODULE, DefaultHeap )
#else
#error DRAGORA_MODULE not defined, please define this macro to be the bare (non-stringified) name of the current compiling module
#endif

#if DRAGORA_SHARED
# define DRAGORA_MODULE_HEAP_FUNCTION_SPEC DRAGORA_API_EXPORT
#else
# define DRAGORA_MODULE_HEAP_FUNCTION_SPEC
#endif

/// Define the default memory heap for the current module.
///
/// This must be called in the source file of a given module in order to set up the default heap to use for that module.
///
/// @param[in] MODULE_NAME  Name of the module.  This will be associated with the module's heap when debugging or
///                         viewing memory stats.
#define DRAGORA_DEFINE_DEFAULT_MODULE_HEAP( MODULE_NAME ) \
	namespace Dragora \
	{ \
		DRAGORA_MODULE_HEAP_FUNCTION_SPEC DynamicMemoryHeap& DRAGORA_MODULE_HEAP_FUNCTION() \
		{ \
			static DynamicMemoryHeap* pModuleHeap = NULL; \
			if( !pModuleHeap ) \
			{ \
				pModuleHeap = static_cast< DynamicMemoryHeap* >( \
					VirtualMemory::Allocate( sizeof( DynamicMemoryHeap ) ) ); \
				new( pModuleHeap ) DynamicMemoryHeap DRAGORA_DYNAMIC_MEMORY_HEAP_INIT( TXT( #MODULE_NAME ) ); \
			} \
			\
			return *pModuleHeap; \
		} \
	}

#if DRAGORA_USE_MODULE_HEAPS
	/// Default memory heap.  This exposes the memory heap for any module in which this file is included.
	#define DRAGORA_DEFAULT_HEAP Dragora::DRAGORA_MODULE_HEAP_FUNCTION()
#else
	/// Default memory heap.
	#define DRAGORA_DEFAULT_HEAP Dragora::GetDefaultHeap()
#endif

#if DRAGORA_USE_EXTERNAL_HEAP
	/// Default dynamic memory heap for allocations from external libraries.
	#define DRAGORA_EXTERNAL_HEAP Dragora::GetExternalHeap()
#else
	/// Default dynamic memory heap for allocations from external libraries.
	#define DRAGORA_EXTERNAL_HEAP DRAGORA_DEFAULT_HEAP
#endif

#endif // DRAGORA_HEAP

//@}

/// @defgroup newdeletemacros "new"/"delete" Utility Macros
//@{

/// Create a new object using a specific heap or allocator.
///
/// @param[in] ALLOCATOR  Reference to a Dragora::MemoryHeap or allocator from which to allocate memory.
/// @param[in] TYPE       Type of object to create.
/// @param[in] ...        Optional parameter list.
///
/// @return  Pointer to the newly created object, if successful.
///
/// @see DRAGORA_DELETE()
#define DRAGORA_NEW( ALLOCATOR, TYPE, ... ) Dragora::NewHelper< TYPE >( ALLOCATOR )

/// Create a new array of objects using a specific heap or allocator.
///
/// @param[in] ALLOCATOR  Reference to a Dragora::MemoryHeap or allocator from which to allocate memory.
/// @param[in] TYPE       Type of object to create.
/// @param[in] COUNT      Number of elements to create.
///
/// @return  Pointer to the newly created array, if successful.
///
/// @see DRAGORA_DELETE_A()
#define DRAGORA_NEW_A( ALLOCATOR, TYPE, COUNT ) Dragora::NewArrayHelper< TYPE >( ALLOCATOR, COUNT )

/// Destroy a single object allocated from a specific heap or allocator.
///
/// @param[in] ALLOCATOR  Reference to a Dragora::MemoryHeap or allocator from which the object memory was allocated.
/// @param[in] OBJ        Pointer to the object to destroy.
///
/// @see DRAGORA_NEW()
#define DRAGORA_DELETE( ALLOCATOR, OBJ ) Dragora::DeleteHelper( ALLOCATOR, OBJ )

/// Destroy an array of objects allocated from a specific heap or allocator.
///
/// @param[in] ALLOCATOR  Reference to a Dragora::MemoryHeap or allocator from which the object memory was allocated.
/// @param[in] OBJ        Pointer to the array to destroy.
///
/// @see DRAGORA_NEW_A()
#define DRAGORA_DELETE_A( ALLOCATOR, OBJ ) Dragora::DeleteArrayHelper( ALLOCATOR, OBJ )

//@}

/// @defgroup memdebug Memory Debug Settings
//@{

#if DRAGORA_HEAP

#ifndef DRAGORA_ENABLE_MEMORY_TRACKING
/// Non-zero if general memory tracking should be enabled.
# define DRAGORA_ENABLE_MEMORY_TRACKING ( !DRAGORA_RELEASE )
#endif

#ifndef DRAGORA_ENABLE_MEMORY_TRACKING_VERBOSE
# if DRAGORA_ENABLE_MEMORY_TRACKING && DRAGORA_DEBUG
/// Non-zero if detailed allocation tracking should be enabled.
#  define DRAGORA_ENABLE_MEMORY_TRACKING_VERBOSE ( 0 )
# endif
#endif

#endif // DRAGORA_HEAP

//@}

/// Size of SIMD vectors, in bytes.
#define DRAGORA_SIMD_SIZE ( 16 )
/// Alignment of SIMD vectors, in bytes.
#define DRAGORA_SIMD_ALIGNMENT ( 16 )

/// Prefix macro for declaring SIMD type or variable.
#define DRAGORA_SIMD_ALIGN_PRE DRAGORA_ALIGN_PRE( 16 )
/// Suffix macro for declaring SIMD type or variable alignment.
#define DRAGORA_SIMD_ALIGN_POST DRAGORA_ALIGN_POST( 16 )

namespace Dragora
{
	class ReadWriteLock;
	class ThreadLocalPointer;

#if DRAGORA_HEAP

#if DRAGORA_ENABLE_MEMORY_TRACKING_VERBOSE
	struct DynamicMemoryHeapVerboseTrackingData;
#endif

	/// Low-level memory allocation interface.  This is used to allocate pages of memory in the application's address
	/// space (physical memory if possible).  Typically, most application will not use this directly, but will instead
	/// allocate using one of the provided heap allocators, which provide better management for most runtime
	/// allocations.
	class DRAGORA_PLATFORM_API VirtualMemory
	{
	public:
		/// @name Memory Allocation
		//@{
		static void* Allocate( size_t size );
		static bool Free( void* pMemory, size_t size );
		//@}

		/// @name Memory Information
		//@{
		static size_t GetPageSize();
		//@}

#if DRAGORA_ENABLE_MEMORY_TRACKING
	private:
		/// Number of bytes of physical memory currently allocated.
		static volatile size_t sm_bytesAllocated;
#endif
	};

#endif // DRAGORA_HEAP

	/// Memory heap base class.
	///
	/// This provides the basic interface for custom heap allocators.  The results of each function should behave
	/// similar to their C standard library counterparts, specifically:
	/// - Allocate() should return a null pointer if an allocation is unsuccessful.
	/// - Reallocate() should behave just like Allocate() if the initial memory address provided is null.
	/// - Reallocate() should behave just as if Free() was called on the memory address provided if the size is null.
	/// - The alignment parameter of AllocateAligned() should be expected to be a power of two.
	/// - Free() should run with no ill effects if a null pointer is provided for the memory address to free.
	class DRAGORA_PLATFORM_API MemoryHeap : NonCopyable
	{
	public:
		/// @name Allocation Interface
		//@{
		virtual void* Allocate( size_t size ) = 0;
		virtual void* Reallocate( void* pMemory, size_t size ) = 0;
		virtual void* AllocateAligned( size_t alignment, size_t size ) = 0;
		virtual void Free( void* pMemory ) = 0;
		virtual void FreeAligned( void* pMemory ) = 0;
		virtual size_t GetMemorySize( void* pMemory ) = 0;
		//@}
	};

#if DRAGORA_HEAP

	/// Thread-safe dynamic memory heap.
	///
	/// This provides a thread-safe, dynamic memory heap for general purpose allocations.  Pages of memory are allocated
	/// from the system using VirtualMemory::Allocate() and VirtualMemory::Free(), and allocations within these blocks
	/// are managed internally using nedmalloc (http://www.nedprod.com/programs/portable/nedmalloc/) to provide
	/// efficient scalability across multiple threads.
	class DRAGORA_PLATFORM_API DynamicMemoryHeap : public MemoryHeap
	{
	public:
#if DRAGORA_ENABLE_MEMORY_TRACKING_VERBOSE
		/// Maximum number of allocations to include in an allocation backtrace.
		static const size_t BACKTRACE_DEPTH_MAX = 32;

		/// Memory allocation backtrace data.
		struct AllocationBacktrace 
		{
			/// Program counter addresses.
			void* pAddresses[ BACKTRACE_DEPTH_MAX ];
		};
#endif

		/// @name Construction/Destruction
		//@{
		DynamicMemoryHeap( size_t capacity = 0 );
#if !DRAGORA_RELEASE && !DRAGORA_PROFILE
		DynamicMemoryHeap( const char* pName, size_t capacity = 0 );
#endif
		virtual ~DynamicMemoryHeap();
		//@}

		/// @name Allocation Interface
		//@{
		virtual void* Allocate( size_t size );
		virtual void* Reallocate( void* pMemory, size_t size );
		virtual void* AllocateAligned( size_t alignment, size_t size );
		virtual void Free( void* pMemory );
		virtual void FreeAligned( void* pMemory );
		virtual size_t GetMemorySize( void* pMemory );
		//@}

		/// @name Global Heap List Iteration
		//@{
		inline DynamicMemoryHeap* GetPreviousHeap() const;
		inline DynamicMemoryHeap* GetNextHeap() const;
		//@}

		/// @name Debugging
		//@{
#if !DRAGORA_RELEASE && !DRAGORA_PROFILE
		inline const char* GetName() const;
#endif

#if DRAGORA_ENABLE_MEMORY_TRACKING
		inline size_t GetAllocationCount() const;
		inline size_t GetBytesActual() const;
#endif
		//@}

		/// @name Global Heap List Access
		//@{
		static DynamicMemoryHeap* LockReadGlobalHeapList();
		static void UnlockReadGlobalHeapList();

		static void UnregisterCurrentThreadCache();
		//@}

#if DRAGORA_ENABLE_MEMORY_TRACKING
		/// @name Memory Status Support
		//@{
		static void LogMemoryStats();
		//@}
#endif

	private:
		/// mspace instance.
		void* m_pMspace;
#if !DRAGORA_RELEASE && !DRAGORA_PROFILE
		/// Heap name (for debugging).
		const char* m_pName;
#endif

		/// Previous dynamic memory heap in the global list.
		DynamicMemoryHeap* volatile m_pPreviousHeap;
		/// Next dynamic memory heap in the global list.
		DynamicMemoryHeap* volatile m_pNextHeap;

#if DRAGORA_ENABLE_MEMORY_TRACKING
		/// Number of allocations.
		volatile size_t m_allocationCount;
		/// Total number of bytes actually allocated.
		volatile size_t m_bytesActual;
#endif

#if DRAGORA_ENABLE_MEMORY_TRACKING_VERBOSE
		/// Verbose memory tracking data.
		DynamicMemoryHeapVerboseTrackingData* m_pVerboseTrackingData;
#endif

		/// Head of the global list of dynamic memory heaps.
		static DynamicMemoryHeap* volatile sm_pGlobalHeapListHead;

#if DRAGORA_ENABLE_MEMORY_TRACKING_VERBOSE
		/// True to temporarily disable memory backtrace tracking.
		static volatile bool sm_bDisableBacktraceTracking;
#endif

		/// @name Private Utility Functions
		//@{
		void ConstructNoName( size_t capacity );

#if DRAGORA_ENABLE_MEMORY_TRACKING
		void AddAllocation( void* pMemory );
		void RemoveAllocation( void* pMemory );
#endif
		//@}

		/// @name Private Static Utility Functions
		//@{
		static ReadWriteLock& GetGlobalHeapListLock();
#if DRAGORA_ENABLE_MEMORY_TRACKING
		static DynamicMemoryHeap* GetAllocationHeap( void* pMemory );
#endif
		//@}
	};

#endif // DRAGORA_HEAP

	/// Default dynamic memory allocator.
	///
	/// This provides access to a single, global allocator managed using DynamicMemoryHeap.  This allocator is used by
	/// the global "new" and "delete" operators.  In general, it is preferred to use specific heaps to improve
	/// performance and prevent fragmentation, but general allocations will still typically be handled more efficiently
	/// than the system's default allocator.
	class DefaultAllocator
	{
	public:
		/// @name Memory Allocation
		//@{
		DRAGORA_FORCEINLINE void* Allocate( size_t size );
		DRAGORA_FORCEINLINE void* AllocateAligned( size_t alignment, size_t size );

		DRAGORA_FORCEINLINE void* Reallocate( void* pMemory, size_t size );
		DRAGORA_FORCEINLINE void* ReallocateAligned( void* pMemory, size_t alignment, size_t size );

		DRAGORA_FORCEINLINE void Free( void* pMemory );
		DRAGORA_FORCEINLINE void FreeAligned( void* pMemory );

		DRAGORA_FORCEINLINE size_t GetMemorySize( void* pMemory );
		DRAGORA_FORCEINLINE size_t GetMemorySizeAligned( void* pMemory, size_t alignment );
		//@}
	};

	/// Stack-based memory heap.
	///
	/// This provides a simple stack-based memory pool.  Allocations are performed by grabbing the current stack pointer
	/// to use as the allocation base address and incrementing the stack pointer by the size of the requested
	/// allocation.  "Freeing" an allocation simply resets the stack pointer to the allocation base address.  As such,
	/// allocations can only be freed in the reverse of the order in which they were made.  Due to the simplistic nature
	/// of the memory pool management, Reallocate() is not supported.
	///
	/// In addition to the standard MemoryHeap interface, the StackMemoryHeap::Marker class allows for more efficient
	/// releasing of stack memory by allowing the user to mark a stack location to immediately "pop" back to, releasing
	/// all allocations after the Marker was set instantly.
	///
	/// StackMemoryHeap is not thread-safe.  Its primary purpose is to provide a method for performing moderately
	/// efficient allocations, often with a short lifetime.
	///
	/// Note that the base address of all blocks are aligned to DRAGORA_SIMD_ALIGNMENT by default.
	template< typename Allocator = DefaultAllocator >
	class StackMemoryHeap : public MemoryHeap
	{
	public:
		/// Stack marker.
		///
		/// Stack markers are used to track the location of the stack pointer at a given time so that the stack can be
		/// popped directly back to the original location, discarding the contents of all allocations made after the
		/// stack marker was set.  This simplifies freeing stack heap memory allocated over a given block of execution
		/// time by allowing all such allocations to be "freed" instantly.
		class Marker
		{
		public:
			/// @name Construction/Destruction
			//@{
			Marker();
			explicit Marker( StackMemoryHeap& rHeap );
			~Marker();
			//@}

			/// @name Stack Manipulation
			//@{
			void Set( StackMemoryHeap& rHeap );
			void Pop();
			//@}

		private:
			/// Currently tracked memory heap.
			StackMemoryHeap* m_pHeap;
			/// Tracked stack pointer.
			void* m_pStackPointer;

			/// @name Construction/Destruction
			//@{
			Marker( const Marker& );  // Not implemented.
			//@}

			/// @name Overloaded Operators
			//@{
			Marker& operator=( const Marker& );  // Not implemented.
			//@}
		};

		/// @name Construction/Destruction
		//@{
		StackMemoryHeap( size_t blockSize, size_t blockCountMax = Invalid< size_t >(), size_t defaultAlignment = 8 );
		~StackMemoryHeap();
		//@}

		/// @name Allocation Interface
		//@{
		virtual void* Allocate( size_t size );
		virtual void* Reallocate( void* pMemory, size_t size );
		virtual void* AllocateAligned( size_t alignment, size_t size );
		virtual void Free( void* pMemory );
		virtual void FreeAligned( void* pMemory );
		virtual size_t GetMemorySize( void* pMemory );
		//@}

	private:
		/// Allocated memory block.
		struct Block
		{
			/// Block memory buffer.
			void* m_pBuffer;

			/// Previous block.
			Block* m_pPreviousBlock;
			/// Next block.
			Block* m_pNextBlock;
		};

		/// Head block in the stack.
		Block* m_pHeadBlock;
		/// Tail block in the stack.
		Block* m_pTailBlock;

		/// Currently active block in the stack.
		Block* m_pCurrentBlock;
		/// Current stack pointer.
		void* m_pStackPointer;

		/// Size of each block.
		size_t m_blockSize;
		/// Default allocation alignment.
		size_t m_defaultAlignment;
		/// Remaining number of blocks that can be allocated.
		size_t m_remainingBlockCount;

		/// @name Utility Functions
		//@{
		Block* AllocateBlock();
		//@}
	};

	/// Thread-local stack-based allocator.
	///
	/// This provides an interface to a StackMemoryHeap instance specifically for the current thread.  This heap is a
	/// growable heap that can be used for various temporary allocations.  Note that since jobs can be run on any
	/// thread, you should never attempt to leave an allocation around for another job to free.
	class DRAGORA_PLATFORM_API ThreadLocalStackAllocator
	{
	public:
		/// Size of each stack block.
		static const size_t BLOCK_SIZE = 512 * 1024;

		/// @name Construction/Destruction
		//@{
		ThreadLocalStackAllocator();
		//@}

		/// @name Memory Allocation
		//@{
		void* Allocate( size_t size );
		void* AllocateAligned( size_t alignment, size_t size );
		void Free( void* pMemory );
		//@}

		/// @name Static Access
		//@{
		static StackMemoryHeap<>& GetMemoryHeap();
		static void ReleaseMemoryHeap();
		//@}

	private:
		/// Cached reference to the current thread's stack heap.
		StackMemoryHeap<>* m_pHeap;

		/// @name Thread-local Storage Access
		//@{
		static ThreadLocalPointer& GetMemoryHeapTls();
		//@}
	};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#if DRAGORA_HEAP

#if DRAGORA_USE_MODULE_HEAPS
	/// Get the default heap to use for dynamic allocations from this module (not DLL-exported).
	DRAGORA_MODULE_HEAP_FUNCTION_SPEC extern DynamicMemoryHeap& DRAGORA_MODULE_HEAP_FUNCTION();
#else
	/// Get the default heap to use for dynamic allocations.
	DRAGORA_PLATFORM_API DynamicMemoryHeap& GetDefaultHeap();
#endif

#if DRAGORA_USE_EXTERNAL_HEAP
	/// Get the default heap to use for dynamic allocations from external libraries.
	DRAGORA_PLATFORM_API DynamicMemoryHeap& GetExternalHeap();
#endif

#endif // DRAGORA_HEAP

	/// @defgroup newdeletehelper "new"/"delete" Helper Functions
	/// Note: These are only intended to be used internally.  Do not call these functions directly.
	//@{
	template< typename T, typename Allocator > T* NewHelper( Allocator& rAllocator );
	template< typename T, typename Allocator > void DeleteHelper( Allocator& rAllocator, T* pObject );

	template< typename T, typename Allocator > T* NewArrayHelper( Allocator& rAllocator, size_t count );
	template< typename T, typename Allocator > T* NewArrayHelper( Allocator& rAllocator, size_t count, const std::true_type& rHasTrivialDestructor );
	template< typename T, typename Allocator > T* NewArrayHelper( Allocator& rAllocator, size_t count, const std::false_type& rHasTrivialDestructor );

	template< typename T, typename Allocator > void DeleteArrayHelper( Allocator& rAllocator, T* pArray );
	template< typename T, typename Allocator > void DeleteArrayHelper( Allocator& rAllocator, T* pArray, const std::true_type& rHasTrivialDestructor );
	template< typename T, typename Allocator > void DeleteArrayHelper( Allocator& rAllocator, T* pArray, const std::false_type& rHasTrivialDestructor );

	template< typename Allocator > void* AllocateAlignmentHelper( Allocator& rAllocator, size_t size );
	template< typename Allocator > void* ReallocateAlignmentHelper( Allocator& rAllocator, void* pMemory, size_t size );
	template< typename Allocator > void FreeAlignmentHelper( Allocator& rAllocator, void* pMemory, size_t size );
	//@}

    // @defgroup Functions used among the STL/EASTL.
    struct PODAlignMemFree
    {
        inline void operator()(void* p)
        {
            Dragora::DefaultAllocator().FreeAligned(p);
        }
    };

#if defined( _MSC_VER )
#pragma warning(push)
#pragma warning(disable:4702)
#endif

    /// And STL allocator that will suppress deallocation when memory comes from a part of a larger heap block
    /// When we serialize an object in via the block serializer, we can loading it into a single heap block.
    /// However, objects loaded new can have STL containers and objects (like vectors and strings). In this case,
    /// the memory used by the container is a just a part of the larger heap block. When the container is
    /// destroyed, it will attempt to free it's memory. In the of a normal container, the memory is it's own
    /// unique heap block, and the normal deallocation functions can be used. But for our serialized containers,
    /// the memory is not a unique heap block. It is just a internal part of much larger block. In this case, we
    /// must suppress the deallocation.
    /// The BlockSerializerAllocator does exactly that. For vectors and containers that have been serialized in,
    /// we suppress the deallocation step.
    template<typename Type>
    class BlockSerializerAllocator : public std::allocator<Type>
    {
    public:
        typedef size_t size_type;
        typedef ptrdiff_t difference_type;
        typedef Type* pointer;
        typedef const Type* const_pointer;
        typedef Type& reference;
        typedef const Type& const_reference;
        typedef Type value_type;

        pointer  allocate(size_type n, std::allocator<void>::const_pointer ptr= 0)
        {
            if (_fromFixedStorage) {
                DRAGORA_ASSERT(("Cannot allocate from a BlockSerializerAllocator than has been serialized in from a fixed block\n"));
                return nullptr;
            }
            return std::allocator<Type>::allocate(n, ptr);
        }

        void deallocate(pointer p, size_type n)
        {
            if (!_fromFixedStorage) {
                std::allocator<Type>::deallocate(p, n);
            }
        }

        template<class _Other>
        struct rebind
        {
            typedef BlockSerializerAllocator<_Other> other;
        };

        BlockSerializerAllocator()                                                  : _fromFixedStorage(0) {}
        explicit BlockSerializerAllocator(unsigned fromFixedStorage)                : _fromFixedStorage(fromFixedStorage) {}
        BlockSerializerAllocator(const std::allocator<Type>& copyFrom)              : _fromFixedStorage(0) {}
        BlockSerializerAllocator(std::allocator<Type>&& moveFrom)                   : _fromFixedStorage(0) {}
        BlockSerializerAllocator(const BlockSerializerAllocator<Type>& copyFrom)    : _fromFixedStorage(0) {}
        BlockSerializerAllocator(BlockSerializerAllocator<Type>&& moveFrom)         : _fromFixedStorage(moveFrom._fromFixedStorage) {}

        BlockSerializerAllocator& operator=(const BlockSerializerAllocator<Type>& copyFrom) { _fromFixedStorage = 0; return *this; }
        BlockSerializerAllocator& operator=(BlockSerializerAllocator<Type>&& moveFrom)		{ _fromFixedStorage = moveFrom._fromFixedStorage; return *this; }

    private:
        unsigned    _fromFixedStorage;
    };

#if defined( _MSC_VER )
#pragma warning(pop)
#endif


    template<typename Type>
    class BlockSerializerDeleter : public std::default_delete<Type>
    {
    public:
        void operator()(Type *_Ptr) const
        {
            if (!_fromFixedStorage) {
                std::default_delete<Type>::operator()(_Ptr);
            }
        }

        BlockSerializerDeleter()                                                : _fromFixedStorage(0) {}
        explicit BlockSerializerDeleter(unsigned fromFixedStorage)              : _fromFixedStorage(fromFixedStorage) {}
        BlockSerializerDeleter(const std::default_delete<Type>& copyFrom)       : _fromFixedStorage(0) {}
        BlockSerializerDeleter(std::default_delete<Type>&& moveFrom)            : _fromFixedStorage(0) {}
        BlockSerializerDeleter(const BlockSerializerDeleter<Type>& copyFrom)    : _fromFixedStorage(copyFrom._fromFixedStorage) {}
        BlockSerializerDeleter(BlockSerializerDeleter<Type>&& moveFrom)         : _fromFixedStorage(moveFrom._fromFixedStorage) {}
    private:
        unsigned    _fromFixedStorage;
    };

	template<int SizeInBytes, typename CharType = char>
	struct FixedMemoryBuffer : public std::basic_streambuf<CharType>
	{
		uint8_t _buffer[SizeInBytes];
		FixedMemoryBuffer(size_t charSize)
		{
			this->setp((CharType*)_buffer, (CharType*)PtrAdd(_buffer, sizeof(_buffer) - charSize));
			std::fill_n(_buffer, dimof(_buffer), 0);
	}

		const void* begin() const { return this->pbase(); }
		const void* end() const { return this->pptr(); }
	};


}

/// @defgroup memoryheapnew Dragora::MemoryHeap "new" Overrides
//@{
#if DRAGORA_NEW_DELETE
inline void* operator new( size_t size, Dragora::MemoryHeap& rHeap );
#endif // DRAGORA_HEAP
//@}


#include "Platform/MemoryHeap.inl"
