#pragma once

#include "Platform/Error.h"
#include "Platform/Console.h"

#include <vector>
#include <exception>

#if DRAGORA_FEATURE_EXCEPTIONS

#define TRY { try 
#define CATCH(x) catch(x) 
#define RETHROW throw 
#define CATCH_END } 
#define THROW(x) throw(x)
#else 

#define TRY { if (true) 
#define CATCH(x) else if (false) 
#define RETHROW 
#define CATCH_END }
#define THROW(x)

#pragma warning(disable:4127)       // conditional expression is constant (must be disabled when using these macros)

#endif

#ifdef DRAGORA_CC_CL // If is MSVC
#define never_throws throw()
#else // Otherwise is GCC or Clang.
#define never_throws noexcept
#endif

namespace Dragora
{
	//
	// Constants
	//

	const size_t ERROR_STRING_BUF_SIZE = 768; 

	//
	// Exception class
	//  Try to only throw in "error" cases. Examples:
	//   * A file format API trying to open a file that doesn't exist (the client api should check if it exists if it was graceful execution)
	//   * A disc drive or resource is out of space and cannot be written to
	//   * A network port is taken and cannot be bound
	//

	class DRAGORA_PLATFORM_API Exception
	{
	protected:
		Exception();

	public:
		Exception( const char *msgFormat, ... );

		// These accessors are thow that re-throw blocks can amend the exception message
		inline std::string& Get();
		inline const std::string& Get() const;
		inline void Set(const std::string& message);

		// This allow operation with std::exception case statements
		virtual const char* What() const;

	protected:
		void SetMessage( const char* msgFormat, ... );
		void SetMessage( const char* msgFormat, va_list msgArgs );

		mutable std::string m_Message;
	};

	namespace ExceptionTypes
	{
		enum ExceptionType
		{
			CPP,
#if DRAGORA_OS_WIN
			Structured,
#else
			Signal
#endif
		};

		static const char* Strings[] =
		{
			TXT("C++"),
#if DRAGORA_OS_WIN
			TXT("Structured"),
#else
			TXT("Signal"),
#endif
		};
	}
	typedef ExceptionTypes::ExceptionType ExceptionType;

	struct DRAGORA_PLATFORM_API ExceptionArgs
	{
		ExceptionType               m_Type;
		bool                        m_Fatal;
		std::string                 m_Message;
		std::string                 m_Callstack;
		std::vector< std::string >  m_Threads;
		std::string                 m_State;

		// CPP-specific info
		std::string                 m_CPPClass;

#if DRAGORA_OS_WIN
		// SEH-specific info
		uint32_t                    m_SEHCode;
		std::string                 m_SEHClass;
		std::string                 m_SEHControlRegisters;
		std::string                 m_SEHIntegerRegisters;
#endif
		ExceptionArgs( ExceptionType type, bool fatal )
			: m_Type( type )
			, m_Fatal( fatal )
#if DRAGORA_OS_WIN
			, m_SEHCode( -1 )
#endif
		{
		};
	};

	/// @defgroup debugutility Debug Utility Functions
	//@{
	// Detects if a debugger is attached to the process
	DRAGORA_PLATFORM_API bool IsDebuggerPresent();

#if !DRAGORA_RELEASE && !DRAGORA_PROFILE
	DRAGORA_PLATFORM_API bool InitializeSymbols( const std::string& path = TXT("") );
	DRAGORA_PLATFORM_API bool GetSymbolsInitialized();
	DRAGORA_PLATFORM_API size_t GetStackTrace( void** ppStackTraceArray, size_t stackTraceArraySize, size_t skipCount = 1 );
	DRAGORA_PLATFORM_API void GetAddressSymbol( std::string& rSymbol, void* pAddress );
	DRAGORA_PLATFORM_API void DebugLog( const char* pMessage );
#endif
	//@}
}

#ifdef DRAGORA_OS_WIN
# include "Platform/ExceptionWin.h"
#endif

#include "Platform/Exception.inl"