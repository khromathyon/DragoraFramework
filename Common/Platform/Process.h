#pragma once

#include "Platform/API.h"
#include "Platform/Types.h"

#if DRAGORA_OS_WIN
# define DRAGORA_INVALID_PROCESS ( NULL )
# define DRAGORA_INVALID_MODULE ( NULL )
# define DRAGORA_MODULE_EXTENSION "dll"
#else // DRAGORA_OS_WIN
# define DRAGORA_INVALID_PROCESS ( 0 )
# define DRAGORA_INVALID_MODULE ( NULL )
# if DRAGORA_OS_MAC
#  define DRAGORA_MODULE_EXTENSION "dylib"
# elif DRAGORA_OS_LINUX
#  define DRAGORA_MODULE_EXTENSION "so"
# else
#  error Unknown module extension!
# endif
#endif // DRAGORA_OS_WIN

namespace Dragora
{
#if DRAGORA_OS_WIN
	typedef HANDLE  ProcessHandle;
	typedef HMODULE ModuleHandle;
#else
	typedef pid_t   ProcessHandle;
	typedef void*   ModuleHandle;
#endif

	/// Creates a new process with no window or output, use it for running scripts and file association apps
	DRAGORA_PLATFORM_API int Execute( const std::string& command );

	/// Creates a new process and captures its standard out and standard error into the passed string
	DRAGORA_PLATFORM_API int Execute( const std::string& command, std::string& output );

	/// Spawn another process and run it alongside the executing process
	DRAGORA_PLATFORM_API ProcessHandle Spawn( const std::string& command, bool autoKill = false );

	/// Test if the spawned process is running
	DRAGORA_PLATFORM_API bool SpawnRunning( ProcessHandle handle );

	/// Get result of spawned process and release any behinds-the-scenes resources
	///  (always call!, even for fire-and-forget child processes)
	DRAGORA_PLATFORM_API int SpawnResult( ProcessHandle handle );

	/// Forcefully terminate spawn
	DRAGORA_PLATFORM_API void SpawnKill( ProcessHandle handle );

	/// Get the process id
	DRAGORA_PLATFORM_API int GetProcessId( ProcessHandle handle = DRAGORA_INVALID_PROCESS );

	/// Get a unique string for this process
	DRAGORA_PLATFORM_API std::string GetProcessString();

	/// Get the application path for this process
	DRAGORA_PLATFORM_API std::string GetProcessPath();

	/// Get the executable name for this process
	DRAGORA_PLATFORM_API std::string GetProcessName();

	/// Get the current working path
	DRAGORA_PLATFORM_API std::string GetCurrentWorkingPath();

	/// Get username of the current proess
	DRAGORA_PLATFORM_API std::string GetUserName();
	
	/// Get machine name of the current process
	DRAGORA_PLATFORM_API std::string GetMachineName();
	
	/// Location for user preferences on disk
	DRAGORA_PLATFORM_API std::string GetHomeDirectory();

	/// Load a module into the caller's address space
	DRAGORA_PLATFORM_API ModuleHandle LoadModule( const char* modulePath );

	/// Unload a module from the caller's address space
	DRAGORA_PLATFORM_API void UnloadModule( ModuleHandle handle );

	/// Find a function in a loaded module
	DRAGORA_PLATFORM_API void* GetModuleFunction( ModuleHandle handle, const char* functionName );
}
