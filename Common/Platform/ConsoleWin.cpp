#include "PlatformPch.h"
#include "Console.h"

#include "Platform/Assert.h"

//#undef _UNICODE
//#include "Platform/ConsoleWin.vsscanf.impl"
//#define _UNICODE 1

#include <stdlib.h>
#include <io.h>
#include <fcntl.h>

using namespace Dragora;

struct ColorEntry
{
	int m_Key;
	int m_Value;
};

ColorEntry g_ColorTable[] =
{
	{ ConsoleColors::None, 0xffffffff },
	{ ConsoleColors::Red, FOREGROUND_RED },
	{ ConsoleColors::Green, FOREGROUND_GREEN },
	{ ConsoleColors::Blue, FOREGROUND_BLUE },
	{ ConsoleColors::Yellow, FOREGROUND_RED | FOREGROUND_GREEN, },
	{ ConsoleColors::Aqua, FOREGROUND_GREEN | FOREGROUND_BLUE, },
	{ ConsoleColors::Purple, FOREGROUND_BLUE | FOREGROUND_RED, },
	{ ConsoleColors::White, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE, }
};

static inline int LookupColor( ConsoleColor color )
{
	for ( int i=0; i<sizeof(g_ColorTable)/sizeof(g_ColorTable[0]); i++ )
	{
		if ( g_ColorTable[i].m_Key == color )
		{
			return g_ColorTable[i].m_Value;
		}
	}

	DRAGORA_ASSERT( false );
	return ConsoleColors::None;
}

int Dragora::Scan(const char* fmt, ...)
{
	char buf[1024];
	fgets( buf, sizeof( buf ), stdin );

	va_list args;
	va_start(args, fmt);
	int result = vsscanf( buf, (const char*)fmt, args );
	va_end(args);
	return result;
}

int Dragora::Scan(const wchar_t* fmt, ...)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::ScanArgs(const char* fmt, va_list args)
{
	char buf[1024];
	fgets( buf, sizeof( buf ), stdin );
	return vsscanf( buf, (const char*)fmt, args );
}

int Dragora::ScanArgs(const wchar_t* fmt, va_list args)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::FileScan(FILE* f, const char* fmt, ...)
{
	char buf[1024];
	fgets( buf, sizeof( buf ), f );

	va_list args;
	va_start(args, fmt);
	int result = vsscanf( buf, (const char*)fmt, args );
	va_end(args);
	return result;
}

int Dragora::FileScan(FILE* f, const wchar_t* fmt, ...)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::FileScanArgs(FILE* f, const char* fmt, va_list args)
{
	char buf[1024];
	fgets( buf, sizeof( buf ), f );
	return vsscanf( buf, (const char*)fmt, args );
}

int Dragora::FileScanArgs(FILE* f, const wchar_t* fmt, va_list args)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::StringScan(const char* str, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vsscanf(str, (const char*)fmt, args);
	va_end(args);
	return result;
}

int Dragora::StringScan(const wchar_t* str, const wchar_t* fmt, ...)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::StringScanArgs(const char* str, const char* fmt, va_list args)
{
	return vsscanf(str, (const char*)fmt, args);
}

int Dragora::StringScanArgs(const wchar_t* str, const wchar_t* fmt, va_list args)
{
	DRAGORA_ASSERT( false );
	return 0;
}

int Dragora::Print(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vprintf(fmt, args);
	va_end(args);
	return result;
}

int Dragora::Print(const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vwprintf(fmt, args);
	va_end(args);
	return result;
}

int Dragora::PrintArgs(const char* fmt, va_list args)
{
	return vprintf(fmt, args);
}

int Dragora::PrintArgs(const wchar_t* fmt, va_list args)
{
	return vwprintf(fmt, args);
}

int Dragora::FilePrint(FILE* f, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vfprintf(f, fmt, args);
	va_end(args);
	return result;
}

int Dragora::FilePrint(FILE* f, const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vfwprintf(f, fmt, args);
	va_end(args);
	return result;
}

int Dragora::FilePrintArgs(FILE* f, const char* fmt, va_list args)
{
	return vfprintf(f, fmt, args);
}

int Dragora::FilePrintArgs(FILE* f, const wchar_t* fmt, va_list args)
{
	return vfwprintf(f, fmt, args);
}

int Dragora::StringPrint(char* dest, size_t destCount, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = vsnprintf(dest, destCount, fmt, args);
	va_end(args);
	return result;
}

int Dragora::StringPrint(wchar_t* dest, size_t destCount, const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = _vsnwprintf(dest, destCount, fmt, args);
	va_end(args);
	return result;
}

int Dragora::StringPrintArgs(char* dest, size_t destCount, const char* fmt, va_list args)
{
	return vsnprintf(dest, destCount, fmt, args);
}

int Dragora::StringPrintArgs(wchar_t* dest, size_t destCount, const wchar_t* fmt, va_list args)
{
	return _vsnwprintf(dest, destCount, fmt, args);
}

template< class T >
int PrintArgs(ConsoleColor color, FILE* stream, const T* fmt, va_list args)
{
	CONSOLE_SCREEN_BUFFER_INFO info;

	if (color != ConsoleColors::None)
	{
		// retrieve settings
		GetConsoleScreenBufferInfo(GetStdHandle(STD_ERROR_HANDLE), &info);

		// extract background colors
		int background = info.wAttributes & ~(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);

		// reset forground only to our desired color
		SetConsoleTextAttribute(GetStdHandle(STD_ERROR_HANDLE), LookupColor( color ) | FOREGROUND_INTENSITY | background);
	}

	int result = FilePrint(stream, fmt, args);

	fflush(stream);

	if (color != ConsoleColors::None)
	{
		// restore previous settings
		SetConsoleTextAttribute(GetStdHandle(STD_ERROR_HANDLE), info.wAttributes);
	}

	return result;
}

template< class T >
int PrintString(ConsoleColor color, FILE* stream, const std::basic_string< T >& str)
{
	CONSOLE_SCREEN_BUFFER_INFO info;

	if (color != ConsoleColors::None)
	{
		// retrieve settings
		GetConsoleScreenBufferInfo(GetStdHandle(STD_ERROR_HANDLE), &info);

		// extract background colors
		int background = info.wAttributes & ~(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);

		// reset forground only to our desired color
		SetConsoleTextAttribute(GetStdHandle(STD_ERROR_HANDLE), LookupColor( color ) | FOREGROUND_INTENSITY | background);
	}

	int result = FilePrint(stream, TXT("%s"), str.c_str());

	fflush(stream);

	if (color != ConsoleColors::None)
	{
		// restore previous settings
		SetConsoleTextAttribute(GetStdHandle(STD_ERROR_HANDLE), info.wAttributes);
	}

	return result;
}

int Dragora::Print(ConsoleColor color, FILE* stream, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = ::PrintArgs<char>( color, stream, fmt, args );
	va_end(args);
	return result;
}

int Dragora::Print(ConsoleColor color, FILE* stream, const wchar_t* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int result = ::PrintArgs<wchar_t>( color, stream, fmt, args );
	va_end(args);
	return result;
}

int Dragora::PrintArgs(ConsoleColor color, FILE* stream, const char* fmt, va_list args)
{
	return ::PrintArgs<char>( color, stream, fmt, args );
}

int Dragora::PrintArgs(ConsoleColor color, FILE* stream, const wchar_t* fmt, va_list args)
{
	return ::PrintArgs<wchar_t>( color, stream, fmt, args );
}

int Dragora::PrintString(ConsoleColor color, FILE* stream, const std::string& string)
{
	return ::PrintString<char>( color, stream, string );
}

int Dragora::PrintString(ConsoleColor color, FILE* stream, const std::wstring& string)
{
	return ::PrintString<wchar_t>( color, stream, string );
}