std::string& Dragora::Exception::Get()
{
    return m_Message;
}

const std::string& Dragora::Exception::Get() const
{
    return m_Message;
}

void Dragora::Exception::Set(const std::string& message)
{
    m_Message = message;
}
