#pragma once

#if __GNUC__ < 4
#error GCC 4 required.
#endif

#define DRAGORA_CC_GCC 1
#define DRAGORA_CC_GCC_VERSION ( __GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__ )

#if __cplusplus < 201103L
# define DRAGORA_CPP11 0
#else
# define DRAGORA_CPP11 1
#endif

/// Declare a class method as overriding a virtual method of a parent class.
#define DRAGORA_OVERRIDE
/// Declare a class as an abstract base class.
#define DRAGORA_ABSTRACT

/// DLL export API declaration.
#define DRAGORA_API_EXPORT
/// DLL import API declaration.
#define DRAGORA_API_IMPORT

/// Attribute for forcing the compiler to inline a function.
#define DRAGORA_FORCEINLINE inline __attribute__( ( always_inline ) )

/// Attribute for explicitly defining a pointer or reference as not being externally aliased.
#define DRAGORA_RESTRICT __restrict

/// Prefix macro for declaring type or variable alignment.
///
/// @param[in] ALIGNMENT  Byte alignment (must be a power of two).
#define DRAGORA_ALIGN_PRE( ALIGNMENT )

/// Suffix macro for declaring type or variable alignment.
///
/// @param[in] ALIGNMENT  Byte alignment (must be a power of two).
#define DRAGORA_ALIGN_POST( ALIGNMENT ) __attribute__( ( aligned( ALIGNMENT ) ) )

/// Mark variable as actually used (omit unused variable warning)
#define DRAGORA_UNUSED(expr) (void)(expr)
