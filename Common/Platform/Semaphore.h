#pragma once

#include "Platform/API.h"
#include "Platform/Types.h"

#if !DRAGORA_OS_WIN
# include <semaphore.h>
#endif

namespace Dragora
{
	class DRAGORA_PLATFORM_API Semaphore
	{
	public:
#if DRAGORA_OS_WIN
		typedef void* Handle;
#elif DRAGORA_OS_MAC
		typedef sem_t* Handle;
#else
		typedef sem_t Handle;
#endif

	public:
		Semaphore();
		~Semaphore();

		void Increment();
		void Decrement();
		void Reset();

		inline const Handle& GetHandle();

	private:
		Handle m_Handle;
	};
}

#include "Platform/Semaphore.inl"