#include "PlatformPch.h"
#include "Semaphore.h"

#include "Platform/Assert.h"
#include "Platform/Error.h"
#include "Platform/Console.h"

using namespace Dragora;

Semaphore::Semaphore()
{
    m_Handle = ::CreateSemaphore(NULL, 0, 0x7fffffff, NULL);
    if ( m_Handle == NULL )
    {
        Dragora::Print(TXT("Failed to create semaphore (%s)\n"), Dragora::GetErrorString().c_str());
        DRAGORA_BREAK();
    }
}

Semaphore::~Semaphore()
{
    BOOL result = ::CloseHandle(m_Handle);
    if ( result != TRUE )
    {
        Dragora::Print(TXT("Failed to close semaphore (%s)\n"), Dragora::GetErrorString().c_str());
        DRAGORA_BREAK();
    }
}

void Semaphore::Increment()
{
    LONG count = 0;
    BOOL result = ::ReleaseSemaphore(m_Handle, 1, &count);
    if ( result != TRUE )
    {
        Dragora::Print(TXT("Failed to inrement semaphore from %d (%s)\n"), count, Dragora::GetErrorString().c_str());
        DRAGORA_BREAK();
    }
}

void Semaphore::Decrement()
{
    DWORD result = ::WaitForSingleObject(m_Handle, INFINITE);
    if ( result != WAIT_OBJECT_0 )
    {
        Dragora::Print(TXT("Failed to decrement semaphore (%s)\n"), Dragora::GetErrorString().c_str());
        DRAGORA_BREAK();
    }
}

void Semaphore::Reset()
{
    BOOL result = ::CloseHandle(m_Handle);
    if ( result != TRUE )
    {
        Dragora::Print(TXT("Failed to close semaphore (%s)\n"), Dragora::GetErrorString().c_str());
        DRAGORA_BREAK();
    }

    m_Handle = ::CreateSemaphore(NULL, 0, 0x7fffffff, NULL);
    if ( m_Handle == NULL )
    {
        Dragora::Print(TXT("Failed to create semaphore (%s)\n"), Dragora::GetErrorString().c_str());
        DRAGORA_BREAK();
    }
}
