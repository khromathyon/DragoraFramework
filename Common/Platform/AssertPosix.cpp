#include "Platform/Assert.h"

#include <cstdio>

using namespace Dragora;

/// Terminate the application on a fatal error.
///
/// @param[in] exitCode  Error code associated with the exit.
void Dragora::FatalExit( int /*exitCode*/ )
{
    abort();
}

#if DRAGORA_ASSERT_ENABLED

#if DRAGORA_UNICODE
#define PRINTF wprintf
#else
#define PRINTF printf
#endif

/// Handle an assertion.
///
/// @param[in] pMessageText  Assert message text.
bool Assert::TriggerImplementation( const char* pMessageText )
{
    PRINTF( "%s\n", pMessageText );
    return true;
}

#endif  // DRAGORA_ASSERT_ENABLED
