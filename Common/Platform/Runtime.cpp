#include "PlatformPch.h"
#include "Runtime.h"

using namespace Dragora;
using namespace Dragora::Platform;

const char* Types::Strings[] = 
{
	"Unknown",
	"Windows",
	"Posix",
};

DRAGORA_COMPILE_ASSERT( Types::Count == sizeof(Types::Strings) / sizeof(const char*) );

const char* Endiannesses::Strings[] = 
{
	"Little",
	"Big",
};

DRAGORA_COMPILE_ASSERT( Endiannesses::Count == sizeof(Endiannesses::Strings) / sizeof(const char*) );

Platform::Endianness Platform::GetEndianness()
{
#if DRAGORA_ENDIAN_LITTLE
    return Endiannesses::Little;
#else
    return Endiannesses::Big;
#endif
}
