#pragma once 

#include "Platform/Types.h"

#include "Foundation/API.h"

namespace Dragora
{
    DRAGORA_FOUNDATION_API bool WildcardMatch(const char *String1,const char *String2);
}