Dragora::TUID::TUID()
	: m_ID( 0x0 )
{

}

Dragora::TUID::TUID( tuid id )
	: m_ID( id )
{

}

Dragora::TUID::TUID(const TUID &id)
	: m_ID( id.m_ID )
{

}

Dragora::TUID::TUID( const std::string& id )
{
	FromString( id );
}

Dragora::TUID& Dragora::TUID::operator=(const TUID &rhs)
{
	m_ID = rhs.m_ID;
	return *this;
}

bool Dragora::TUID::operator==(const TUID &rhs) const
{
	return m_ID == rhs.m_ID;
}

bool Dragora::TUID::operator==( const tuid& rhs ) const
{
	return m_ID == rhs;
}

bool Dragora::TUID::operator!=(const TUID &rhs) const
{
	return m_ID != rhs.m_ID;
}

bool Dragora::TUID::operator!=( const tuid &rhs ) const
{
	return m_ID != rhs;
}

bool Dragora::TUID::operator<(const TUID &rhs) const
{
	return m_ID < rhs.m_ID;
}

Dragora::TUID::operator tuid() const
{
	return m_ID;
}

void Dragora::TUID::Reset()
{
	m_ID = 0x0;
}

template<>
inline void Dragora::Swizzle< Dragora::TUID >(TUID& val, bool swizzle)
{
	// operator tuid() const will handle the conversion into the other swizzle func
	val = ConvertEndian(val, swizzle);
}