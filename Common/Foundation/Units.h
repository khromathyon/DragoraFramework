#pragma once

#include "Platform/Types.h"

#include "Foundation/API.h"

namespace Dragora
{
    DRAGORA_FOUNDATION_API std::string BytesToString( uint64_t bytes );
}