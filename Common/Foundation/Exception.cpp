#include "FoundationPch.h"
#include "Exception.h"

#include "Platform/Assert.h"
#include "Platform/Encoding.h"
#include "Platform/Console.h"

#include "Foundation/Log.h"

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <sys/stat.h>

using namespace Dragora;

BreakpointSignature::Delegate   Dragora::g_BreakpointOccurred;
ExceptionSignature::Delegate    Dragora::g_ExceptionOccurred;
TerminateSignature::Event       Dragora::g_Terminating;

bool g_EnableExceptionFilter = false;

#if DRAGORA_OS_WIN

static LONG __stdcall ProcessFilteredException(LPEXCEPTION_POINTERS info)
{
	return Dragora::ProcessException(info, true, true);
}

#endif

void Dragora::EnableExceptionFilter(bool enable)
{
#if DRAGORA_OS_WIN

	g_EnableExceptionFilter = enable;

	// handles an exception occuring in the process not handled by a user exception handler
	SetUnhandledExceptionFilter(g_EnableExceptionFilter ? &ProcessFilteredException : NULL);

#endif
}

void Dragora::ProcessException(const Dragora::Exception& exception, bool print, bool fatal)
{
#if DRAGORA_OS_WIN
	SetUnhandledExceptionFilter(NULL);
#endif

	ExceptionArgs args(ExceptionTypes::CPP, fatal);

	const char* cppClass = "Unknown";
#ifdef _CPPRTTI
	try
	{
		cppClass = typeid(exception).name();
	}
	catch (...)
	{
	}
#else
	cppClass = "Unknown, no RTTI";
#endif

	bool converted = Dragora::ConvertString(exception.What(), args.m_Message);
	DRAGORA_ASSERT(converted);

	converted = Dragora::ConvertString(cppClass, args.m_CPPClass);
	DRAGORA_ASSERT(converted);

	args.m_State = Log::GetOutlineState();

	if (print)
	{
		Dragora::Print(Dragora::ConsoleColors::Red, stderr, TXT("An exception has occurred\nType:    C++ Exception\n Class:   %s\n Message: %s\n"), args.m_CPPClass.c_str(), args.m_Message.c_str());
	}

	g_ExceptionOccurred.Invoke(args);

	if (fatal)
	{
		g_Terminating.Raise(TerminateArgs());
		EnableExceptionFilter(false);
	}
	else if (g_EnableExceptionFilter)
	{
#if DRAGORA_OS_WIN
		SetUnhandledExceptionFilter(&ProcessFilteredException);
#endif
	}
}

void Dragora::ProcessException(const std::exception& exception, bool print, bool fatal)
{
#if DRAGORA_OS_WIN
	SetUnhandledExceptionFilter(NULL);
#endif

	ExceptionArgs args(ExceptionTypes::CPP, fatal);

	const char* cppClass = "Unknown";
#ifdef _CPPRTTI
	try
	{
		cppClass = typeid(exception).name();
	}
	catch (...)
	{
	}
#else
	cppClass = "Unknown, no RTTI";
#endif

	bool converted = Dragora::ConvertString(exception.what(), args.m_Message);
	DRAGORA_ASSERT(converted);

	converted = Dragora::ConvertString(cppClass, args.m_CPPClass);
	DRAGORA_ASSERT(converted);

	args.m_State = Log::GetOutlineState();

	if (print)
	{
		Dragora::Print(Dragora::ConsoleColors::Red, stderr, TXT("An exception has occurred\nType:    C++ Exception\n Class:   %s\n Message: %s\n"), args.m_CPPClass.c_str(), args.m_Message.c_str());
	}

	g_ExceptionOccurred.Invoke(args);

	if (fatal)
	{
		g_Terminating.Raise(TerminateArgs());
		EnableExceptionFilter(false);
	}
	else if (g_EnableExceptionFilter)
	{
#if DRAGORA_OS_WIN
		SetUnhandledExceptionFilter(&ProcessFilteredException);
#endif
	}
}

#if DRAGORA_OS_WIN

uint32_t Dragora::ProcessException(LPEXCEPTION_POINTERS info, bool print, bool fatal)
{
	SetUnhandledExceptionFilter(NULL);

	// handle breakpoint exceptions outside the debugger
	if (!::IsDebuggerPresent()
		&& info->ExceptionRecord->ExceptionCode == EXCEPTION_BREAKPOINT
		&& g_BreakpointOccurred.Valid())
	{
		BreakpointArgs args(info, fatal);
		g_BreakpointOccurred.Invoke(args);
		return args.m_Result;
	}
	else
	{
		ExceptionArgs args(ExceptionTypes::Structured, fatal);

		args.m_State = Log::GetOutlineState();

		Dragora::GetExceptionDetails(info, args);

		if (print)
		{
			Dragora::Print(Dragora::ConsoleColors::Red, stderr, TXT("%s"), GetExceptionInfo(info).c_str());
		}

		g_ExceptionOccurred.Invoke(args);

		if (fatal)
		{
			g_Terminating.Raise(TerminateArgs());
			EnableExceptionFilter(false);
		}
	}

	if (g_EnableExceptionFilter)
	{
		SetUnhandledExceptionFilter(&ProcessFilteredException);
	}

	return EXCEPTION_CONTINUE_SEARCH;
}

#endif