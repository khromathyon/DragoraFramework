#pragma once

#include "Platform/Types.h"
#include "Foundation/API.h"
#include "Foundation/String.h"

namespace Dragora
{
    static const uint64_t DefaultSeed64 = 0xE49B0E3F5C27F17Eull;
    static const uint32_t DefaultSeed32 = 0xB0F57EE3;

    /// xxHash operations.
    uint64_t DRAGORA_FOUNDATION_API Hash64(const void* begin, const void* end, uint64_t seed = DefaultSeed64);
	uint64_t DRAGORA_FOUNDATION_API Hash64(StringSection<char> string, uint64_t seed = DefaultSeed64);

    inline uint64_t Hash64(const char str[], uint64_t seed = DefaultSeed64)
    {
        return Hash64(str, StringEnd(str), seed);
    }

    inline uint64_t Hash64(const std::string& str, uint64_t seed = DefaultSeed64)
    {
        return Hash64(&(*str.begin()), &(*str.end()), seed);
    }

    DRAGORA_FOUNDATION_API uint32_t Hash32(const void* begin, const void* end, uint32_t seed = DefaultSeed32);

    inline uint32_t IntegerHash32(uint32_t key)
    {
        // taken from https://gist.github.com/badboy/6267743
        // See also http://burtleburtle.net/bob/hash/integer.html
        key = (key+0x7ed55d16) + (key<<12);
        key = (key^0xc761c23c) ^ (key>>19);
        key = (key+0x165667b1) + (key<<5);
        key = (key+0xd3a2646c) ^ (key<<9);
        key = (key+0xfd7046c5) + (key<<3);
        key = (key^0xb55a4f09) ^ (key>>16);
        return key;
    }

    inline uint64_t IntegerHash64(uint64_t key)
    {
        // taken from https://gist.github.com/badboy/6267743
        key = (~key) + (key << 21); // key = (key << 21) - key - 1;
        key = key ^ (key >> 24);
        key = (key + (key << 3)) + (key << 8); // key * 265
        key = key ^ (key >> 14);
        key = (key + (key << 2)) + (key << 4); // key * 21
        key = key ^ (key >> 28);
        key = key + (key << 31);
        return key;
    }

    /// Compile time hashing.
    template<unsigned S0, unsigned S1 = 0, unsigned S2 = 0, unsigned S3 = 0>
    struct ConstHash64
    {
    public:
        template<unsigned NewValue, uint64_t CumulativeHash>
        struct Calc
        {
            // Here is the hash algorithm --
            //  Since we're dealing 32 bit values, rather than chars, the algorithm
            //  must be slightly difference. Here's something I've just invented.
            //  It might be OK, but no real testing has gone into it.
            //  Note that since we're building a 64 bit hash value, any strings with
            //  8 or fewer characters can be stored in their entirety, anyway
            static const uint64_t Value = (NewValue == 0) ? CumulativeHash : (((CumulativeHash << 21ull) | (CumulativeHash >> 43ull)) ^ uint64_t(NewValue));
        };

        static const uint64_t Seed = 0xE49B0E3F5C27F17Eull;
        static const uint64_t Value = Calc<S3, Calc<S2, Calc<S1, Calc<S0, Seed>::Value>::Value>::Value>::Value;
    };

    inline uint64_t HashCombine(uint64_t high, uint64_t low)
    {
        // This code based on "FarmHash"... which was in-turn
        // inspired by Murmur Hash. See:
        // https://code.google.com/p/farmhash/source/browse/trunk/src/farmhash.h
        // We want to combine two 64 bit hash values to create a new hash value
        // We could just return an xor of the two values. But this might not
        // be perfectly reliable (for example, the xor of 2 equals values is zero,
        // which could result in loss of information sometimes)
        const auto kMul = 0x9ddfea08eb382d69ull;
        auto a = (low ^ high) * kMul;
        a ^= (a >> 47);
        auto b = (high ^ a) * kMul;
        b ^= (b >> 47);
        b *= kMul;
        return b;
    }

    inline uint64_t HashFilename(const char* filename)
    {

        char buffer[260];
        const char *i = &filename[0];
        const char *iend = StringEnd(filename);
        char* b = buffer;
        while (i!=iend && b!=ArrayEnd(buffer)) {
            *b = /*ToLower*/(*i); ++i; ++b;
        }
        return Hash64((const void*)buffer, (const void*)b, DefaultSeed64);
    }

	inline uint64_t HashFilename(const wchar_t* filename)
	{

		wchar_t buffer[260];
		const wchar_t *i = &filename[0];
		const wchar_t *iend = StringEnd(filename);
		wchar_t* b = buffer;
		while (i != iend && b != ArrayEnd(buffer)) {
			*b = /*ToLower*/(*i); ++i; ++b;
		}
		return Hash64((const void*)buffer, (const void*)b, DefaultSeed64);
	}

	inline uint64_t HashFilename(const StringSection<char> filename)
	{
		return Hash64((const void*)filename.begin(), (const void*)filename.end(), DefaultSeed64);
	}

	inline uint64_t HashFilename(const StringSection<wchar_t> filename)
	{
		return Hash64((const void*)filename.begin(), (const void*)filename.end(), DefaultSeed64);
	}


}
