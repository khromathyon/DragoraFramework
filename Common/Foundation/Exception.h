#pragma once

#include <string>
#include <vector>
#include <exception>

#include "Platform/Exception.h"

#include "Foundation/API.h"
#include "Foundation/Event.h"

namespace Dragora
{
    //
    // Filter exceptions via CRT
    //

    // installs unhandled exception filter into the C-runtime (for worker threads)
    DRAGORA_FOUNDATION_API void EnableExceptionFilter(bool enable);

    //
    // Breakpoint Hit
    //

    struct BreakpointArgs
    {
#if DRAGORA_OS_WIN
        BreakpointArgs( LPEXCEPTION_POINTERS info, bool fatal )
            : m_Info( info )
            , m_Fatal( fatal )
            , m_Result( 0 )
        {

        }
#else
        BreakpointArgs( bool fatal )
            : m_Fatal( fatal )
            , m_Result( 0 )
        {

        }
#endif

#if DRAGORA_OS_WIN
        LPEXCEPTION_POINTERS    m_Info;
#endif
        bool                    m_Fatal;
        mutable int             m_Result;
    };
    typedef Dragora::Signature< const BreakpointArgs& > BreakpointSignature;
    extern DRAGORA_FOUNDATION_API BreakpointSignature::Delegate g_BreakpointOccurred;

    //
    // Exception Event (raised when an exception occurs)
    //

    typedef Dragora::Signature< const ExceptionArgs& > ExceptionSignature;
    extern DRAGORA_FOUNDATION_API ExceptionSignature::Delegate g_ExceptionOccurred;

    //
    // Termination Event (raised before process termination after a fatal exception)
    //

    struct DRAGORA_FOUNDATION_API TerminateArgs {};
    typedef Dragora::Signature< const TerminateArgs& > TerminateSignature;
    extern DRAGORA_FOUNDATION_API TerminateSignature::Event g_Terminating;

    //
    // Exception reporting
    //

    // prepare and dispatch a report for a C++ exception
    DRAGORA_FOUNDATION_API void ProcessException( const Dragora::Exception& ex,
        bool print = false,
        bool fatal = false );

    // prepare and dispatch a report for a C++ exception
    DRAGORA_FOUNDATION_API void ProcessException( const std::exception& ex,
        bool print = false,
        bool fatal = false );

#if DRAGORA_OS_WIN
    // prepare and dispatch a report for an SEH exception such as divide by zero, page fault from a invalid memory access, or even breakpoint instructions
    DRAGORA_FOUNDATION_API uint32_t ProcessException( LPEXCEPTION_POINTERS info,
        bool print = false,
        bool fatal = false );
#endif
}
