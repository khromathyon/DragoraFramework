#include "Foundation/HashUtils.h" 
#include "xxhash.h"

using namespace Dragora;


/// xxHash64


uint32_t Hash32(const void* begin, const void* end, uint32_t seed)
{
    const std::vector<uint8_t> data((uint8_t*)begin, (uint8_t*)end); // It's really nasty.
    return XXH32(data.data(), data.size(), seed);

}

uint64_t  Dragora::Hash64(const void * begin, const void * end, uint64_t seed)
{
	//TODO: Really find a solution, to merge two pointers into one.
	const std::vector<uint8_t> data((uint8_t*)begin, (uint8_t*)end);
#if DRAGORA_CPU_X86_64
	return XXH64(data.data(), data.size(), seed);
#else
	return XXH32(data.data(), data.size(), seed);
#endif
}

uint64_t Dragora::Hash64(StringSection<char> string, uint64_t seed)
{
	return Hash64((const void*)string.begin(), (const void*)string.end(), seed);
}
