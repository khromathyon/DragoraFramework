#pragma once

#include "Platform/Types.h"
#include "Platform/MemoryHeap.h"
#include "Platform/Utility.h"
#include "Platform/Console.h"

#include "Foundation/API.h"
#include "Foundation/Math.h"
#include "Foundation/HashFunctions.h"
#include "Foundation/DynamicArray.h"

#include <string>
#include <EASTL/string.h>
#include <stdlib.h>

namespace Dragora
{
	/// TODO: Should make other constructors explicit?
	/// Base string class.
	template< typename CharType, typename Allocator = DefaultAllocator >
	class StringBase
	{
	public:
		/// @name Construction/Destruction
		//@{
		StringBase();
		explicit StringBase( const CharType* pString );
		StringBase( const CharType* pString, size_t size );
		//@}

		/// @name String Operations
		//@{
		size_t GetSize() const;
		bool IsEmpty() const;
		void Resize( size_t size, CharType fill = static_cast< CharType >( ' ' ) );

		size_t GetCapacity() const;
		void Reserve( size_t capacity );
        void Trim();

		const CharType* GetData() const;

		void Clear();

		CharType& GetElement( size_t index );
		const CharType& GetElement( size_t index ) const;

		void Add( CharType character, size_t count = 1 );
		void Add( const CharType* pString, size_t length = 0 );
		void Insert( size_t index, CharType character, size_t count = 1 );
		void Insert( size_t index, const CharType* pString );
		void Remove( size_t index, size_t count = 1 );

		template< typename OtherAllocator > void Substring(
			StringBase< CharType, OtherAllocator >& rOutput, size_t index = 0,
			size_t count = Invalid< size_t >() ) const;
		StringBase Substring( size_t index = 0, size_t count = Invalid< size_t >() ) const;

		CharType& GetFirst();
		const CharType& GetFirst() const;
		CharType& GetLast();
		const CharType& GetLast() const;
		size_t Push( CharType character );
		void Pop();

		void Format( const CharType* pFormatString, ... );
		uint32_t Parse( const CharType* pFormatString, ... );
		//@}

		/// @name Parsing
		//@{
		size_t Find( CharType character, size_t startIndex = 0 ) const;
		size_t FindReverse( CharType character, size_t startIndex = Invalid< size_t >() ) const;

		size_t FindAny(
			const CharType* pCharacters, size_t startIndex = 0, size_t characterCount = Invalid< size_t >() ) const;
		template< typename OtherAllocator > size_t FindAny(
			const StringBase< CharType, OtherAllocator >& rCharacters, size_t startIndex = 0 ) const;

		size_t FindAnyReverse(
			const CharType* pCharacters, size_t startIndex = Invalid< size_t >(),
			size_t characterCount = Invalid< size_t >() ) const;
		template< typename OtherAllocator > size_t FindAnyReverse(
			const StringBase< CharType, OtherAllocator >& rCharacters, size_t startIndex = Invalid< size_t >() ) const;

		size_t FindNone(
			const CharType* pCharacters, size_t startIndex = 0, size_t characterCount = Invalid< size_t >() ) const;
		template< typename OtherAllocator > size_t FindNone(
			const StringBase< CharType, OtherAllocator >& rCharacters, size_t startIndex = 0 ) const;

		size_t FindNoneReverse(
			const CharType* pCharacters, size_t startIndex = Invalid< size_t >(),
			size_t characterCount = Invalid< size_t >() ) const;
		template< typename OtherAllocator > size_t FindNoneReverse(
			const StringBase< CharType, OtherAllocator >& rCharacters, size_t startIndex = Invalid< size_t >() ) const;

		bool Contains( CharType character ) const;
		template< typename OtherAllocator > bool Contains(
			const StringBase< CharType, OtherAllocator >& rString ) const;
		bool Contains( const CharType* pString ) const;

		template< typename OtherAllocator > bool StartsWith(
			const StringBase< CharType, OtherAllocator >& rString ) const;
		bool StartsWith( const CharType* pString ) const;

		template< typename OtherAllocator > bool EndsWith(
			const StringBase< CharType, OtherAllocator >& rString ) const;
		bool EndsWith( const CharType* pString ) const;

		template< typename ArrayType, typename ArrayAllocator > void Split(
			DynamicArray< ArrayType, ArrayAllocator >& rStringResults, CharType separator,
			bool bCombineAdjacentSeparators = false ) const;
		template< typename ArrayType, typename ArrayAllocator > void Split(
			DynamicArray< ArrayType, ArrayAllocator >& rStringResults, const CharType* pSeparators,
			size_t separatorCount = Invalid< size_t >(), bool bCombineAdjacentSeparators = false ) const;
		template< typename ArrayType, typename ArrayAllocator, typename StringAllocator > void Split(
			DynamicArray< ArrayType, ArrayAllocator >& rStringResults,
			const StringBase< CharType, StringAllocator >& rSeparators, bool bCombineAdjacentSeparators = false ) const;
		//@}

		/// @name Overloaded Operators
		//@{
		const CharType* operator*() const;

		CharType& operator[]( ptrdiff_t index );
		const CharType& operator[]( ptrdiff_t index ) const;

		StringBase& operator=( CharType character );
		StringBase& operator=( const CharType* pString );
		template< typename OtherAllocator > StringBase& operator=(
			const StringBase< CharType, OtherAllocator >& rSource );

		bool operator<( const CharType* pString ) const;
		template< typename OtherAllocator > bool operator<(
			const StringBase< CharType, OtherAllocator >& rString ) const;

		bool operator==( const CharType* pString ) const;
		template< typename OtherAllocator > bool operator==(
			const StringBase< CharType, OtherAllocator >& rString ) const;

		bool operator!=( const CharType* pString ) const;
		template< typename OtherAllocator > bool operator!=(
			const StringBase< CharType, OtherAllocator >& rString ) const;
		//@}

	protected:
		/// Allocated string buffer.
		DynamicArray< CharType, Allocator > m_buffer;

		/// @name Protected String Operations
		//@{
		template< typename OtherAllocator > void Add( const StringBase< CharType, OtherAllocator >& rString );
		template< typename OtherAllocator > void Insert(
			size_t index, const StringBase< CharType, OtherAllocator >& rString );
		//@}
	};

	/// 8-bit character string class.
	class DRAGORA_FOUNDATION_API CharString : public StringBase< char, DefaultAllocator >
	{
	public:
		/// @name Construction/Destruction
		//@{
		CharString();
		/* explicit */ CharString( const char* pString );
		CharString( const char* pString, size_t size );
		CharString( const CharString& rSource );
		//@}

        CharString Trimmed();

		/// @name String Operations
		//@{
		void Add( char character, size_t count = 1 );
		void Add( const char* pString, size_t length = 0 );
		void Add( const CharString& rString );

		void Insert( size_t index, char character, size_t count = 1 );
		void Insert( size_t index, const char* pString );
		void Insert( size_t index, const CharString& rString );
		//@}

		/// @name Overloaded Operators
		//@{
		CharString& operator=( char character );
		CharString& operator=( const char* pString );
		CharString& operator=( const CharString& rSource );

		CharString& operator+=( char character );
		CharString& operator+=( const char* pString );
		CharString& operator+=( const CharString& rString );
		
		CharString& operator+(char character);
		CharString& operator+(const char* pString);
		CharString& operator+(const CharString& rString);

		bool operator==( const char* pString ) const;
		bool operator==( const CharString& rString ) const;
		bool operator!=( const char* pString ) const;
		bool operator!=( const CharString& rString ) const;
		//@}

	};

	/// Wide character string class.
	class DRAGORA_FOUNDATION_API WideString : public StringBase< wchar_t, DefaultAllocator >
	{
	public:
		/// @name Construction/Destruction
		//@{
		WideString();
		/*explicit*/ WideString( const wchar_t* pString );
		WideString( const wchar_t* pString, size_t size );
		WideString( const WideString& rSource );
		//@}

		/// @name String Operations
		//@{
		WideString Trimmed();

		void Add( wchar_t character, size_t count = 1 );
		void Add( const wchar_t* pString, size_t length = 0 );
		void Add( const WideString& rString );

		void Insert( size_t index, wchar_t character, size_t count = 1 );
		void Insert( size_t index, const wchar_t* pString );
		void Insert( size_t index, const WideString& rString );
		//@}

		/// @name Overloaded Operators
		//@{
		WideString& operator=( wchar_t character );
		WideString& operator=( const wchar_t* pString );
		WideString& operator=( const WideString& rSource );

		WideString& operator+(wchar_t character);
		WideString& operator+(const wchar_t* pString);
		WideString& operator+(const WideString& rString);


		WideString& operator+=( wchar_t character );
		WideString& operator+=( const wchar_t* pString );
		WideString& operator+=( const WideString& rString );

		bool operator==( const wchar_t* pString ) const;
		bool operator==( const WideString& rString ) const;
		bool operator!=( const wchar_t* pString ) const;
		bool operator!=( const WideString& rString ) const;
		//@}
	};

	/// Default CharString hash.
	template<>
	class DRAGORA_FOUNDATION_API Hash< CharString >
	{
	public:
		size_t operator()( const CharString& rKey ) const;
	};

	/// Default WideString hash.
	template<>
	class DRAGORA_FOUNDATION_API Hash< WideString >
	{
	public:
		size_t operator()( const WideString& rKey ) const;
	};

	/// Default string class.
	typedef CharString String;


	////////////   S T R I N G   S E C T I O N   ////////////

	/// <summary>Pointers to the start and end of a string</summary>
	/// This object points into the interior of another object, identifying
	/// the start and end of a string.
	///
	/// This is a 3rd common string representation:
	///     * c-style char pointer
	///     * stl-style std::string
	///     * begin/end string "section"
	///
	/// This useful for separating a part of a large string, or while serializing
	/// from an text file (when we want to identify an interior string without
	/// requiring an extra allocation).
	template<typename CharType=char>
	class StringSection
	{
	public:
		const CharType* _start;
		const CharType* _end;

		size_t Length() const                           { return size_t(_end - _start); }
		bool IsEmpty() const                            { return _end <= _start; }
		std::basic_string<CharType> AsString() const    { return std::basic_string<CharType>(_start, _end); }
        eastl::basic_string<CharType> AsEASTLString() const  { return eastl::basic_string<CharType>(_start, _end);}

		const CharType* begin() const   { return _start; }
		const CharType* end() const     { return _end; }
		size_t size() const				{ return Length(); }

		const CharType& operator[](size_t index) const { DRAGORA_ASSERT(index < Length()); return _start[index]; }

		StringSection(const CharType* start, const CharType* end) : _start(start), _end(end) {}
		StringSection() : _start(nullptr), _end(nullptr) {}
		StringSection(const CharType* nullTerm) : _start(nullTerm), _end(StringEnd(_start)) {}

		template<typename CT, typename A>
		StringSection(const std::basic_string<CharType, CT, A>& str) : _start(AsPointer(str.cbegin())), _end(AsPointer(str.cend())) {}
	};

	template<typename CharType>
	inline StringSection<CharType> MakeStringSection(const CharType* start, const CharType* end)
	{
		return StringSection<CharType>(start, end);
	}

	template<typename CharType>
	inline StringSection<CharType> MakeStringSection(const CharType* nullTerm)
	{
		return StringSection<CharType>(nullTerm);
	}

	template<typename CharType, typename CT, typename A>
	inline StringSection<CharType> MakeStringSection(const std::basic_string<CharType, CT, A>& str)
	{
		return StringSection<CharType>(AsPointer(str.cbegin()), AsPointer(str.cend()));
	}

	template<typename CharType, typename CT, typename A>
	inline StringSection<CharType> MakeStringSection(
			const typename std::basic_string<CharType, CT, A>::const_iterator& begin,
			const typename std::basic_string<CharType, CT, A>::const_iterator& end)
	{
		return StringSection<CharType>(AsPointer(begin), AsPointer(end));
	}

	template<typename CharType, typename CT, typename A>
	inline StringSection<CharType> MakeStringSection(
			const typename std::basic_string<CharType, CT, A>::iterator& begin,
			const typename std::basic_string<CharType, CT, A>::iterator& end)
	{
		return StringSection<CharType>(AsPointer(begin), AsPointer(end));
	}

	/// <summary>Dynamic string formatting utility<summary>
	/// StringMeld provides a simple and handy method to build a string using operator<<.
	/// StringMeld never allocates. It just has a fixed size buffer. So this is a handy
	/// way to build a string in a way that is type-safe, convenient and avoids any allocations.
	///
	/// <example>
	///     <code>\code
	///         window.SetTitle(StringMeld<128>() << "Shrapnel sample [OGRE Renderer: " << v.first << ", " << v.second << "]");
	///     \endcode</code>
	/// </example>
	template<int Count, typename CharType = char> class StringMeld
	{
	public:
		using DemotedType = typename Dragora::DemoteCharType<CharType>::Value;
		mutable std::basic_ostream<DemotedType> _stream;

		StringMeld() : _stream(&_buffer), _buffer(sizeof(CharType)) {}

		operator const CharType*() const { return (const CharType*)_buffer._buffer; }
		const CharType* get() const { return (const CharType*)_buffer._buffer; }
		StringSection<CharType> AsStringSection() const
		{
			return StringSection<CharType>((const CharType*)_buffer.begin(), (const CharType*)_buffer.end());
		}
		operator StringSection<CharType>() const { return AsStringSection(); }

	protected:
		FixedMemoryBuffer<Count * sizeof(CharType), DemotedType> _buffer;

		StringMeld(const StringMeld&) = delete;
		StringMeld& operator=(const StringMeld&) = delete;
	};


}

#include "Foundation/String.inl"

// Integrate our StringSection to std's << operators.
namespace std
{
	template<typename CharType>
	basic_ostream<CharType>& operator<<(basic_ostream<CharType>& stream, Dragora::StringSection<CharType> section)
	{
		using Demoted = typename Dragora::DemoteCharType<CharType>::Value;
		stream.write((const Demoted*)section.begin(), section.size());
		return stream;
	}
}
