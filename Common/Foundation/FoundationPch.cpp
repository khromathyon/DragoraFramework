#include "FoundationPch.h"

#include "Platform/MemoryHeap.h"

#if DRAGORA_HEAP

DRAGORA_DEFINE_DEFAULT_MODULE_HEAP( Foundation );

#if DRAGORA_DEBUG
#include "Platform/NewDelete.h"
#endif

#endif // DRAGORA_HEAP
