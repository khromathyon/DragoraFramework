#pragma once

#include <vector>
#include <algorithm>

#include <EASTL/vector.h>
#include <EASTL/algorithm.h>

namespace Dragora
{
	//
	//  This file contains some utilities for common STL operations.
	//  Avoid adding too many utilities to this file, because it's included
	//  in many places (including through Assets.h, which propagates through
	//  a lot)
	//  
	template <typename First, typename Second>
	class CompareFirst
	{
	public:
		// STD Operators
		inline bool operator()(const std::pair<First, Second>& lhs, const std::pair<First, Second>& rhs) const { return lhs.first < rhs.first; }
		inline bool operator()(const std::pair<First, Second>& lhs, const First& rhs) const { return lhs.first < rhs; }
		inline bool operator()(const First& lhs, const std::pair<First, Second>& rhs) const { return lhs < rhs.first; }

		// EASTL Operators
		inline bool operator()(const eastl::pair<First, Second>& lhs, const eastl::pair<First, Second>& rhs) const { return lhs.first < rhs.first; }
		inline bool operator()(const eastl::pair<First, Second>& lhs, const First& rhs) const { return lhs.first < rhs; }
		inline bool operator()(const First& lhs, const eastl::pair<First, Second>& rhs) const { return lhs < rhs.first; }

	};

	template <typename First, typename Second>
	class CompareSecond
	{
	public:
		// Std Operators
		inline bool operator()(std::pair<First, Second>& lhs, std::pair<First, Second>& rhs) const { return lhs.second < rhs.second; }
		inline bool operator()(std::pair<First, Second>& lhs, Second& rhs) const { return lhs.second < rhs; }
		inline bool operator()(Second& lhs, std::pair<First, Second>& rhs) const { return lhs < rhs.second; }

		// EASTL operators.
		inline bool operator()(eastl::pair<First, Second>& lhs, eastl::pair<First, Second>& rhs) const { return lhs.second < rhs.second; }
		inline bool operator()(eastl::pair<First, Second>& lhs, Second& rhs) const { return lhs.second < rhs; }
		inline bool operator()(Second& lhs, eastl::pair<First, Second>& rhs) const { return lhs < rhs.second; }
	};


	// Std Lowerbound Non-Constant Iterator
	template <typename First, typename Second, typename Allocator>
    static typename std::vector<std::pair<First, Second>, Allocator>::iterator LowerBound(
		 std::vector<std::pair<First, Second>, Allocator>&v, First compareToFirst)
	{
		return std::lower_bound(v.begin(), v.end(), compareToFirst, CompareFirst<First, Second>());
	}

	// Std Lowerbound Constant Iterator
	template <typename First, typename Second, typename Allocator>
    static typename std::vector<std::pair<First, Second>, Allocator>::const_iterator LowerBound(
		 const std::vector<std::pair<First, Second>, Allocator>&v, First compareToFirst)
	{
		return std::lower_bound(v.cbegin(), v.cend(), compareToFirst, CompareFirst<First, Second>());
	}

	// EASTL Lowerbound Non-Constant Iterator
	/*
	template <typename First, typename Second, typename Allocator>
	static typename eastl::vector<eastl::pair<First, Second>, Allocator>::iterator LowerBound(
		 eastl::vector<eastl::pair<First, Second>, Allocator>&v, First compareToFirst)
	{
		return eastl::lower_bound(v.begin(), v.end(), compareToFirst, CompareFirst<First, Second>());
	}

	// EASTL Lowerbound Constant Iterator
	template <typename First, typename Second, typename Allocator>
	static typename eastl::vector<eastl::pair<First, Second>, Allocator>::const_iterator LowerBound(
            const eastl::vector<eastl::pair<First, Second>, Allocator>&v, First compareToFirst)
	{
		return eastl::lower_bound(v.cbegin(), v.cend(), compareToFirst, CompareFirst<First, Second>());
	}*/

    //Iterator Range (STL)
    template<typename Iterator>
    struct IteratorRange : public std::pair<Iterator, Iterator>
    {
    public:
        Iterator begin() const      { return this->first;  }
        Iterator end() const        { return this->second; }
        Iterator cbegin() const     { return this->first; }
        Iterator cend() const       { return this->second; }
        size_t size() const         { return size_t(std::distance(this->first, this->second)); }
        bool empty() const          { return this->first == this->second; }

        decltype(*std::declval<Iterator>()) operator[](size_t index) const { return this->first[index]; }

        IteratorRange() : std::pair<Iterator, Iterator>(nullptr, nullptr) {}
        IteratorRange(Iterator f, Iterator s) : std::pair<Iterator, Iterator>(f, s) {}

        template<typename OtherIterator>
        IteratorRange(const std::pair<OtherIterator, OtherIterator>& copyFrom)
                : std::pair<Iterator, Iterator>(copyFrom) {}

        template<typename OtherIterator>
        operator IteratorRange<OtherIterator>() const { return IteratorRange<OtherIterator>(cbegin(), cend()); }
    };

    template<typename Container>
    IteratorRange<const typename Container::value_type*> MakeIteratorRange(const Container& c)
    {
        return IteratorRange<const typename Container::value_type*>(AsPointer(c.cbegin()), AsPointer(c.cend()));
    }

    template<typename Container>
    IteratorRange<typename Container::value_type*> MakeIteratorRange(Container& c)
    {
        return IteratorRange<typename Container::value_type*>(AsPointer(c.begin()), AsPointer(c.end()));
    }

    template<typename Iterator>
    IteratorRange<Iterator> MakeIteratorRange(Iterator begin, Iterator end)
    {
        return IteratorRange<Iterator>(begin, end);
    }

    template<typename ArrayElement, int Count>
    IteratorRange<ArrayElement*> MakeIteratorRange(ArrayElement (&c)[Count])
    {
        return IteratorRange<ArrayElement*>(&c[0], &c[Count]);
    }

    //IteratorRange (EASTL)
    //TODO.
}
