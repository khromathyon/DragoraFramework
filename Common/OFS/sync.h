/*/////////////////////////////////////////////////////////////////////////////////
/// An
///    ___   ____ _______
///   / _ \ / ___|_ ____/
///  | | | | |  _ | |__
///  | |_| | |_| || ___|
///   \___/ \____||_|
///                              File
///
/// Copyright (c) 2011 - 2015 Ismail TARIM <ismail@royalspor.com> and the Ogitor Team
//
//This program is free software; you can redistribute it and/or modify it under
//the terms of the GNU Lesser General Public License as published by the Free Software
//Foundation; either version 2 of the License, or (at your option) any later
//version.
//
//This program is distributed in the hope that it will be useful, but WITHOUT
//ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License along with
//this program; if not, write to the Free Software Foundation, Inc., 59 Temple
//Place - Suite 330, Boston, MA 02111-1307, USA, or go to
//http://www.gnu.org/copyleft/lesser.txt.
////////////////////////////////////////////////////////////////////////////////*/

/* Parts of this file derived from Smoke Demo of Intel Corporation
   Copyright message is below                                      */

// Copyright © 2008-2009 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED "AS IS."
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.

#pragma once

#define OFS_PLATFORM_WIN32 1
#define OFS_PLATFORM_LINUX 2
#define OFS_PLATFORM_APPLE 3

#define OFS_ARCHITECTURE_32 1
#define OFS_ARCHITECTURE_64 2

#if defined( __WIN32__ ) || defined( _WIN32 )
#   define OFS_PLATFORM OFS_PLATFORM_WIN32
#elif defined( __APPLE_CC__)
#   define OFS_PLATFORM OFS_PLATFORM_APPLE
#else
#   define OFS_PLATFORM OFS_PLATFORM_LINUX
#endif

    /* Find the arch type */
#if defined(__x86_64__) || defined(_M_X64) || defined(__powerpc64__) || defined(__alpha__) || defined(__ia64__) || defined(__s390__) || defined(__s390x__)
#   define OFS_ARCH_TYPE OFS_ARCHITECTURE_64
#else
#   define OFS_ARCH_TYPE OFS_ARCHITECTURE_32
#endif

#if OFS_PLATFORM == OFS_PLATFORM_LINUX
#include <pthread.h>
#endif

#ifndef DRAGORA_DISABLE_THREADSAFETY

namespace OFSMUTEX
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    ///   A spin wait object class similar to a critcal section.
    /// </summary>
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    class SpinWait
    {
        friend class Lock;

    public:

        /// <summary>
        ///   Constructor.
        /// </summary>
        SpinWait( void );

        /// <summary>
        ///   Destructor.
        /// </summary>
        ~SpinWait( void );


        ////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///   A class for locking a spin wait object.
        /// </summary>
        ////////////////////////////////////////////////////////////////////////////////////////////////

        class Lock
        {
        public:

            /// <summary>
            ///   Constructor.
            /// </summary>
            /// <remarks>
            ///   Locks the spin wait object.
            /// </remarks>
            /// <param name="sw">The spin wait object to lock.</param>
            /// <param name="bReadOnly">Signifies if the lock is for reading,
            ///                         or for reading and writing.</param>
            Lock( SpinWait& sw, bool bReadOnly = false );

            /// <summary>
            ///   Constructor.
            /// </summary>
            /// <remarks>
            ///   Unlocks the spin wait object.  So when the class goes out of scope the spin wait is
            //     released.
            /// </remarks>
            ~Lock( void );

            /// <summary>
            ///   This is a dummy assignment operator.  This class cannot be assigned.
            /// </summary>
            Lock& operator=( const Lock& l )
            {
                assert( false && "This class cannot be assigned." );

                return *this;
            }


        protected:

            SpinWait&       m_SpinWait;
        };


    protected:
#if OFS_PLATFORM == OFS_PLATFORM_WIN32
#  if OFS_ARCH_TYPE == OFS_ARCHITECTURE_32
        __declspec(align(8)) unsigned int m_Lock[ 6 ];
#  elif OFS_ARCH_TYPE == OFS_ARCHITECTURE_64
        __declspec(align(8)) unsigned int m_Lock[ 10 ];
#  endif
#elif OFS_PLATFORM == OFS_PLATFORM_LINUX
        pthread_mutex_t m_Lock;
#endif
    };

}

#endif