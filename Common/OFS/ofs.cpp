/*/////////////////////////////////////////////////////////////////////////////////
/// An
///    ___   ____ ___ _____ ___  ____
///   / _ \ / ___|_ _|_   _/ _ \|  _ \
///  | | | | |  _ | |  | || | | | |_) |
///  | |_| | |_| || |  | || |_| |  _ <
///   \___/ \____|___| |_| \___/|_| \_\
///                              File
///
/// Copyright (c) 2008-2013 Ismail TARIM <ismail@royalspor.com> and the Ogitor Team
////
/// The MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE. 
///////////////////////////////////////////////////////////////////////////////////*/

#include "ofs_base.h"
#include "ofs14.h"
#include "ofs_rfs.h"
#include <algorithm>
#include <stdio.h>
#include <regex>


using namespace std;

namespace OFS
{
    STATIC_AUTO_MUTEX_DECL(_OfsBase)

    _OfsBase::NameOfsHandleMap _OfsBase::mAllocatedHandles;

    const UUID UUID_ZERO(0,0,0,0,0,0,0,0,0,0,0);

    const unsigned int MAX_BUFFER_SIZE = (16 * 1024 * 1024);

//------------------------------------------------------------------------------

    size_t FileStream::write( const void *data, size_t size )
    {
        assert( m_pFile != NULL );

        size_t actual_len =  fwrite( data, 1, size, m_pFile );
        
        assert(actual_len == size);
        
        return actual_len;
    }


    void FileStream::close()
    {
        if( m_pFile != NULL )
        {
            fflush( m_pFile );
            fclose( m_pFile );
            m_pFile = NULL;
        }
    }

//------------------------------------------------------------------------------

    void FileStream::open( const char *file, const char *mode, int flag )
    {
        close();

#if (defined( __WIN32__ ) || defined( _WIN32 )) && ! defined( __GNUC__ )
		if( flag == 0)
            flag = _SH_DENYNO;

        m_pFile = _fsopen( file, mode, flag );
#else
        m_pFile = fopen( file, mode );
#endif
		
	}

//------------------------------------------------------------------------------
	char fl_4k[ 4096 ];
	char fl_32[ 32 ];
	char fl_b = 0;


    void FileStream::fill( ofs64 len )
    {
		ofs64 i;

		ofs64 fl4k = len >> 12;
		ofs64 fl32 = ( len & 0xFFF ) >> 5; 
		len = len & 0x1F;

        assert( m_pFile != NULL );
         
	    for( i = 0; i < fl4k; i++ )
		    fwrite( &fl_4k, 4096, 1, m_pFile );    

	    for( i = 0; i < fl32; i++ )
		    fwrite( &fl_32, 32, 1, m_pFile );    

		for( i = 0; i < len; i++ )
		    fwrite( &fl_b, 1, 1, m_pFile );    
    }


//------------------------------------------------------------------------------

    void OFSHANDLE::_preparePointers(bool append)
    {
        assert(mEntryDesc != NULL);

        mPos = 0;
        mBlock = 0;
        mBlockEnd = mEntryDesc->UsedBlocks[0].Start + mEntryDesc->UsedBlocks[0].Length;
        mRealPos = mEntryDesc->UsedBlocks[0].Start + sizeof(_Ofs::strMainEntryHeader);

        if(append)
            _setPos(mEntryDesc->FileSize);
    }

//------------------------------------------------------------------------------

    void OFSHANDLE::_setPos(ofs64 value)
    {
        if(mPos != value)
        {
            mPos = value;
            mBlockEnd = mEntryDesc->UsedBlocks[0].Start + mEntryDesc->UsedBlocks[0].Length;

            ofs64 block_size = mEntryDesc->UsedBlocks[0].Length - sizeof(_Ofs::strMainEntryHeader);
            int i = 0;
            ofs64 max_i = mEntryDesc->UsedBlocks.size();
            while(block_size <= value)
            {
                value -= block_size;
                i++;

                if(i != max_i)
                {
                    block_size = mEntryDesc->UsedBlocks[i].Length - sizeof(_Ofs::strExtendedEntryHeader);
                    mBlockEnd = mEntryDesc->UsedBlocks[i].Start + mEntryDesc->UsedBlocks[i].Length;
                }
                else
                {
                    mPos -= value;
                    value = 0;
                }
            }

            mBlock = i;
            if(i == 0)
                mRealPos = mEntryDesc->UsedBlocks[i].Start + sizeof(_Ofs::strMainEntryHeader) + value;
            else if(i == max_i)
                mRealPos = mEntryDesc->UsedBlocks[i - 1].Start + mEntryDesc->UsedBlocks[i - 1].Length;
            else
                mRealPos = mEntryDesc->UsedBlocks[i].Start + sizeof(_Ofs::strExtendedEntryHeader) + value;
        }
    }

//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

    _OfsBase::_OfsBase( FileSystemType type ) : mFileSystemType(type), mActive(false), mUseCount(0), mRecoveryMode(false), mLinkMode(false)
    {
        mFileName           = "";
        mRootDir.Owner     = this;
        mRootDir.Id         = ROOT_DIRECTORY_ID;
        mRootDir.ParentId   = ROOT_DIRECTORY_ID;
        mRootDir.Flags      = OFS_DIR;
        mRootDir.FileSize   = 0;
        mRootDir.Parent     = NULL;

        mRecycleBinRoot.Owner      = this;
        mRecycleBinRoot.Id         = RECYCLEBIN_DIRECTORY_ID;
        mRecycleBinRoot.ParentId   = RECYCLEBIN_DIRECTORY_ID;
        mRecycleBinRoot.Flags      = OFS_DIR;
        mRecycleBinRoot.FileSize   = 0;
        mRecycleBinRoot.Parent     = NULL;
    }

//------------------------------------------------------------------------------

    _OfsBase::~_OfsBase()
    {
    }

//------------------------------------------------------------------------------

    void _OfsBase::_incUseCount()
    {
        LOCK_AUTO_MUTEX

        ++mUseCount;
    }

//------------------------------------------------------------------------------

    void _OfsBase::_decUseCount()
    {
        std::string name;
        bool doDelete = false;

        //This scope is needed to make sure deletion occurs after
        //SCOPED_LOCK terminates
        {
            LOCK_AUTO_MUTEX

            --mUseCount;
            if(mUseCount < 1)
            {
                name = mFileName;
                doDelete = true;

                if(mActive)
                    _unmount();
            }
        }

        if(doDelete)
        {
            STATIC_LOCK_AUTO_MUTEX
            
            delete this;

            NameOfsHandleMap::iterator it = mAllocatedHandles.find(name);
            
            if(it != mAllocatedHandles.end())
                mAllocatedHandles.erase(it);
        }
    }


//------------------------------------------------------------------------------

    bool _OfsBase::isActive()
    {
        LOCK_AUTO_MUTEX

        return mActive; 
    }


//------------------------------------------------------------------------------

    std::string _OfsBase::getFileSystemDirectory()
    {
        if(mFileSystemType == OFS_RFS )
            return mFileName;
        else
        {
            int pos1 = mFileName.find_last_of("/");
            int pos2 = mFileName.find_last_of("\\");

            if( pos2 > pos1 ) pos1 = pos2;

            return mFileName.substr( 0, pos1 );
        }
    }

//------------------------------------------------------------------------------

	static const char *get_filename_ext(const char *filename) {
		const char *dot = strrchr(filename, '.');
		if (!dot || dot == filename) return "";
		return dot + 1;
	}

    OfsResult _OfsBase::mount(_OfsBase** ptr, const char *file, unsigned int op)
    {
        assert(ptr != 0);

        STATIC_LOCK_AUTO_MUTEX

        std::string file_name = file;

        if((file_name[file_name.size() - 1] == '/') || (file_name[file_name.size() - 1] == '\\'))
            file_name.erase(file_name.size() - 1, 1);

        NameOfsHandleMap::iterator it = mAllocatedHandles.find(file_name);

        if(it == mAllocatedHandles.end())
        {
			const char* extension = get_filename_ext(file);
#ifndef _WIN32
            int result = strcasecmp(extension, "dpackage");
#else
            int result = _strcmpi(extension, "dpackage");
#endif          
            if(op & OFS_MOUNT_FORCEOFS)
                result = 0;

			if (result == 0)
				*ptr = new _Ofs();
			else
				*ptr = new _OfsRfs();

            (*ptr)->_incUseCount();
            
            OfsResult ret = (*ptr)->_mount(file_name.c_str(), op);

            if(ret != OFS_OK)
            {
                (*ptr)->_decUseCount();
                *ptr = 0;
                return ret;
            }

            mAllocatedHandles.insert(NameOfsHandleMap::value_type(file_name, *ptr));
        }
        else
        {
            if((op & OFS_MOUNT_LINK) && (it->second->mLinkMode == false))
                return OFS_ACCESS_DENIED;

            if(op == OFS_MOUNT_CREATE)
            {
                OFS_EXCEPT("_Ofs::mount, Cannot overwrite an archive in use.");

                return OFS_ACCESS_DENIED;
            }

            *ptr = it->second;
            (*ptr)->_incUseCount();
        }

        return OFS_OK;
    }

    void _OfsBase::findFiles(const std::string& pattern, bool recursive, bool dirs, std::vector<std::string>* simpleList, OFS::FileList* detailList)
    {
        std::string mod_pattern = pattern;
        std::string directory = "/";

        // pattern can contain a directory name, separate it from mask
        size_t pos1 = pattern.rfind ('/');
        size_t pos2 = pattern.rfind ('\\');
        if (pos1 == pattern.npos || ((pos2 != pattern.npos) && (pos1 < pos2)))
            pos1 = pos2;
        
        if (pos1 != pattern.npos)
        {
            directory = pattern.substr (0, pos1 + 1);
            mod_pattern.erase( 0, directory.length() );
        }

        OFS::FileList retList;

        if( recursive )
        {
            std::string src_path = directory;
            listFilesRecursive( src_path.c_str(), retList );

            int del_size = src_path.length();

            for(unsigned int i = 0;i < retList.size();i++)
            {
                retList[ i ].name.erase( 0, del_size );
            }
        }
        else
            retList = listFiles( directory.c_str(), OFS::OFS_FILE );

        if(mod_pattern == "*")
        {
            for(unsigned int i = 0;i < retList.size();i++)
            {
                if( retList[i].flags & OFS::OFS_DIR )
                    continue;

                if (simpleList)
                {
                    simpleList->push_back( retList[i].name );
                }
                else if (detailList)
                {
                    *detailList = retList;
                }
            }
        }
        else
        {
            if(mod_pattern[0] == '*')
                mod_pattern = "." + mod_pattern;

            try
            {
                const std::regex e( mod_pattern.c_str() );
                
                for(unsigned int i = 0;i < retList.size();i++)   
                {
                    if( retList[i].flags & OFS::OFS_DIR )
                        continue;
                     
                    if(std::regex_match(retList[i].name.c_str(), e))
                    {
                        if (simpleList)
                        {
                            simpleList->push_back( retList[i].name );
                        }
                        else if (detailList)
                        {
                            detailList->push_back( retList[ i ] );
                        }
                    }
                }
            }
            catch(...)
            {
            }
        }
    }

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------

    OfsPtr& OfsPtr::operator=(OfsPtr& other)
    {
        if(mPtr == other.mPtr)
            return *this;

        if(mPtr != 0)
            mPtr->_decUseCount();

        mPtr = other.mPtr;
        if(mPtr != 0)
            mPtr->_incUseCount();

        return *this;
    }


    OfsResult OfsPtr::mount(const char *file, unsigned int op)
    {
        assert(mPtr == 0);

        return _OfsBase::mount(&mPtr, file, op);
    }

//------------------------------------------------------------------------------

    void OfsPtr::unmount()
    {
        if(mPtr != 0)
            mPtr->_decUseCount();
        
        mPtr = 0;
    }

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------


}
