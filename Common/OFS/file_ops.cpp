/*/////////////////////////////////////////////////////////////////////////////////
/// An
///    ___   ____ ___ _____ ___  ____
///   / _ \ / ___|_ _|_   _/ _ \|  _ \
///  | | | | |  _ | |  | || | | | |_) |
///  | |_| | |_| || |  | || |_| |  _ <
///   \___/ \____|___| |_| \___/|_| \_\
///                              File
///
/// Copyright (c) 2008-2013 Ismail TARIM <ismail@royalspor.com> and the Ogitor Team
////
/// The MIT License
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE. 
///////////////////////////////////////////////////////////////////////////////////*/

#include "ofs_base.h"
#include "file_ops.h"
#include "Foundation/DirectoryIterator.h"
#include "Foundation/FilePath.h"
#include "Platform/File.h"

bool OfsDirectoryExists(const char *path)
{
    return Dragora::FilePath(path).IsDirectory();
}

bool OfsCreateDirectory(const char *path)
{
    bool exists =  Dragora::FilePath(path).Exists();
    if( !exists )
        exists =   Dragora::MakePath(path);
    return exists;
}

bool OfsDeleteFile(const char *path)
{
    return Dragora::Delete(path);
}

bool OfsDeleteDirectory(const char *path)
{
    return Dragora::DeleteDirectory(path);
}

bool OfsRenameFile(const char *path, const char *new_path)
{
    Dragora::Rename(path, new_path);
    return true;
}


void OfsListContentsRecursive(OFS::_OfsBase *owner, OFS::_OfsBase::OfsEntryDesc *desc, int &id, const char *name, bool linkmode)
{
      OFS::_OfsBase::OfsEntryDesc *newdesc;


      if (Dragora::FilePath(name).IsFile())        // is name a regular file?
      {
          auto fileSize = Dragora::Status();
          fileSize.Read(name);

          newdesc = new OFS::_OfsBase::OfsEntryDesc();

          newdesc->Owner = owner;
          newdesc->Id = id;
          newdesc->ParentId = desc->Id;
          newdesc->Parent = desc;
          newdesc->Flags = OFS::OFS_FILE;
          newdesc->Name = Dragora::FilePath(std::string(name)).Filename().Get();
          newdesc->FileSize = fileSize.m_Size;
          newdesc->OldParentId = desc->Id;
          newdesc->UseCount = 0;
          newdesc->WriteLocked = false;
          newdesc->Uuid = OFS::UUID_ZERO;
          desc->Children.push_back(newdesc);

          if(linkmode)
              newdesc->Flags |= OFS::OFS_LINK;
      }
      else if (Dragora::FilePath(name).IsDirectory())      // is p a directory?
      {
          newdesc = new OFS::_OfsBase::OfsEntryDesc();

          newdesc->Owner = owner;
          newdesc->Id = id;
          newdesc->ParentId = desc->Id;
          newdesc->Parent = desc;
          newdesc->Flags = OFS::OFS_DIR;
          newdesc->Name = Dragora::FilePath(name).Directory().Get();
          newdesc->FileSize = 0;
          newdesc->OldParentId = desc->Id;
          newdesc->UseCount = 0;
          newdesc->WriteLocked = false;
          newdesc->Uuid = OFS::UUID_ZERO;
          desc->Children.push_back(newdesc);

          if(linkmode)
              newdesc->Flags |= OFS::OFS_LINK;
          

          Dragora::FilePath dirPath(name);
          if (dirPath.IsDirectory())      // is p a directory?
          {
              for( Dragora::DirectoryIterator it(dirPath); !it.IsDone(); it.Next() )
              {
                  if(it.GetItem().m_Path.IsDirectory())
                    OfsListContentsRecursive(owner, desc, ++id, it.GetItem().m_Path.Get().c_str(), linkmode);
              }
          }
      }
}

void OfsListContents(OFS::_OfsBase *owner, OFS::_OfsBase::OfsEntryDesc *desc, int &id, const char *path, bool linkmode)
{
      Dragora::FilePath dirPath(path);
      if (dirPath.IsDirectory())      // is p a directory?
      {
          for( Dragora::DirectoryIterator it(dirPath); !it.IsDone(); it.Next() )
          {
              if(it.GetItem().m_Path.IsDirectory())
                OfsListContentsRecursive(owner, desc, id, it.GetItem().m_Path.Get().c_str(), linkmode);
          }
      }
}
