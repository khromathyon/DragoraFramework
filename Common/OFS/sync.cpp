/*/////////////////////////////////////////////////////////////////////////////////
/// An
///    ___   ____ _______
///   / _ \ / ___|_ ____/
///  | | | | |  _ | |__
///  | |_| | |_| || ___|
///   \___/ \____||_|
///                              File
///
/// Copyright (c) 2011 - 2015 Ismail TARIM <ismail@royalspor.com> and the Ogitor Team
//
//This program is free software; you can redistribute it and/or modify it under
//the terms of the GNU Lesser General Public License as published by the Free Software
//Foundation; either version 2 of the License, or (at your option) any later
//version.
//
//This program is distributed in the hope that it will be useful, but WITHOUT
//ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License along with
//this program; if not, write to the Free Software Foundation, Inc., 59 Temple
//Place - Suite 330, Boston, MA 02111-1307, USA, or go to
//http://www.gnu.org/copyleft/lesser.txt.
////////////////////////////////////////////////////////////////////////////////*/

/* Parts of this file derived from Smoke Demo of Intel Corporation
   Copyright message is below                                      */

// Copyright © 2008-2009 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED "AS IS."
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.

#include <cassert>
#include "sync.h"

#ifndef DRAGORA_DISABLE_THREADSAFETY

#if OFS_PLATFORM == OFS_PLATFORM_WIN32
#define _WIN32_WINNT  0x0403 // required for InitializeCriticalSectionAndSpinCount
#include <windows.h>
#elif OFS_PLATFORM == OFS_PLATFORM_LINUX
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <errno.h>
#include <sys/time.h>
#endif

using namespace OFSMUTEX;


#if OFS_PLATFORM == OFS_PLATFORM_WIN32
    SpinWait::SpinWait( void )
    {
        // IMPLEMENTATION NOTE
        // In cases when there is no oversubscription the reasonable value of the spin 
        // count would be 50000 - 100000 (25 - 50 microseconds on modern CPUs).
        // Unfortunately the spinning in Windows critical section is power inefficient
        // so the value is a traditional 1000 (approx. the cost of kernel mode transition)
        //
        // To achieve maximal locking efficiency use TBB spin_mutex (which employs 
        // exponential backoff technique, and supports cooperative behavior in case 
        // of oversubscription)
        BOOL Result = ::InitializeCriticalSectionAndSpinCount( reinterpret_cast<LPCRITICAL_SECTION>(m_Lock), 1000 );

        assert( Result );
    }


    SpinWait::~SpinWait( void )
    {
        ::DeleteCriticalSection( reinterpret_cast<LPCRITICAL_SECTION>(m_Lock) );
    }


    SpinWait::Lock::Lock( SpinWait& sw, bool bReadOnly )
        : m_SpinWait( sw )
    {
        ::EnterCriticalSection( reinterpret_cast<LPCRITICAL_SECTION>(m_SpinWait.m_Lock) );
    }


    SpinWait::Lock::~Lock( void )
    {
        ::LeaveCriticalSection( reinterpret_cast<LPCRITICAL_SECTION>(m_SpinWait.m_Lock) );
    }

#elif OFS_PLATFORM == OFS_PLATFORM_LINUX

    SpinWait::SpinWait( void )
    {
	    pthread_mutexattr_t attr;
	    pthread_mutexattr_init(&attr);
	    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);

        pthread_mutex_init(&m_Lock, &attr);
    }


    SpinWait::~SpinWait( void )
    {
        pthread_mutex_destroy(&m_Lock);
    }


    SpinWait::Lock::Lock( SpinWait& sw, bool bReadOnly )
        : m_SpinWait( sw )
    {
        pthread_mutex_lock(&sw.m_Lock);
    }


    SpinWait::Lock::~Lock( void )
    {
        pthread_mutex_unlock(&m_SpinWait.m_Lock);
    }

#endif
#endif