# Dragora Framework Game Engine

![DragoraLogo](./Docs/Ph_DragoraFrameworkLogoAlt.png)

This is the repository of the Dragora Framework in-house game engine, developed under the direction of **Khromathyon Research** Division, this engine is only used for research
and experimental purposes, until we reach an production usage to consider an drop-in replacement for Unreal Engine 4 for future projects, at least projects which uses Cel-Shading.

This repository includes the engine runtime and the tools souce code.

## Prerequisites

Before you start to build Dragora Framework in your system the following software components and development kits must be present in your system.

* An Fairly modern C++11 compiler (MSVC2013/2015, GCC or Clang)
* CMake 3.0 or later
* Qt 5.6 or later (if the option of `DRAGORA_BUILDCONFIG_ENABLETOOLS` was checked/turned on in CMake)
* Internet Connection (to download additional dependencies at build time)

## How to build Dragora Framework (Using BootBox)

> **TODO**: Finish this section and implement BootBox utility :) .

In order to build Dragora Framework in your development machine, the following development kits must be installed into your system:

* OGRE 2.1 Development Kit: Both DirectX11 and OpenGL3Plus Rendersystems (in Windows).
* Newton Dynamics Physics Engine
* LabSound.io Sound Library
* Recast & Detour.
* SDL 2
* RTTR Reflection Library
* libRocket UI Library (Khromathyon Fork.)

*Isn't a lot of dependencies?, yes they are....*
 
Development kits such as OGRE 2.1 and others needs some special manual setup in order to work, configuring their dependencies and build systems.

**BootBox** is an special utility which downloads all those dependencies, builds them and installs them automatically into your system, BootBox arbitarly can disable the process of downloading dependencies if already are installed in your system, and also manages different development kits of Dragora Framework and their projects!
