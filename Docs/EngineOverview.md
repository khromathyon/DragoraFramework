# Dragora Framework Architecture Overview

This document present an brief overview about the Dragora Framework architecture and the components which conforms them, and also the dependencies between them and the externals ones (EASTL, OGRE, LabSound.io, etc.)

> **TODO:** Note this document is still incomplete!, features or quirks mentioned here may not appear or are not implemented.

## Core Components
General-purpose but optimized C++ frameworks/libraries which are cornerstones in the Engine components and Middleware integrations of them, some of those libraries are based from the [Helium Project](http://http://heliumproject.org/) game engine (See Platform and Foundation below.), the Core Components has only one dependency: EASTL.

### Platform 
*Location:* `Common/Platform/`

is a simple platform abstraction layer that works on Windows, OSX, and Linux. It provides a thin abstraction layer to file i/o, threads, synchronization primitives, and address space allocation.

### Foundation 
*Location*: `Common/Foundation/`

 is a set of C++ containers, interprocess communication, reference counting, and language utilities like events and delegates.

### KMath
*Location*:  `Common/KMath`

Simple C/C++ math library unlike Eigen math libraries, KMath is used only as an "container", "proxy" or "abstraction" from other libraries such OGRE, Newton Physics,etc, and ease serialization between them without hooking Reflection to the middleware types.

### OFS 
*Location*: `Common/OFS`

The "official" packaging format and "Virtual File System"-subset of Dragora Framework, based in [Ogitor File System](https://bitbucket.org/ogitor/ogitor-v2/) which is planned to add additional support like ZStandard and LZ4 compression, support to load/create .zip archives (via miniz) under OFS APIs, also removed the legacy support of older formats of OFS archives.

## Engine Components

They can be called the "Kernel" of the engine, those components conforms the engine which depends the Core Components mentioned above, this includes an Basic Runtime which provides from game information to Task Scheduling services and background/asynchronous data loading and an goodly complex Asset System thanks to XLE.

### Runtime

*Location:* `Engine/Runtime`
*Library Name (in CMake):* `DragoraEngine.Runtime`

The Runtime component provides basic services and where's the game loop starts, and also the entry point to initialize the other components such Neutrino and the Entity System Framework, also provides an game and system information given.

Runtime also features Task Scheduling facilities thanks to Intel Threading Building Blocks as their foundation, and background data streaming/loading, Runtime can calculate how threads remains in your system available to other parts to the engine to use for example OGRE 2.1 needs some threads (or just run in the main one) to do Culling, other middleware can use the task scheduler itself.

Also Runtime features an VFS API: SimpleVFS which provides simple resource access (no dependency solving, etc. which Neutrino already provides) can open Zip and OFS archives, SimpleVFS was designed when the begging of the development of Dragora Framework was started and the development of Shrapnel Blossom for an contest since Neutrino would take a lot of time to finish and stabilize, this API can be used inside of the scripting engine.

### Neutrino Asset System

*Location:* `Engine/NeutrinoAssetSystem`
*Library Name (in CMake):* `DragoraEngine.Neutrino`

> **TODO**: Finish it, at the time about i wrote this document, this is still non-functional.

The Neutrino Asset System is an heavily modified version of the [XLE](https://github.com/xlgames-inc/XLE) Engine Asset System licensed in MIT License in the `experimental` git branch, uses OFS and SimpleVFS as underlying file systems to access their data, uses an complex file data chunk format for quick access, asset dependency solving, asset hot reloading, asset compiling pipelines, etc. Once finished new middleware integrations must use Neutrino's APIs instead of SimpleVFS (as long if that middleware doesn't need dependency solving,or advanced features).

## Internal Dependency Table

This describes the internal dependencies between Dragora Components, *(note: this doesn't show external dependencies such OGRE, EASTL or cJSON, even those incorporated already in the engine's repository such miniz for example)*

```
Core Components/
├── Platform
├── Foundation
│   └── Platform
├── KMath
│   └── Platform
└── OFS
    ├── Platform
    └── Foundation

Engine Components/
├── DragoraEngine.Runtime
│   ├── Platform
│   ├── Foundation
│   └── OFS
└── DragoraEngine.Neutrino
    ├── Platform
    ├── Foundation
    ├── OFS
    ├── KMath
    └── DragoraEngine.Runtime

System Integrations/
```

## External Dependency Table

> ** TODO: ** Finish, not all planned dependencies  are here.


| Dependency Name         | Dependency Type            | Used by                    |
|-------------------------|----------------------------|----------------------------|
| [EASTL](https://github.com/electronicarts/EASTL)| External/Downloadable| All|
| [cJSON](https://github.com/DaveGamble/cJSON)| External/Downloadable| All |
| [Catch](http://catch-lib.net)| External/Downloadable| Unit Tests |
| [miniz](https://github.com/icebreaker/miniz)| External/Fixed| OFS, SimpleVFS, Neutrino, Tools|
| [Intel Threading Building Blocks](https://threadingbuildingblocks.org)| External | Engine Components, Tools|
| [LUrlParser](https://github.com/corporateshark/LUrlParser)| External/Fixed| Tools, Runtime|
| [Qt 5](https://www.qt.io/)| External | Tools | 
| Qt Widget: [burger-menu](https://github.com/chrisaverage/burger-menu)| External/Fixed| Tools|
| Qt Widget: [QPropertyBrowser](https://github.com/intbots/QtPropertyBrowser)| External/Fixed | Tools |
| [OGRE3D 2.1](http://ogre3d.org/) | External | OGRE2GraphicsSystem, Tools |
| [Newton Dynamics](http://newtondynamics.com/) | External | NewtonDynamicsSystem, Tools|
| [LibRocket](http://librocket.com/)| External(?)| libRocketSystem, Tools |
| [ImGui](https://github.com/ocornut/imgui)| External/Downloadable | Tools|
| [SDL2](https://www.libsdl.org/)| External | Engine Runtime, SDL2System |
| [xxHash](https://cyan4973.github.io/xxHash/)| External/Downloadable | Engine Runtime, Neutrino, Tools |


